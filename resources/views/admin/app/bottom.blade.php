<script type="text/javascript">
	function showConfirmModal(url){
		$('#confirmmodel').modal('show',{backdrop: 'true'});
		$("#delete").attr("action", url);
	}
	$(document).on('submit', '#delete', function(event,url) {
		var u = $(this).attr("action");
		var l = Ladda.create($(this).find('button').get(0));
		l.start();
		var modal = $('#confirmmodel').modal();
		$.ajax({
			url: u,
			type:'GET',
			data:$(this).serialize(),
			datatype : "application/json",
			success:function(data){
				l.stop();
				$('#confirmmodel').modal('hide');
				if(data.success == "true"){
					cp_app.notifyWithtEle(data.message,'success');
					setTimeout(function(){ window.location = location.href; }, 1000);
				}else{
					cp_app.notifyWithtEle(data.message,'error');
				}
			}
		});
		event.preventDefault();
	});
	function commonpopup(url){
		$('#commonModel').modal('show');
		//$('.modal-body').load(url);
		$('#commonModel').find('.modal-body').load(url);
	}
</script>

<div class="modal fade" id="confirmmodel" aria-hidden="true" aria-labelledby="confirmmodel" role="dialog" tabindex="-1">
	<div class="main-popup fifth-popup">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						x
					</button>
				</div>
				<div class="modal-body">
					<div class="confirmdelete">
						<h5> Are You Sure?</h5>
						<div class="row form-group">
							<div class="col col-md-12">
								<form accept="" id="delete" method="delete">
									<button class="btn btn-primary blue-btn" >Ok</button>
									<button type="button" class="btn btn-secondary black-btn" data-dismiss="modal">Cancel</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="commonModel" aria-hidden="true" aria-labelledby="faqModel" role="dialog" tabindex="-1">
	<div class="main-popup">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">x</button>
				</div>
				<div class="modal-body">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div class="pageloader">
  <img class="pg-loader-img" src="assets/images/page-loader.gif" alt="not load image" />
</div> -->



<!-- BEGIN: Page Vendor JS-->
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/charts/chartist.min.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/charts/raphael-min.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/charts/morris.min.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/timeline/horizontal-timeline.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{$ADMIN_THEME_PATH}}/app-assets/js/core/app-menu.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/app-assets/js/core/app.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/ui/jquery.sticky.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/app-assets/js/scripts/tables/datatables/datatable-api.js"></script>
<!-- END: Page JS-->

<script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/plugins/ladda/spin.min.js"></script>
<script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/plugins/ladda/ladda.min.js"></script>
<script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/plugins/noty/noty.min.js"></script>
<script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/js/common.min.js"></script>
<script type="text/javascript">
@if ($success = Session::get('success'))
	cp_app.notifyWithtEle("{{$success}}",'success');
@endif
@if ($error = Session::get('error'))
	cp_app.notifyWithtEle("{{$error}}",'error');
@endif
@if ($warning = Session::get('warning'))
	cp_app.notifyWithtEle("{{$warning}}",'warning');
@endif

//$(window).on('load', function(){$('.pageloader').hide();});
</script>

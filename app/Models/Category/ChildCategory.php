<?php

namespace App\Models\Category;

use Eloquent, Request;

class ChildCategory extends Eloquent {

	protected $table = 'mst_child_cat';

	public $timestamps = true;

	protected $fillable = [
		'sub_cat_id',
		'cat_id',
		'child_cat_name',
		'child_cat_img',
		'child_cat_banner',
		'cat_dec',
		'order_seq',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $primaryKey = "child_cat_id";
	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

	protected $casts = ['child_cat_img' => 'array','child_cat_banner' => 'array'];

	public function category()
	{
		return $this->hasOne('App\Models\Category\Category','cat_id','cat_id');
	}

	public function subcategory()
	{
		return $this->hasOne('App\Models\Category\SubCategory','sub_cat_id','sub_cat_id');
	}
}
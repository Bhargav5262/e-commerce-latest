@extends('admin.app.index')

@section('title')
    {{$section ?? 'Admin'}}
@stop

@section('css')

@stop

@section('model')

@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row mb-1">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Master Management</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">{{$section}} Management</a></li>
                        <li class="breadcrumb-item"><a href="{{url($sectionSlug)}}">{{$section}}</a></li>
                        <li class="breadcrumb-item active">{{ isset($view) ? ucfirst($view) : 'List'}}</li>
                    </ol>
                </div>
            </div>
            <h3 class="content-header-title mb-0">{{$singleSection}}</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ isset($view) ? ucfirst($view) : 'List'}} {{$singleSection}}
                                @if ( isset($view) && in_array($view, ['add','edit','view']) )
                                    <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary pull-right"><i class="ft-arrow-left"></i> Back To List</a>
                                @else
                                    <a href="{{ url( $sectionSlug , 'add') }}" class="btn btn-primary pull-right"><i class="ft-plus"></i> Add {{ $singleSection }}</a>
                                @endif
                            </h4>
                        </div>    
                        @if ( isset($view) && in_array($view, ['add','edit']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST" name="category_form" id="category_form" class="form" action="{{url($sectionSlug.'/action/')}}/{{$view}}/{{isset($data->ptl_id) ? $data->ptl_id: '0'}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-body my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
                                            <div class="form-group">
                                                <label for="cat_name">plt_name</label>
                                                <input type="text" maxlength="20" pattern="[a-zA-Z0-9\s]+" required="required" name="plt_name" id="plt_name" value="{{$data->plt_name ?? old('plt_name')}}" class="form-control round" placeholder="plt_name">
                                                <input type="hidden" name="_plt_name" id="_plt_name" value="{{$data->plt_name ?? ''}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="cat_dec">plt_val</label>
                                                <textarea name="plt_val" id="plt_val" maxlength="20" pattern="[a-zA-Z0-9\s]+" required="required" class="form-control round" placeholder="plt_val">{{$data->plt_val ?? old('plt_val')}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="cat_dec">plt_display_val</label>
                                                <textarea name="plt_disp_val" id="plt_disp_val" maxlength="20" pattern="[a-zA-Z0-9\s]+" required="required" class="form-control round" placeholder="plt_display_val">{{$data->plt_disp_val ?? old('plt_disp_val')}}</textarea>
                                            </div>
                                                <inputy type="hidden" name="plt_data_type" id="plt_data_type" value="">
                                            <div class="form-group">
                                                <label for="plt_image">plt_image</label>
                                                <input type="file" name="plt_image[]" id="plt_image" multiple="" class="form-control round @if(isset($view) && in_array($view, ['edit']) ) ignore @endif">
                                            </div>
                                            <?php
                                            if(isset($data->plt_image) && !empty($data->plt_image))
                                            {
                                            ?>
                                                <div class="row">
                                                        <figure class="col-lg-1 col-md-6 col-12">
                                                           <img style="height:50px;" class="img-thumbnail img-fluid" id="image" src="{{ $data->plt_image }}" itemprop="thumbnail"/>
                                                        </figure>
                                                </div>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <div class="row" style="display:none;" id="imageTag">
                                                    <figure class="col-lg-1 col-md-6 col-12">
                                                        <img style="height:50px;" class="img-thumbnail img-fluid" id="image" itemprop="thumbnail"/>
                                                    </figure>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                            <div class="form-group">
                                                <label for="order_seq">plt_display_priority (1 Highest, 100 Lowest)</label>
                                                <input type="number" min="1" max="100" required="required" name="plt_disp_priority" id="plt_disp_priority" value="{{$data->plt_disp_priority ?? old('plt_disp_priority')}}" class="form-control round" placeholder="plt_display_priority">
                                            </div>
                                            <div class="form-group">
                                                <label for="is_active">Status</label>
                                                <select class="form-control round" name="is_active" id="is_active">
                                                    <option value="1" @if(isset($data) && $data->is_active == "1") selected @endif>Active</option>
                                                    <option value="0" @if(isset($data) && $data->is_active == "0") selected @endif>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                        @if ( isset($view) && in_array($view, ['add','edit']) )
                                            <div class="form-actions">
                                                <a href="{{url($sectionSlug)}}" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        @elseif ( isset($view) && in_array($view, ['view']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-6 label-control">Plt_Name:</label>
                                                    <label class="col-md-6 label-control">{{$data->plt_name ?? old('plt_name')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-6 label-control">Plt_val:</label>
                                                    <label class="col-md-6 label-control">{{$data->plt_val ?? old('plt_val')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-6 label-control">Plt_Display_Value:</label>
                                                    <label class="col-md-6 label-control">{{$data->plt_disp_val ?? old('plt_disp_val')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-6 label-control">Plt_Disp_Priority:</label>
                                                    <label class="col-md-6 label-control">{{$data->plt_disp_priority ?? old('plt_disp_priority')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-6 label-control">Images:</label>
                                                    @if(isset($data->plt_image) && !empty($data->plt_image))
                                                        <div class="row">
                                                                <figure class="col-md-6">
                                                                   <img class="img-thumbnail img-fluid" style="height:50px;width:50%;" src="{{ $data->plt_image }}" itemprop="thumbnail"/>
                                                                </figure>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-6 label-control">Status:</label>
                                                    <label class="col-md-6 label-control">{{$data->status ?? old('status')}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary"><i class="ft-arrow-left"></i> Back To List</a>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="{{$singleSection}}-table" class="table table-striped table-bordered text-inputs-searching">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Name</th>
                                                    <th>Display Value</th>
                                                    <th>Display Priority</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Name</th>
                                                    <th>Display Value</th>
                                                    <th>Display Priority</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                @if(isset($data) && count($data) > 0)
                                                    @foreach($data as $key => $row)
                                                        <tr>
                                                            <td>{{ $row->ptl_id }}</td>
                                                            <td>{{ $row->plt_name }}</td>
                                                            <td>{{ $row->plt_disp_val }}</td>
                                                            <td>{{ $row->plt_disp_priority }}</td>
                                                            <td><?= $row->is_active=="1"?"Active":"Inactive"; ?></td>
                                                            <td>
                                                                <a href="{{ url( $sectionSlug , [ 'view' , $row->ptl_id] ) }}" title="View"><i class="ft-eye"></i></a> | 
                                                                <?php if($row->is_active=="1")
                                                                {
                                                                ?>
                                                                <a href="{{ url( $sectionSlug , [ 'edit' , $row->ptl_id ] ) }}" title="Edit"><i class="la la-pencil"></i></a> |
                                                                <?php 
                                                                }
                                                                ?>
                                                                <a href="{{ url( $sectionSlug ,[ $row->ptl_id , $row->is_active, 'status']) }}" title="Change Status" >
                                                                    @if( $row->is_active == '0' )
                                                                        <i class="la la-check"></i>
                                                                    @else
                                                                        <i class="la la-times"></i>
                                                                    @endif
                                                                </a>
                                                            </td>    
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="3" class="text-center">No data found.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
document.getElementById("plt_image").onchange = function () {
    $('#imageTag').show();
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        document.getElementById("image").src = e.target.result;
    };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
};
</script>
@stop

@section('jspage')

@stop

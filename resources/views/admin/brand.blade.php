@extends('admin.app.index')

@section('title')
    {{$section ?? 'Admin'}}
@stop

@section('css')

@stop

@section('model')

@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row mb-1">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Master Management</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">{{$section}} Management</a></li>
                        <li class="breadcrumb-item"><a href="{{url($sectionSlug)}}">{{$section}}</a></li>
                        <li class="breadcrumb-item active">{{ isset($view) ? ucfirst($view) : 'List'}}</li>
                    </ol>
                </div>
            </div>
            <h3 class="content-header-title mb-0">{{$singleSection}}</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ isset($view) ? ucfirst($view) : 'List'}} {{$singleSection}}
                                @if ( isset($view) && in_array($view, ['add','edit','view']) )
                                    <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary pull-right"><i class="ft-arrow-left"></i> Back To List</a>
                                @else
                                    <a href="{{ url( $sectionSlug , 'add') }}" class="btn btn-primary pull-right"><i class="ft-plus"></i> Add {{ $singleSection }}</a>
                                @endif
                            </h4>
                        </div>    
                        @if ( isset($view) && in_array($view, ['add','edit']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST" name="brand_form" id="brand_form" class="form" action="{{url($sectionSlug.'/action/')}}/{{$view}}/{{isset($data->brand_id) ? $data->brand_id: '0'}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-body my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
                                            <div class="form-group">
                                                <label for="brand_name">Brand Name</label>
                                                <input type="text" name="brand_name" id="brand_name" value="{{$data->brand_name ?? old('brand_name')}}" class="form-control round" placeholder="brand name">
                                                <input type="hidden" name="_brand_name" id="_brand_name" value="{{$data->brand_name ?? ''}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="brand_code">Brand Code</label>
                                                <input type="text" name="brand_code" id="brand_code" value="{{$data->brand_code ?? old('brand_code')}}" class="form-control round" placeholder="brand code">
                                            </div>
                                            <div class="form-group">
                                                <label for="brand_img_files">Brand Images</label>
                                                <input type="file" name="brand_img_files[]" id="brand_img_files" multiple="" class="form-control round @if(isset($view) && in_array($view, ['edit']) ) ignore @endif">
                                            </div>
                                            @if(isset($data->brand_img) && !empty($data->brand_img))
                                                <div class="row">
                                                    @foreach($data->brand_img as $key => $row)
                                                        <figure class="col-lg-1 col-md-6 col-12">
                                                           <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                        </figure>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <label for="brand_banner_files">Brand Banner</label>
                                                <input type="file" name="brand_banner_files[]" id="brand_banner_files" multiple="" class="form-control round @if(isset($view) && in_array($view, ['edit']) ) ignore @endif">
                                            </div>
                                            @if(isset($data->brand_banner) && !empty($data->brand_banner))
                                                <div class="row">
                                                    @foreach($data->brand_banner as $key => $row)
                                                        <figure class="col-lg-1 col-md-6 col-12">
                                                           <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                        </figure>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <label for="order_seq">Order Sequence</label>
                                                <input type="number" name="order_seq" id="order_seq" value="{{$data->order_seq ?? old('order_seq')}}" class="form-control round" placeholder="Order Sequence">
                                            </div>
                                            <div class="form-group">
                                                <label for="is_active">Status</label>
                                                <select class="form-control round" name="is_active" id="is_active">
                                                    <option value="1" @if(isset($data) && $data->is_active == "1") selected @endif>Active</option>
                                                    <option value="0" @if(isset($data) && $data->is_active == "0") selected @endif>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                        @if ( isset($view) && in_array($view, ['add','edit']) )
                                            <div class="form-actions">
                                                <a href="{{url($sectionSlug)}}" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        @elseif ( isset($view) && in_array($view, ['view']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Brand Name:</label>
                                                    <label class="col-md-8 label-control">{{$data->brand_name ?? old('brand_name')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Brand Images:</label>
                                                    @if(isset($data->brand_img) && !empty($data->brand_img))
                                                        <div class="row">
                                                            @foreach($data->brand_img as $key => $row)
                                                                <figure class="col-lg-1 col-md-6 col-12">
                                                                   <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                                </figure>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Brand Banner:</label>
                                                    @if(isset($data->brand_banner) && !empty($data->brand_banner))
                                                        <div class="row">
                                                            @foreach($data->brand_banner as $key => $row)
                                                                <figure class="col-lg-1 col-md-6 col-12">
                                                                   <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                                </figure>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Order Sequence:</label>
                                                    <label class="col-md-8 label-control">{{$data->order_seq ?? old('order_seq')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Status:</label>
                                                    <label class="col-md-8 label-control">{{$data->status ?? old('status')}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary"><i class="ft-arrow-left"></i> Back To List</a>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="{{$singleSection}}-table" class="table table-striped table-bordered text-inputs-searching">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                @if(isset($data) && count($data) > 0)
                                                    @foreach($data as $key => $row)
                                                        <tr>
                                                            <td>{{ $row->brand_name }}</td>
                                                            <td>{{ $row->status }}</td>
                                                            <td>
                                                                <a href="{{ url( $sectionSlug , [ 'view' , $row->brand_id] ) }}" title="View"><i class="ft-eye"></i></a> | 
                                                                <a href="{{ url( $sectionSlug , [ 'edit' , $row->brand_id ] ) }}" title="Edit"><i class="la la-pencil"></i></a> |
                                                                <a href="{{ url( $sectionSlug ,[ $row->brand_id , $row->is_active, 'status']) }}" title="Change Status" >
                                                                    @if( $row->is_active == '0' )
                                                                        <i class="la la-check"></i>
                                                                    @else
                                                                        <i class="la la-times"></i>
                                                                    @endif
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="3" class="text-center">No data found.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('jspage')

@stop

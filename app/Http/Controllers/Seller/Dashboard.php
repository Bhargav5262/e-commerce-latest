<?php
namespace App\Http\Controllers\Seller;
use App\Http\Controllers\Controller;

class Dashboard extends Controller {

	protected $section;
	public function __construct(){
		$this->section = "Dashboard";
	}

	public function index() {
		$_data=array(
			'view'=>"list",
		);
		return view('seller/dashboard', $_data);
	}

}
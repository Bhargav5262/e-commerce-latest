<?php
namespace App\Http\Controllers\Seller;
use Request, Auth, Lang, Hash;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Models\Admin as AdminModel;

class Login extends Controller {

	protected function getLogin() {
		if(\Cookie::get('seller_login') == '1'):
			return redirect('seller/dashboard');
		endif;
		return view('seller/login');
	}

	protected function postLogin() {
		$data = Request::all();
		unset($data['_token']);
		$result = _curl_response(env('API_URL').'tra/usr/loginusr',$data);
		if(isset($result['status']) && $result['status'] == 200):
			if(isset($result['data']['user_id'])):
				Auth::guard('seller')->loginUsingId($result['data']['user_id'], true);
			endif;
			$arr = array("success"=>"true","redirect"=>url('seller/dashboard'),"message"=>"Signin successfull.");
		else:
			$arr = array("error"=>"false","message"=>$result['message']);
		endif;	
		return response()->json($arr);
	}
	
	protected function getLogout() {
		$auth = auth()->guard('seller');
		$auth->logout();
		return redirect('seller/login')->with(['success' => 'Logged Out']);
	}

	protected function getForgotPassword() {

		if (auth()->guard('seller')->check()) {
			return redirect('seller/dashboard');
		}
		return view('seller/forgotpassword');
	}

	protected function postForgotPassword( Request $request ) {
		$data = Request::all();
		unset($data['_token']);
		$result = _curl_response(env('API_URL').'tra/usr/forgotpwdusr',$data);
		if(isset($result['status']) && $result['status'] == 200):
			$arr = array("success"=>"true","redirect"=>url('seller/verifyotp'),"message"=>Lang::get('message.checkSMS'));
		else:
			$arr = array("error"=>"false","message"=>$result['message']);
		endif;	
		return response()->json($arr);
	}

	public function newPassword($id="") {
		$data = Request::all();
		if (auth()->guard('seller')->check()):
			return redirect('seller/dashboard');
		endif;
		$data = AdminModel::where("remember_token",$id)->first();
		if(isset($data) && !empty($data)):
			$_data=array('id'=>$id,'successlink'=>1);
	        return view( 'seller/new-password', $_data );
		else:
			$_data=array('id'=>$id,'successlink'=>0);
	        return view( 'seller/new-password', $_data);
		endif;
	}

	public function postConfirmNewPassword() {
		$post_data = Request::all();
		$data = AdminModel::where("remember_token",$post_data['id'])->first();
		if(isset($data) && !empty($data)):
			$update=array('remember_token'=>"",'password'=>Hash::make($post_data['password']));
			AdminModel::find($data->id)->update($update);
			$_data=array('id'=>$post_data['id'],'success'=>1);
			return redirect('seller/login')->with('success','Password has been successfully update.');
		else:
			$_data=array('id'=>$post_data['id'],'success'=>0);
			return redirect('seller/new-password/'.$post_data['id']);				
		endif;
		
	}

	public function getVerifyOTP($id="") {
		$data = Request::all();
		if (auth()->guard('seller')->check()):
			return redirect('seller/dashboard');
		endif;
		$data = AdminModel::where("id", $id)->first();
		if(isset($data) && !empty($data)):
			$_data = array('id' => $id, 'successotp' => 1);
	        return view( 'seller/verifyotp', $_data );
		else:
			$_data = array('id' => $id, 'successotp' => 0);
	        return view( 'seller/verifyotp', $_data );
		endif;
	}

	public function postVerifyOTP( Request $request ) {
		$data = Request::all();
		unset($data['_token']);
		$result = _curl_response(env('API_URL').'tra/usr/validateotp',$data);
		if(isset($result['status']) && $result['status'] == 200):
			$arr = array("success"=>"true","redirect"=>url('seller/login'),"message"=>$result['message']);
		else:
			$arr = array("error"=>"false","message"=>$result['message']);
		endif;	
		return response()->json($arr);
	}

	public function postConfirmOTP() {
		$post_data = Request::all();
		$data = AdminModel::where("id",$post_data['id'])->first();
		if(isset($data) && !empty($data)):
			$update=array('otp'=>"",'password'=>Hash::make($post_data['password']));
			AdminModel::find($data->id)->update($update);
			$_data=array('id'=>$post_data['id'],'success'=>1);
			return redirect('seller/login')->with('success','Password has been successfully update.');
		else:
			$_data=array('id'=>$post_data['id'],'success'=>0);
			return redirect('seller/new-password-otp/'.$post_data['id']);				
		endif;
		
	}

	public function generateNumericOTP($n) { 
	    $generator = "1357902468"; 
	  
	    $result = ""; 
	    for ($i = 1; $i <= $n; $i++) { 
	        $result .= substr($generator, (rand()%(strlen($generator))), 1); 
	    } 
	    return $result; 
	} 
}

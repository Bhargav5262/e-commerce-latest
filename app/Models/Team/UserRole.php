<?php

namespace App\Models\Team;

use Eloquent, Request;

class UserRole extends Eloquent {

	protected $table = 'mst_role';

	public $timestamps = true;

	protected $fillable = [
		'role_name',
		'role_access',
		'php_access',
		'mobile_access',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $primaryKey = "role_id";
	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
}
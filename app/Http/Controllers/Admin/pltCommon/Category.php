<?php
namespace App\Http\Controllers\Admin\pltCommon;
use Request, Lang, File, Storage;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Category\Common as CategoryModel;

class Category extends Controller {

	protected $section;
	protected $singleSection;
    protected $viewPath;
    protected $sectionSlug;

	public function __construct(){
		$this->section = 'pltCommon';
		$this->singleSection = 'pltCommon';
        $this->viewPath = 'admin/pltCommon/category';
        $this->sectionSlug = 'admin/pltCommon';
	}

	public function index() {
		$data = DB::table('plt_common')->
				orderBy('plt_name', 'DESC')->
    			orderBy('plt_disp_val', 'DESC')->
				get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"list",
			'data'=>$data,
		);
		return view($this->viewPath, $_data);
	}

	public function Add() {
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"add",
		);
		return view($this->viewPath, $_data);
	}
	
	public function Edit($id="") {
        $data = CategoryModel::where('ptl_id',$id)->first();
        if(isset($data) && !empty($data)):
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"edit",
	            'data'=>$data,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function View($id="") {
        $data = CategoryModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->where('ptl_id',$id)->first();
        if(isset($data) && !empty($data)):
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"view",
	            'data'=>$data,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function Action($action="",$id="") {
		$post_data = Request::all();
		unset($post_data['_token']);
		$userId = 0;
		if( auth()->guard('admin')->check() ):
			$userId = auth()->guard('admin')->user()->id;
		endif;
		if($action=="add"):
			##  Images
          	if(isset($post_data['plt_image']) && $post_data['plt_image'] != ''):
		        if( Request::file() ):
		            $images = Request::file('plt_image');
		            if($images):
		            	$imagesdata = array();
		            	foreach($images as $key => $row):
		            		$fileName = 'img_'.$userId.'_'.time().'_'.$row->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/pltCommon/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($row));
           					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/pltCommon/' . $fileName;
			                $imagesdata = _preg_replace($filePath);
		            	endforeach;	
			        	$post_data['plt_image'] = $imagesdata;
		            endif;
		        endif;
		    endif;
		    #end
		    
		    $post_data['created_by'] = $userId;
			$post_data['updated_by'] = $userId;
			$CategoryModel=CategoryModel::where('plt_name',$post_data['plt_name'])
							->where('plt_val',$post_data['plt_val'])
							->first();
			if(empty($CategoryModel))
			{
				CategoryModel::create($post_data);
				return redirect($this->sectionSlug)->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ])); 
			}
			else
			{
				return redirect($this->sectionSlug)->with( 'error', "Data already existed"); 
			}						
        elseif($action=="edit"):
        	##  Images
          	if(isset($post_data['plt_image']) && $post_data['plt_image'] != ''):
		        if( Request::file() ):
		            $images = Request::file('plt_image');
		            if($images):
		            	$imagesdata = array();
		            	foreach($images as $key => $row):
		            		$fileName = 'img_'.$userId.'_'.time().'_'.$row->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/pltCommon/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($row));
           					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/pltCommon/' . $fileName;
			                $imagesdata = _preg_replace($filePath);
		            	endforeach;	
			        	$post_data['plt_image'] = $imagesdata;
		            endif;
		        endif;
		    endif;
		    #end
		    $post_data['updated_by'] = $userId;
    		
			
			$CategoryModel=CategoryModel::where('plt_name',$post_data['plt_name'])
							->where('plt_val',$post_data['plt_val'])
							->where('ptl_id','!=',$id)
							->first();
			if(empty($CategoryModel))
			{
				CategoryModel::find($id)->update($post_data);
				return redirect($this->sectionSlug.'/edit/'.$id)->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ]));
			}
			else
			{
				return redirect($this->sectionSlug)->with( 'error', "Data already existed"); 
			}

		elseif($action=="delete"):
            $_data = CategoryModel::where('ptl_id',$id)->first();
        	if(isset($_data)):
                $_data->delete();
                $arr = array("success"=>"true","message"=>Lang::get('message.detailDeleted', [ 'section' => $this->section ]));
            else:
                $arr = array("success"=>"false","message"=>"Something Went Wrong.");
            endif;
            return response()->json($arr);
        endif; 
    }

    public function status( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '0'];
			$message = Lang::get('message.statusInactive', ['section' => $this->section]);
		elseif($status == '0'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusActive', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			CategoryModel::where('ptl_id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}

}

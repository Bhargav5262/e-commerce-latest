@extends('admin.app.index')

@section('title')
    {{$section ?? 'Admin'}}
@stop

@section('css')

@stop

@section('model')

@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row mb-1">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Master Management</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">{{$section}} Management</a></li>
                        <li class="breadcrumb-item"><a href="{{url($sectionSlug)}}">{{$section}}</a></li>
                        <li class="breadcrumb-item active">{{ isset($view) ? ucfirst($view) : 'List'}}</li>
                    </ol>
                </div>
            </div>
            <h3 class="content-header-title mb-0">{{$singleSection}}</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ isset($view) ? ucfirst($view) : 'List'}} {{$singleSection}}</h4>
                        </div>    
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="{{$singleSection}}-table" class="table table-striped table-bordered text-inputs-searching">
                                        <thead>
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Mobile</th>
                                                <th>WhatsApp No.</th>
                                                <th>Email</th>
                                                <th>Seller Type</th>
                                                <th>Buyer Type</th>
                                                <th>GST NO.</th>
                                                <th>Area</th>
                                                <!-- <th>Added By</th> -->
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Mobile</th>
                                                <th>WhatsApp No.</th>
                                                <th>Email</th>
                                                <th>Seller Type</th>
                                                <th>Buyer Type</th>
                                                <th>GST NO.</th>
                                                <th>Area</th>
                                                <!-- <th>Added By</th> -->
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @if(isset($data) && count($data) > 0)
                                                @foreach($data as $key => $row)
                                                    <tr>
                                                        <td>{{ $row->comp_name ?? ''}}</td>
                                                        <td>{{ $row->mobile ?? ''}}</td>
                                                        <td>{{ $row->whatsapp_mob_no ?? ''}}</td>
                                                        <td>{{ $row->email ?? ''}}</td>
                                                        <td>{{ $row->seller_type ?? ''}}</td>
                                                        <td>{{ $row->buyer_type ?? ''}}</td>
                                                        <td>{{ $row->gst_no ?? ''}}</td>
                                                        <td>{{ $row->zone->zone_name ?? ''}}, {{ $row->region->region_name ?? ''}}, {{ $row->area->area_name ?? ''}}, {{ isset($row->route_id) ? _route($row->route_id) : ''}}</td>
                                                        <td>{{ $row->status ?? ''}}</td>
                                                        <td>
                                                            @if( ($row->is_active == '2') )
                                                                <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                                                                    <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ft-settings icon-left"></i></button>
                                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                                        <a class="dropdown-item" href="{{ url( $sectionSlug ,[ $row->user_id , 1, 'application']) }}">Approve</a>
                                                                        <a class="dropdown-item" href="{{ url( $sectionSlug ,[ $row->user_id , 3, 'application']) }}">Disapprove</a>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            @if( ($row->is_active == '0' || $row->is_active == '1') )
                                                                <a href="{{ url( $sectionSlug ,[ $row->user_id , $row->is_active, 'status']) }}" title="Change Status for active or inactive" >
                                                                    @if( $row->is_active == '0' )
                                                                        <i class="la la-check"></i>
                                                                    @else
                                                                        <i class="la la-times"></i>
                                                                    @endif
                                                                </a>
                                                            @endif
                                                        </td>    
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="10" class="text-center">No data found.</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('jspage')

@stop

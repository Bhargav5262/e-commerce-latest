<?php
namespace App\Http\Controllers\Admin\Team;
use Request, Lang, File, Storage;
use App\Http\Controllers\Controller;
use App\Models\Team\Team as TeamModel;
use App\Models\Team\UserRole as UserRoleModel;
use App\Models\Team\Headquarter as HeadquarterModel;
use App\Models\Zone as ZoneModel;
use App\Models\Route as RouteModel;

class Team extends Controller {

	protected $section;
	protected $singleSection;
    protected $viewPath;
    protected $sectionSlug;

	public function __construct(){
		$this->section = 'Team';
		$this->singleSection = 'Team';
        $this->viewPath = 'admin/team/team';
        $this->sectionSlug = 'admin/team';
	}

	public function index() {
		$data = TeamModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->orderBy('sales_team_id',"DESC")->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"list",
			'data'=>$data,
		);
		return view($this->viewPath, $_data);
	}

	public function Add() {
		$roles = UserRoleModel::where('is_active', '1')->orderBy('role_name','ASC')->get();
		$zones = ZoneModel::where('is_active','1')->orderBy('zone_name','ASC')->get();
		$headquarters = HeadquarterModel::where('is_active', '1')->orderBy('HQ_name','ASC')->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"add",
			'roles'=>$roles,
			'zones'=>$zones,
			'headquarters'=>$headquarters,
		);
		return view($this->viewPath, $_data);
	}
	
	public function Edit($id="") {
        $data = TeamModel::where("sales_team_id",$id)->first();
        $roles = UserRoleModel::where('is_active', '1')->orderBy('role_name','ASC')->get();
		$zones = ZoneModel::where('is_active','1')->orderBy('zone_name','ASC')->get();
		$headquarters = HeadquarterModel::where('is_active', '1')->orderBy('HQ_name','ASC')->get();
        if(isset($data) && !empty($data)):
        	$data->password = decrypt($data->password);
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"edit",
	            'data'=>$data,
				'roles'=>$roles,
				'zones'=>$zones,
				'headquarters'=>$headquarters,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function View($id="") {
        $data = TeamModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->where("sales_team_id",$id)->first();
        if(isset($data) && !empty($data)):
	        $routes = [];
	        if(isset($data->route_id) && !empty($data->route_id)):
	        	$routes = RouteModel::where('is_active','1')->whereIn('sales_team_id', explode(',',$data->route_id))->orderBy('route_name','ASC')->get();
	    	endif;
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"view",
	            'data'=>$data,
	            'routes'=>$routes,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function Action($action="",$id="") {
		$post_data = Request::all();
		unset($post_data['_token']);
		$userId = 0;
		if( auth()->guard('admin')->check() ):
			$userId = auth()->guard('admin')->user()->id;
		endif;
		if($action=="add"):
			$post_data['password'] = encrypt($post_data['password']);
			if(isset($post_data['route_id']) && !empty($post_data['route_id'])):
				$post_data['route_id'] = implode(',', $post_data['route_id']);	
			endif;
			$post_data['created_by'] = $userId;
		    $post_data['updated_by'] = $userId;
            TeamModel::create($post_data);
        	return redirect($this->sectionSlug)->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ])); 
        elseif($action=="edit"):
        	$post_data['password'] = encrypt($post_data['password']);
        	if(isset($post_data['route_id']) && !empty($post_data['route_id'])):
				$post_data['route_id'] = implode(',', $post_data['route_id']);	
			endif;
        	$post_data['updated_by'] = $userId;
    		TeamModel::find($id)->update($post_data);
            return redirect($this->sectionSlug.'/edit/'.$id)->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ]));
		elseif($action=="delete"):
            $_data = TeamModel::where('sales_team_id',$id)->first();
        	if(isset($_data)):
                $_data->delete();
                $arr = array("success"=>"true","message"=>Lang::get('message.detailDeleted', [ 'section' => $this->section ]));
            else:
                $arr = array("success"=>"false","message"=>"Something Went Wrong.");
            endif;
            return response()->json($arr);
        endif; 
    }

    public function status( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '0'];
			$message = Lang::get('message.statusInactive', ['section' => $this->section]);
		elseif($status == '0'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusActive', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			TeamModel::where('sales_team_id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}

}

@extends('admin.app.index')

@section('title')
    {{$section ?? 'Admin'}}
@stop

@section('css')

@stop

@section('model')

@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row mb-1">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Master Management</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">{{$section}}</a></li>
                        <li class="breadcrumb-item active">{{ isset($view) ? ucfirst($view) : 'List'}}</li>
                    </ol>
                </div>
            </div>
            <h3 class="content-header-title mb-0">{{$singleSection}}</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ isset($view) ? ucfirst($view) : 'List'}} {{$singleSection}}
                                @if ( isset($view) && in_array($view, ['add','edit','view']) )
                                    <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary pull-right"><i class="ft-arrow-left"></i> Back To List</a>
                                @else
                                    <a href="{{ url( $sectionSlug , 'add') }}" class="btn btn-primary pull-right"><i class="ft-plus"></i> Add {{ $singleSection }}</a>
                                @endif
                            </h4>
                        </div>    
                        @if ( isset($view) && in_array($view, ['add','edit']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST" name="route_form" id="route_form" class="form" action="{{url($sectionSlug.'/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-body my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
                                            
                                            <div class="form-group">
                                                <label for="zone_id">Zone</label>
                                                <select class="form-control round" name="zone_id" id="zone_id">
                                                    <option value="">Select Zone</option>
                                                    @foreach($zones as $zone)
                                                        <option value="{{ $zone->id }}" @if(isset($data) && $zone->id == $data->zone_id) selected @endif>{{ $zone->zone_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="region_id">Region</label>
                                                <select class="form-control round" name="region_id" id="region_id">
                                                    <option value="">Select Region</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="area_id">Area</label>
                                                <select class="form-control round" name="area_id" id="area_id">
                                                    <option value="">Select Area</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="route_name">Route Name</label>
                                                <input type="text" name="route_name" id="route_name" value="{{$data->route_name ?? old('route_name')}}" class="form-control round" placeholder="route name">
                                                <input type="hidden" name="_route_name" id="_route_name" value="{{$data->route_name ?? ''}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="is_active">Status</label>
                                                <select class="form-control round" name="is_active" id="is_active">
                                                    <option value="1" @if(isset($data) && $data->is_active == "1") selected @endif>Active</option>
                                                    <option value="0" @if(isset($data) && $data->is_active == "0") selected @endif>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                        @if ( isset($view) && in_array($view, ['add','edit']) )
                                            <div class="form-actions">
                                                <a href="{{url($sectionSlug)}}" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        @elseif ( isset($view) && in_array($view, ['view']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Zone Name:</label>
                                                    <label class="col-md-8 label-control">{{$data->zones->zone_name}}</label>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Region Name:</label>
                                                    <label class="col-md-8 label-control">{{$data->regions->region_name}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Area Name:</label>
                                                    <label class="col-md-8 label-control">{{$data->areas->area_name}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Route Name:</label>
                                                    <label class="col-md-8 label-control">{{$data->route_name}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Status:</label>
                                                    <label class="col-md-8 label-control">{{$data->status ?? old('status')}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary"><i class="ft-arrow-left"></i> Back To List</a>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="{{$singleSection}}-table" class="table table-striped table-bordered text-inputs-searching">
                                            <thead>
                                                <tr>
                                                    <th>Zone</th>
                                                    <th>Region</th>
                                                    <th>Area</th>
                                                    <th>Name</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Zone</th>
                                                    <th>Region</th>
                                                    <th>Area</th>
                                                    <th>Name</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                @if(isset($data) && count($data) > 0)
                                                    @foreach($data as $key => $row)

                                                        <tr>
                                                            <td>{{ $row->zones->zone_name ?? '' }}</td>
                                                            <td>{{ $row->regions->region_name ?? '' }}</td>            
                                                            <td>{{ $row->areas->area_name ?? '' }}</td>
                                                            <td>{{ $row->route_name }}</td>
                                                            <td>{{ $row->status }}</td>
                                                            <td>
                                                                <a href="{{ url( $sectionSlug , [ 'view' , $row->id] ) }}" title="View"><i class="ft-eye"></i></a> | 
                                                                <a href="{{ url( $sectionSlug , [ 'edit' , $row->id ] ) }}" title="Edit"><i class="la la-pencil"></i></a> |
                                                                <a href="{{ url( $sectionSlug ,[ $row->id , $row->is_active, 'status']) }}" title="Change Status" >
                                                                    @if( $row->is_active == '0' )
                                                                        <i class="la la-check"></i>
                                                                    @else
                                                                        <i class="la la-times"></i>
                                                                    @endif
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="6" class="text-center">No data found.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('jspage')

<script type="text/javascript">
    $(document).ready(function(){
        @if(isset($view) && in_array($view, ['add','edit']))
            getRegionSelectOption($('#zone_id').val(),"{{url('admin/route/zone/region')}}");
            $('#zone_id').change(function() {
                getRegionSelectOption($(':selected').val(),"{{url('admin/route/zone/region')}}");
            });
            function getRegionSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'zone_id':val}).done(function(response) {
                    $('#region_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#region_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    $('#region_id').val("{{$data->region_id ?? ""}}");
                    getAreaSelectOption($('#region_id').val(),"{{url('admin/route/region/area')}}");
                });
            }

            $('#region_id').change(function() {
                getAreaSelectOption($('#region_id').val(),"{{url('admin/route/region/area')}}");
            });

            function getAreaSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'region_id':val}).done(function(response) {
                    $('#area_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#area_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    $('#area_id').val("{{$data->area_id ?? ""}}");
                });
            }
        @endif
    });
</script>
@stop

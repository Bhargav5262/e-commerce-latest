<?php

namespace App\Models\Team;

use Eloquent, Request;

class Team extends Eloquent {

	protected $table = 'mst_sales_team';

	public $timestamps = true;

	protected $fillable = [
		'sales_team_name',
		'emp_code',
		'mobile',
		'otp',
		'password',
		'email',
		'role_id',
		'zone_id',
		'region_id',
		'area_id',
		'route_id',
		'HQ',
		'user_img',
		'user_fcm',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $primaryKey = "sales_team_id";
	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

	public function role()
	{
		return $this->hasOne('App\Models\Team\UserRole','role_id','role_id');
	}
	public function hq()
	{
		return $this->hasOne('App\Models\Team\Headquarter','HQ_id','HQ');
	}
	public function zone()
	{
		return $this->hasOne('App\Models\Zone','id','zone_id');
	}
	public function region()
	{
		return $this->hasOne('App\Models\Region','id','region_id');
	}
	public function area(){
		return $this->hasOne('App\Models\Area','id','area_id');
	}
	public function route(){
		return $this->hasOne('App\Models\Route','id','route_id');
	}


}
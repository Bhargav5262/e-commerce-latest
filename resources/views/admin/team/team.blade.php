@extends('admin.app.index')

@section('title')
    {{$section ?? 'Admin'}}
@stop

@section('css')

@stop

@section('model')

@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row mb-1">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Master Management</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">{{$section}} Management</a></li>
                        <li class="breadcrumb-item"><a href="{{url($sectionSlug)}}">{{$section}}</a></li>
                        <li class="breadcrumb-item active">{{ isset($view) ? ucfirst($view) : 'List'}}</li>
                    </ol>
                </div>
            </div>
            <h3 class="content-header-title mb-0">{{$singleSection}}</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ isset($view) ? ucfirst($view) : 'List'}} {{$singleSection}}
                                @if ( isset($view) && in_array($view, ['add','edit','view']) )
                                    <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary pull-right"><i class="ft-arrow-left"></i> Back To List</a>
                                @else
                                    <a href="{{ url( $sectionSlug , 'add') }}" class="btn btn-primary pull-right"><i class="ft-plus"></i> Add {{ $singleSection }}</a>
                                @endif
                            </h4>
                        </div>    
                        @if ( isset($view) && in_array($view, ['add','edit']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST" name="team_form" id="team_form" class="form" action="{{url($sectionSlug.'/action/')}}/{{$view}}/{{isset($data->sales_team_id) ? $data->sales_team_id: '0'}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-body my-gallery">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="sales_team_name">Name</label>
                                                        <input type="text" name="sales_team_name" id="sales_team_name" value="{{$data->sales_team_name ?? old('sales_team_name')}}" class="form-control round" placeholder="Name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="emp_code">Code</label>
                                                        <input type="text" name="emp_code" id="emp_code" value="{{$data->emp_code ?? old('emp_code')}}" class="form-control round" placeholder="Code">
                                                        <input type="hidden" name="_emp_code" id="_emp_code" value="{{$data->emp_code ?? old('emp_code')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="mobile">Mobile</label>
                                                        <input type="text" name="mobile" id="mobile" value="{{$data->mobile ?? old('mobile')}}" class="form-control round" placeholder="Mobile">
                                                        <input type="hidden" name="_mobile" id="_mobile" value="{{$data->mobile ?? old('mobile')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="password">Password</label>
                                                        <input type="password" name="password" id="password" value="{{$data->password ?? old('password')}}" class="form-control round" placeholder="Password">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input type="email" name="email" id="email" value="{{$data->email ?? old('email')}}" class="form-control round" placeholder="Email">
                                                        <input type="hidden" name="_email" id="_email" value="{{$data->email ?? old('email')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="role_id">User Type</label>
                                                        <select class="form-control round" name="role_id" id="role_id">
                                                            <option value="">Select type</option>
                                                            @foreach($roles as $role)
                                                                <option value="{{ $role->role_id }}" @if(isset($data) && $role->role_id == $data->role_id) selected @endif>{{ $role->role_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="zone_id">Zone</label>
                                                        <select class="form-control round" name="zone_id" id="zone_id">
                                                            <option value="">Select Zone</option>
                                                            @foreach($zones as $zone)
                                                                <option value="{{ $zone->id }}" @if(isset($data) && $zone->id == $data->zone_id) selected @endif>{{ $zone->zone_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="region_id">Region</label>
                                                        <select class="form-control round" name="region_id" id="region_id">
                                                            <option value="">Select Region</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="area_id">Area</label>
                                                        <select class="form-control round" name="area_id" id="area_id">
                                                            <option value="">Select Area</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="route_id">Route</label>
                                                        <select class="form-control round" name="route_id[]" multiple="" id="route_id">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="HQ">Headquarter</label>
                                                        <select class="form-control round" name="HQ" id="HQ">
                                                            <option value="">Select Headquarter</option>
                                                            @foreach($headquarters as $headquarter)
                                                                <option value="{{ $headquarter->HQ_id }}" @if(isset($data) && $headquarter->HQ_id == $data->HQ) selected @endif>{{ $headquarter->HQ_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>    
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="is_active">Status</label>
                                                        <select class="form-control round" name="is_active" id="is_active">
                                                            <option value="1" @if(isset($data) && $data->is_active == "1") selected @endif>Active</option>
                                                            <option value="0" @if(isset($data) && $data->is_active == "0") selected @endif>Inactive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if ( isset($view) && in_array($view, ['add','edit']) )
                                            <div class="form-actions">
                                                <a href="{{url($sectionSlug)}}" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        @elseif ( isset($view) && in_array($view, ['view']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Name:</label>
                                                    <label class="col-md-8 label-control">{{$data->sales_team_name ?? ''}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Code:</label>
                                                    <label class="col-md-8 label-control">{{$data->emp_code ?? ''}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Mobile:</label>
                                                    <label class="col-md-8 label-control">{{$data->mobile ?? ''}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Password:</label>
                                                    <label class="col-md-8 label-control">XXXXXX</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Email:</label>
                                                    <label class="col-md-8 label-control">{{$data->email ?? ''}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">User Type:</label>
                                                    <label class="col-md-8 label-control">{{$data->role->name ?? ''}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Zone:</label>
                                                    <label class="col-md-8 label-control">{{$data->zone->zone_name ?? ''}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Region:</label>
                                                    <label class="col-md-8 label-control">{{$data->region->region_name ?? ''}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Area:</label>
                                                    <label class="col-md-8 label-control">{{$data->area->area_name ?? ''}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-2 label-control">Route:</label>
                                                    <label class="col-md-8 label-control">
                                                        @if(isset($routes) && !empty($routes))
                                                            @foreach($routes as $key => $row)
                                                                <a href="javascript:void(0);" class="btn btn-info round btn-min-width mr-1 mb-1">{{$row->route_name ?? ''}}</a>
                                                            @endforeach
                                                        @endif
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Headquarter:</label>
                                                    <label class="col-md-8 label-control">{{$data->hq->name ?? ''}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Status:</label>
                                                    <label class="col-md-8 label-control">{{$data->status ?? old('status')}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary"><i class="ft-arrow-left"></i> Back To List</a>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="{{$singleSection}}-table" class="table table-striped table-bordered text-inputs-searching">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Code</th>
                                                    <th>Mobile</th>
                                                    <th>Email</th>
                                                    <th>User Type</th>
                                                    <th>Zone</th>
                                                    <th>Region</th>
                                                    <th>Area</th>
                                                    <th>Routes</th>
                                                    <th>HQ</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Code</th>
                                                    <th>Mobile</th>
                                                    <th>Email</th>
                                                    <th>User Type</th>
                                                    <th>Zone</th>
                                                    <th>Region</th>
                                                    <th>Area</th>
                                                    <th>Routes</th>
                                                    <th>HQ</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                @if(isset($data) && count($data) > 0)
                                                    @foreach($data as $key => $row)
                                                        <tr>
                                                            <td>{{ $row->sales_team_name ?? ''}}</td>
                                                            <td>{{ $row->emp_code ?? ''}}</td>
                                                            <td>{{ $row->mobile ?? ''}}</td>
                                                            <td>{{ $row->email ?? ''}}</td>
                                                            <td>{{ $row->role->role_name ?? ''}}</td>
                                                            <td>{{ $row->zone->zone_name ?? ''}}</td>
                                                            <td>{{ $row->region->region_name ?? ''}}</td>
                                                            <td>{{ $row->area->area_name ?? ''}}</td>
                                                            <td>{{ _route($row->route_id) ?? ''}}</td>
                                                            <td>{{ $row->hq->HQ_name ?? ''}}</td>
                                                            <td>{{ $row->status ?? ''}}</td>
                                                            <td>
                                                                <a href="{{ url( $sectionSlug , [ 'view' , $row->sales_team_id] ) }}" title="View"><i class="ft-eye"></i></a> | 
                                                                <a href="{{ url( $sectionSlug , [ 'edit' , $row->sales_team_id ] ) }}" title="Edit"><i class="la la-pencil"></i></a> |
                                                                <a href="{{ url( $sectionSlug ,[ $row->sales_team_id , $row->is_active, 'status']) }}" title="Change Status" >
                                                                    @if( $row->is_active == '0' )
                                                                        <i class="la la-check"></i>
                                                                    @else
                                                                        <i class="la la-times"></i>
                                                                    @endif
                                                                </a>
                                                                <a href="{{ url('admin/users?zone='.$row->zone_id.'&region='.$row->region_id.'&area='.$row->area_id.'&route='.$row->route_id) }}" class="btn btn-warning btn-sm">Show users</a>
                                                            </td>    
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="12" class="text-center">No data found.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('jspage')
<script type="text/javascript">
    $(document).ready(function(){
       @if(isset($view) && in_array($view, ['add','edit']))
            getRegionSelectOption($('#zone_id').val(),"{{url('region')}}");
            $('#zone_id').change(function() {
                getRegionSelectOption($('#zone_id').val(),"{{url('region')}}");
            });
            function getRegionSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'zone_id':val}).done(function(response) {
                    $('#region_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#region_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    @if(isset($view) && in_array($view, ['edit']))
                        $('#region_id').val("{{$data->region_id ?? ""}}");
                    @endif
                    getAreaSelectOption($('#region_id').val(),"{{url('area')}}");    
                });
            }
            $('#region_id').change(function() {
                getAreaSelectOption($('#region_id').val(),"{{url('area')}}");
            });
            function getAreaSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'region_id':val}).done(function(response) {
                    $('#area_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#area_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    @if(isset($view) && in_array($view, ['edit']))
                        $('#area_id').val("{{$data->area_id ?? ""}}");
                    @endif
                    getRouteSelectOption($('#area_id').val(),"{{url('getroute')}}");
                });
            }
            $('#area_id').change(function() {
                getRouteSelectOption($('#area_id').val(),"{{url('getroute')}}");
            });
            function getRouteSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'area_id':val}).done(function(response) {
                    //$('#route_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#route_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    @if(isset($view) && in_array($view, ['edit']))
                        var string = "{{$data->route_id ?? ""}}";
                        var opts = string.split(',');
                        $('#route_id').val(opts); 
                    @endif
                });
            }
        @endif
    });
</script>
@stop

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <base href="{{ url('/') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{$SITE_NAME}} :: Seller</title>
    <link rel="apple-touch-icon" href="{{$ADMIN_THEME_PATH}}/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{$ADMIN_THEME_PATH}}/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/css/pages/login-register.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link media="all" type="text/css" rel="stylesheet" href="{{$ADMIN_THEME_PATH}}/plugins/animate.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="{{$ADMIN_THEME_PATH}}/plugins/noty/noty.css">
    <link media="all" type="text/css" rel="stylesheet" href="{{$ADMIN_THEME_PATH}}/plugins/ladda/ladda.min.css">
    <link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu horizontal-menu-padding 1-column   blank-page" data-open="click" data-menu="horizontal-menu" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content container center-layout mt-2">
        <div class="content-wrapper">
            <div class="content-header row mb-1">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <img src="{{$ADMIN_THEME_PATH}}/app-assets/images/logo/logo-dark.png" alt="branding logo">
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>We will send you a link to reset password.</span></h6>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" id="forgotpasswordform" name="forgotpassword-form" action="{{url('seller/forgotpassword')}}" method="POST">
                                        {{ csrf_field() }}
                                            <fieldset class="form-group position-relative has-icon-left mb-1">
                                                <input type="text" name="mobile" id="mobile" class="form-control form-control-lg input-lg" placeholder="Enter Email or Number">
                                                <div class="form-control-position">
                                                    <i class="ft-mail"></i>
                                                </div>
                                            </fieldset>
                                            <button class="btn btn-info btn-lg btn-block"><i class="ft-unlock"></i> Recover Password</button>
                                        </form>
                                    </div>
                                    <p class="text-center"><a href="{{ url('seller/login') }}" class="card-link">Login Here!</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{$ADMIN_THEME_PATH}}/app-assets/js/core/app-menu.js"></script>
    <script src="{{$ADMIN_THEME_PATH}}/app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/plugins/ladda/spin.min.js"></script>
    <script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/plugins/ladda/ladda.min.js"></script>
    <script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/plugins/noty/noty.min.js"></script>

    <script type="text/javascript" src="{{$ADMIN_THEME_PATH}}/js/sellerlogin.js"></script>
    <!-- END: Page JS-->
    <script type="text/javascript">
    @if ($success = Session::get('success'))
        cp.notifyWithtEle("{{$success}}",'success');
    @endif
    @if ($error = Session::get('error'))
        cp.notifyWithtEle("{{$error}}",'error');
    @endif
    @if ($warning = Session::get('warning'))
        cp.notifyWithtEle("{{$warning}}",'warning');
    @endif

    //$(window).on('load', function(){$('.pageloader').hide();});
    </script>
</body>
<!-- END: Body-->

</html>
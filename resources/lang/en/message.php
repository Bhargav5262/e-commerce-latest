<?php

return [

    /*
    |--------------------------------------------------------------------------
    |   Section Confirmation Messages
    |--------------------------------------------------------------------------
    */
    
    
    'keyExist'          => ':section Already Exist Please Use Different.',
    
    'detailAdded'       => ':section details has been successfully inserted.',
    'detailUpdated'     => ':section details has been successfully updated.',    
    'detailDeleted'     => ':section details has been successfully deleted.',
    'somethingWrong'    => 'Something Went Wrong',
    'passwordNotMatch'  => 'Password and confirm password did not match.',
    'emailAndPassword'  => 'Email Address and Password does not match.',
    'checkEmail'        => 'Reset password link successfully send in mail.',
    'emailNotExist'     => 'Your email address doesn’t exist in our system.',
    'mobileNotExist'     => 'Your mobile doesn’t exist in our system.',
    'emailMobileNotValid'     => 'Please enter valid email or number.',
    'requestExpired'    => 'Your reset password request has expired.',
    'reseAccount'       => 'Your account password has been reset successfully.',
    'adminNotExist'     => 'Admin dose not exist.',
    'invalidOTP'        =>  'Please enter valid OTP.',
    'requiredOTP'       =>  'Please enter OTP.',
    'checkSMS'          => 'Reset password OTP successfully send in your mobile number.',
    

    'store'             => ':section has been added.',
    'update'            => ':section has been updated.',
    'statusActive'      => 'Status of :section has been changed to Active.',
    'statusInactive'    => 'Status of :section has been changed to Inactive.',
    'statusEnabled'      => 'Status of :section has been changed to Enabled.',
    'statusDisabled'    => 'Status of :section has been changed to Disabled.',
    'statusApproved'      => 'Status of :section has been changed to Approved.',
    'statusDisapproved'    => 'Status of :section has been changed to Disapproved.',
    'delete'            => ':section has been deleted.',
    'noRecords'         => 'No Records Found.',
    'sitemap'           => 'Sitemap has been generate successfully.',
];

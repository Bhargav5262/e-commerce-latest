@extends('seller.app.index')

@section('title')
    {{$section ?? 'Seller'}}
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/css/forms/tags/tagging.css">
<link href="{{$ADMIN_THEME_PATH}}/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
@stop

@section('model')

@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row mb-1">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('seller/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Manage {{$section}}</a></li>
                        <li class="breadcrumb-item active">{{ isset($view) ? ucfirst($view) : 'List'}}</li>
                    </ol>
                </div>
            </div>
            <h3 class="content-header-title mb-0">{{$section}}</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ isset($view) ? ucfirst($view) : 'List'}} {{$singleSection}}
                                @if ( isset($view) && in_array($view, ['add','edit','view','add new','edit draft','search']) )
                                    <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary pull-right"><i class="ft-arrow-left"></i> Back To List</a>
                                @else
                                    {{--<a href="{{ url( $sectionSlug , 'add') }}" class="btn btn-primary pull-right"><i class="ft-plus"></i> Add {{ $singleSection }}</a>--}}
                                @endif
                            </h4>
                        </div>    
                        @if ( isset($view) && in_array($view, ['search']) )
                            <div class="card-content collapse show">
                                <div class="card-body pb-0">
                                    <form method="GET" name="addproduct_form" id="addproduct_form" class="form" action="">
                                        <div class="row">
                                            <div class="col-md-5">    
                                                <fieldset class="form-group position-relative mb-0">
                                                    <input type="text" name="q" id="q" value="{{$data['q'] ?? old('q')}}" class="form-control form-control-md input-md" placeholder="Search by Name, Barcode/EAN NO, HSN No">
                                                    <div id="productsList"></div>  
                                                </fieldset>
                                                <div class="form-control-position" style="margin: -4px 0px 0px 24px">
                                                    <button type="submit" class="btn btn-primary"><i class="la la-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            {{-- <p class="text-muted font-small-3">About 68,00,000 results (0.58 seconds) </p> --}}
                                            <ul class="media-list p-0">
                                                @if(isset($products) && !empty($products) && count($products) > 0)
                                                    @foreach($products as $key => $row)
                                                    <li class="media">
                                                        <div class="media-left">
                                                            <a href="javascript:void(0);">
                                                                @if( isset($row->product_image->media) && $row->product_image->media != '' )
                                                                    <img class="media-object width-100" src="{{ $row->product_image->media }}" alt="">
                                                                @else
                                                                    <img class="media-object width-100" src="{{url('public/images/no-image.png')}}" alt="">
                                                                @endif    
                                                            </a>
                                                        </div>
                                                        <div class="media-body media-search">
                                                            <p class="lead mb-0"><a href="javascript:void(0);"><span class="text-bold-600">{{ $row->product_name ?? '' }}</span></a></p>
                                                            <ul class="list-inline list-inline-pipe text-muted">
                                                                <li><b> Barcode/EAN NO: {{ $row->barcode ?? '' }}</b></li>
                                                                <li><b> HSN No: {{ $row->hsn_no ?? '' }}</b></li>
                                                            </ul>
                                                            <a href="{{ url('seller/products/add', $row->hsn_no) }}" class="btn btn-primary btn-sm">Sell this product</a>
                                                        </div>
                                                    </li>
                                                    @endforeach    
                                                @else
                                                    @if(isset($data['q']) && !empty($data['q']))
                                                    <p class="lead mb-0">We could not find any products for: {{ $data['q'] }}</p>
                                                    @endif
                                                @endif
                                                <!--search with video-->
                                                {{-- <li class="media">
                                                    <div class="media-left media-search">
                                                        <iframe width="150" height="110" src="https://www.youtube.com/embed/SsE5U7ta9Lw?rel=0&amp;controls=0&amp;showinfo=0"></iframe>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="lead mb-0"><a href="#"><span class="text-bold-600">The Table</span> - for what do you feel you would bring to</a></p>
                                                        <p class="mb-0"><a href="#" class="teal darken-1">http://youtube.com/<span class="text-bold-600">modern</span>/ <i class="la la-angle-down" aria-hidden="true"></i></a></p>
                                                        <ul class="list-inline list-inline-pipe text-muted">
                                                            <li>Feb 3, 2018</li>
                                                            <li>1M Views</li>
                                                            <li>Uploaded by PlayStation</li>
                                                        </ul>
                                                        <p><span class="text-bold-600">Proceduralize</span> Not the long pole in my tent. Get buy-in pixel pushing, and quick win . What's the status on the deliverables for eow? goalposts golden goose, and take five.</p>
                                                    </div>
                                                </li> --}}
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @elseif ( isset($view) && in_array($view, ['add']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST" name="existproduct_form" id="existproduct_form" class="form" action="{{url($sectionSlug.'/action/')}}/{{str_replace(' ', '', $view)}}/{{isset($data->id) ? $data->id: '0'}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-body my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
                                            <h4 class="form-section"><i class="ft-image"></i> Product details</h4>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px;">
                                                                        @if( isset($defaultImage->media) && $defaultImage->media != '' )
                                                                        <img src="{{ $defaultImage->media }}" style="max-width: 200px;" alt= />
                                                                        @else
                                                                        <img src="{{url('public/images/no-image.png')}}" style="max-width: 200px;"  alt= />  
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group">
                                                                <label><b>{{ $data->product_name ?? '' }}</b></label>
                                                                <div class="card-text">
                                                                    <p><b>MRP: </b>{{ _price($data->vender_details->mrp) }}</p>
                                                                    <p><b> Barcode/EAN NO: </b>{{ $data->barcode ?? '' }}</p>
                                                                    <p><b> HSN No: </b>{{ $data->hsn_no ?? '' }}</p>
                                                                    <p><a href="javascript:void(0);">View Listings on {{$SITE_NAME}}</a></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="mrp">MRP</label>
                                                        <input type="hidden" id="exmrp" value="{{$data->vender_details->mrp}}" min="1">
                                                        <input type="number" name="mrp" id="mrp" value="{{$data->mrp ?? old('mrp')}}" class="form-control round" min="1" placeholder="MRP">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="inv_qty">Quantity (Inventory)</label>
                                                        <input type="number" name="inv_qty" id="inv_qty" value="{{$data->inv_qty ?? old('inv_qty')}}" class="form-control round" placeholder="Quantity (Inventory)">
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section"><i class="ft-tag"></i> For Retailer - basic Cost (exclude GST)</h4>
                                            <div class="row">
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ret_minord_qnty">Min. Order Quantity</label>
                                                        <input type="number" name="ret_minord_qnty" id="ret_minord_qnty" value="{{$data->ret_minord_qnty ?? old('ret_minord_qnty')}}" class="form-control round" placeholder="Min. Order Quantity">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ret_minord_qnty_cost_a">Cost for order 10</label>
                                                        <input type="number" name="ret_minord_qnty_cost_a" id="ret_minord_qnty_cost_a" value="{{$data->ret_minord_qnty_cost_a ?? old('ret_minord_qnty_cost_a')}}" class="form-control round" placeholder="Cost for order 10">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ret_minord_qnty_cost_b">Cost for order 20</label>
                                                        <input type="number" name="ret_minord_qnty_cost_b" id="ret_minord_qnty_cost_b" value="{{$data->ret_minord_qnty_cost_b ?? old('ret_minord_qnty_cost_b')}}" class="form-control round" placeholder="Cost for order 20">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ret_minord_qnty_cost_c">Cost for order 50</label>
                                                        <input type="number" name="ret_minord_qnty_cost_c" id="ret_minord_qnty_cost_c" value="{{$data->ret_minord_qnty_cost_c ?? old('ret_minord_qnty_cost_c')}}" class="form-control round" placeholder="Cost for order 50">
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section"><i class="ft-tag"></i> For Bulk - basic Cost (exclude GST)</h4>
                                            <div class="row">
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="whs_minord_qnty">Min. Order Quantity</label>
                                                        <input type="number" name="whs_minord_qnty" id="whs_minord_qnty" value="{{$data->whs_minord_qnty ?? old('whs_minord_qnty')}}" class="form-control round" placeholder="Min. Order Quantity">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="whs_minord_qnty_cost_a">Cost for order 10</label>
                                                        <input type="number" name="whs_minord_qnty_cost_a" id="whs_minord_qnty_cost_a" value="{{$data->whs_minord_qnty_cost_a ?? old('whs_minord_qnty_cost_a')}}" class="form-control round" placeholder="Cost for order 10">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="whs_minord_qnty_cost_b">Cost for order 20</label>
                                                        <input type="number" name="whs_minord_qnty_cost_b" id="whs_minord_qnty_cost_b" value="{{$data->whs_minord_qnty_cost_b ?? old('whs_minord_qnty_cost_b')}}" class="form-control round" placeholder="Cost for order 20">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="whs_minord_qnty_cost_c">Cost for order 50</label>
                                                        <input type="number" name="whs_minord_qnty_cost_c" id="whs_minord_qnty_cost_c" value="{{$data->whs_minord_qnty_cost_c ?? old('whs_minord_qnty_cost_c')}}" class="form-control round" placeholder="Cost for order 50">
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                        @if ( isset($view) && in_array($view, ['add','edit']) )
                                            <div class="form-actions">
                                                <a href="{{url($sectionSlug)}}" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        @endif
                                    </form>
                                </div>
                            </div>    
                        @elseif ( isset($view) && in_array($view, ['add new','edit draft', 'edit']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST" name="product_form" id="product_form" class="form" action="{{url($sectionSlug.'/action/')}}/{{str_replace(' ', '', $view)}}/{{isset($productVendor->id) ? $productVendor->id: '0'}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-body my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
                                            <div class="row">
                                                @if(isset($flag) && $flag == true)
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="cat_id">Category</label>
                                                            <select class="form-control round" name="cat_id" id="cat_id">
                                                                <option value="">Select category</option>
                                                                @foreach($category as $cat)
                                                                    <option value="{{ $cat->cat_id }}" @if(isset($data) && $cat->cat_id == $data->cat_id) selected @endif>{{ $cat->cat_name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>    
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="sub_cat_id">Sub Category</label>
                                                            <select class="form-control round" name="sub_cat_id" id="sub_cat_id">
                                                                <option value="">Select sub category</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    {{-- <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="child_cat_id">Child Category</label>
                                                            <select class="form-control round" name="child_cat_id" id="child_cat_id">
                                                                <option value="">Select child category</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="child_sub_cat_id">Child Sub Category</label>
                                                            <select class="form-control round" name="child_sub_cat_id" id="child_sub_cat_id">
                                                                <option value="">Select child sub category</option>
                                                            </select>
                                                        </div>
                                                    </div> --}}
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="brand_id">Brand *</label>
                                                            <select class="form-control round" name="brand_id" id="brand_id">
                                                                <option value="">Select Brand</option>
                                                                @if(isset($brands) && count($brands) > 0)
                                                                    @foreach($brands as $brand)
                                                                        <option value="{{ $brand->brand_id }}" @if(isset($data) && $brand->brand_id == $data->brand_id) selected @endif>{{ $brand->brand_name }}</option>
                                                                    @endforeach
                                                                @endif    
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="manufacturer">Manufacturer Name*</label>
                                                            <input type="text" name="manufacturer" id="manufacturer" value="{{$data->manufacturer ?? old('manufacturer')}}" class="form-control round" placeholder="manufacturer name">
                                                        </div>
                                                    </div>
                                                    {{-- <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="product_type">Products Type</label><br>
                                                            <div class="d-inline-block custom-control custom-radio mr-1">
                                                                <input type="radio" class="custom-control-input" name="product_type" value="1" @if(isset($data) && $data->product_type == "1") checked @endif id="radio1">
                                                                <label class="custom-control-label" for="radio1">Blubuck Products</label>
                                                            </div>
                                                            <div class="d-inline-block custom-control custom-radio mr-1">
                                                                <input type="radio" class="custom-control-input" name="product_type" value="2" @if(isset($data) && $data->product_type == "2") checked @endif id="radio2">
                                                                <label class="custom-control-label" for="radio2">Other Products</label>
                                                            </div>
                                                        </div>
                                                    </div> --}}
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="barcode">BARCODE/EAN NO*</label>
                                                            <input type="text" name="barcode" id="barcode" value="{{$data->barcode ?? old('barcode')}}" class="form-control round" placeholder="BARCODE/EAN NO">
                                                            <input type="hidden" name="_barcode" id="_barcode" value="{{$data->barcode ?? old('barcode')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="hsn_no">HSN No*</label>
                                                            <input type="text" name="hsn_no" id="hsn_no" value="{{$data->hsn_no ?? old('hsn_no')}}" class="form-control round" placeholder="HSN No">
                                                            <input type="hidden" name="_hsn_no" id="_hsn_no" value="{{$data->hsn_no ?? old('hsn_no')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">    
                                                        <div class="form-group">
                                                            <label for="product_name">ITEM FULL NAME*</label>
                                                            <input type="text" name="product_name" id="product_name" value="{{$data->product_name ?? old('product_name')}}" class="form-control round" placeholder="ITEM FULL NAME">
                                                            <input type="hidden" name="_product_name" id="_product_name" value="{{$data->product_name ?? old('product_name')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">    
                                                        <div class="form-group">
                                                            <label for="prod_desc">Product description*</label>
                                                            <textarea name="prod_desc" id="prod_desc" class="form-control round" placeholder="Product description">{{$data->prod_desc ?? old('prod_desc')}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="sku_size">Size (sku size)</label>
                                                            <input type="text" name="sku_size" id="sku_size" value="{{$data->sku_size ?? old('sku_size')}}" class="form-control round" placeholder="Size (sku size)">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="uom_id">UOM</label>
                                                            <select class="form-control round" name="uom_id" id="uom_id">
                                                                <option value="">Select UOM</option>
                                                                @foreach($uom as $row)
                                                                    <option value="{{ $row->id }}" @if(isset($data) && $row->id == $data->uom_id) selected @endif>{{ $row->uom_name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="gst">GST%</label>
                                                            <input type="text" name="gst" id="gst" value="{{$data->gst ?? old('gst')}}" class="form-control round" placeholder="GST%">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="igst">IGST</label>
                                                            <input type="text" name="igst" id="igst" value="{{$data->igst ?? old('igst')}}" class="form-control round" placeholder="IGST">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="sgst">SGST</label>
                                                            <input type="text" name="sgst" id="sgst" value="{{$data->sgst ?? old('sgst')}}" class="form-control round" placeholder="SGST">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="case_size">Case Size means no of unit in one case</label>
                                                            <input type="text" name="case_size" id="case_size" value="{{$data->case_size ?? old('case_size')}}" class="form-control round" placeholder="Case Size means no of unit in one case">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="case_weight">Case Weight(actual)</label>
                                                            <input type="text" name="case_weight" id="case_weight" value="{{$data->case_weight ?? old('case_weight')}}" class="form-control round" placeholder="Case Weight(actual)">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="case_dimen">Case dimension(volume) H*W*L</label>
                                                            <input type="text" name="case_dimen" id="case_dimen" value="{{$data->case_dimen ?? old('case_dimen')}}" class="form-control round" placeholder="Case dimension(volume) H*W*L">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="prod_weight">Product Weight(actual)</label>
                                                            <input type="text" name="prod_weight" id="prod_weight" value="{{$data->prod_weight ?? old('prod_weight')}}" class="form-control round" placeholder="Product Weight(actual)">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="prod_dimen">Product dimension(volume) H*W*L</label>
                                                            <input type="text" name="prod_dimen" id="prod_dimen" value="{{$data->prod_dimen ?? old('prod_dimen')}}" class="form-control round" placeholder="Product dimension(volume) H*W*L">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="shelf_life">Shelf Life In Days( day,month,Year )</label>
                                                            <input type="text" name="shelf_life" id="shelf_life" value="{{$data->shelf_life ?? old('shelf_life')}}" class="form-control round" placeholder="Shelf Life In Days( daya,month,Year )">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="prod_tag">Search Key word tag</label>
                                                            <div class="case-sensitive form-control round" data-tags-input-name="prod_tag" style="min-height: 40px;">{{$data->prod_tag ?? old('prod_tag')}}</div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px;">
                                                                            @if( isset($defaultImage->media) && $defaultImage->media != '' )
                                                                            <img src="{{ $defaultImage->media }}" style="max-width: 200px;" alt= />
                                                                            @else
                                                                            <img src="{{url('public/images/no-image.png')}}" style="max-width: 200px;"  alt= />  
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <label><b>{{ $data->product_name ?? '' }}</b></label>
                                                                    <div class="card-text">
                                                                        <p><b>MRP: </b>{{ _price($data->vender_details->mrp) }}</p>
                                                                        <p><b> Barcode/EAN NO: </b>{{ $data->barcode ?? '' }}</p>
                                                                        <p><b> HSN No: </b>{{ $data->hsn_no ?? '' }}</p>
                                                                        <p><a href="javascript:void(0);">View Listings on {{$SITE_NAME}}</a></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                @endif
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="mrp">MRP</label>
                                                        <input type="number" name="mrp" id="mrp" value="{{$productVendor->mrp ?? old('mrp')}}" class="form-control round" placeholder="MRP">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="inv_qty">Quantity (Inventory)</label>
                                                        <input type="number" name="inv_qty" id="inv_qty" value="{{$productVendor->inv_qty ?? old('inv_qty')}}" class="form-control round" placeholder="Quantity (Inventory)">
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section"><i class="ft-tag"></i> For Retailer - basic Cost (exclude GST)</h4>
                                            <div class="row">
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ret_minord_qnty">Min. Order Quantity</label>
                                                        <input type="number" name="ret_minord_qnty" id="ret_minord_qnty" value="{{$productVendor->ret_minord_qnty ?? old('ret_minord_qnty')}}" class="form-control round" placeholder="Min. Order Quantity">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ret_minord_qnty_cost_a">Cost for order 10</label>
                                                        <input type="number" name="ret_minord_qnty_cost_a" id="ret_minord_qnty_cost_a" value="{{$productVendor->ret_minord_qnty_cost_a ?? old('ret_minord_qnty_cost_a')}}" class="form-control round" placeholder="Cost for order 10">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ret_minord_qnty_cost_b">Cost for order 20</label>
                                                        <input type="number" name="ret_minord_qnty_cost_b" id="ret_minord_qnty_cost_b" value="{{$productVendor->ret_minord_qnty_cost_b ?? old('ret_minord_qnty_cost_b')}}" class="form-control round" placeholder="Cost for order 20">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ret_minord_qnty_cost_c">Cost for order 50</label>
                                                        <input type="number" name="ret_minord_qnty_cost_c" id="ret_minord_qnty_cost_c" value="{{$productVendor->ret_minord_qnty_cost_c ?? old('ret_minord_qnty_cost_c')}}" class="form-control round" placeholder="Cost for order 50">
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section"><i class="ft-tag"></i> For Bulk - basic Cost (exclude GST)</h4>
                                            <div class="row">
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="whs_minord_qnty">Min. Order Quantity</label>
                                                        <input type="number" name="whs_minord_qnty" id="whs_minord_qnty" value="{{$productVendor->whs_minord_qnty ?? old('whs_minord_qnty')}}" class="form-control round" placeholder="Min. Order Quantity">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="whs_minord_qnty_cost_a">Cost for order 10</label>
                                                        <input type="number" name="whs_minord_qnty_cost_a" id="whs_minord_qnty_cost_a" value="{{$productVendor->whs_minord_qnty_cost_a ?? old('whs_minord_qnty_cost_a')}}" class="form-control round" placeholder="Cost for order 10">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="whs_minord_qnty_cost_b">Cost for order 20</label>
                                                        <input type="number" name="whs_minord_qnty_cost_b" id="whs_minord_qnty_cost_b" value="{{$productVendor->whs_minord_qnty_cost_b ?? old('whs_minord_qnty_cost_b')}}" class="form-control round" placeholder="Cost for order 20">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="whs_minord_qnty_cost_c">Cost for order 50</label>
                                                        <input type="number" name="whs_minord_qnty_cost_c" id="whs_minord_qnty_cost_c" value="{{$productVendor->whs_minord_qnty_cost_c ?? old('whs_minord_qnty_cost_c')}}" class="form-control round" placeholder="Cost for order 50">
                                                    </div>
                                                </div>
                                            </div> 
                                            @if(isset($flag) && $flag == true)
                                                <h4 class="form-section"><i class="ft-image"></i> Catalog Media</h4>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label for="media_files">Default Media</label>
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px;">
                                                                            @if( isset($defaultImage->media) && $defaultImage->media != '' )
                                                                            <img src="{{ $defaultImage->media }}" style="max-width: 200px;" alt= />
                                                                            @else
                                                                            <img src="{{url('public/images/no-image.png')}}" style="max-width: 200px;"  alt= />  
                                                                            @endif
                                                                        </div>
                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select Image / Video </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="main_image" class="@if(isset($view) && in_array($view, ['edit draft','edit']) ) ignore @endif"> </span>
                                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <div class="form-group">
                                                                    <label>Your image recommendations</label>
                                                                    <div class="card-text">
                                                                        <p>1) Choose images that are clear, information-rich and attractive.</p>
                                                                        <p>2) Products must fill at least 85% of the image.</p>
                                                                        <p>3) Images must show only the product that is for sale, with few or no props and with no logos, watermarks or inset images.</p>
                                                                        <p>4) Images must be at least 1000 pixels on the longest side and at least 500 pixels on the shortest side to be zoom-able.</p>
                                                                        <p>5) Images must not exceed 10000 pixels on the longest side.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                                @if(isset($productImages) && count($productImages))
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                            @foreach($productImages as $key => $val)
                                                                <div class="col-md-3 trashmedia-{{$val->id}}">
                                                                    <a href="javascript:void(0);" class="pull-right" onclick="TrashMedia('{{$val->id}}');"><i class="ft-trash mr-1"></i></a>
                                                                    <div class="form-group">
                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                            <div class="fileinput-new thumbnail" style="width: 200px;">
                                                                                @if( isset($val->media) && $val->media != '' )
                                                                                    <img src="{{ $val->media }}" style="max-width: 200px;" alt= />
                                                                                @else
                                                                                    <img src="{{url('public/images/no-image.png')}}" style="max-width: 200px;" alt= />
                                                                                @endif    
                                                                            </div>
                                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>
                                                                            <div>
                                                                                <span class="btn default btn-file">
                                                                                <span class="fileinput-new"> Select Image / Video </span>
                                                                                <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="media_files[]" >
                                                                                <input type="hidden" name="ids[]" value="{{$key}}"></span>
                                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row multi-imgs">
                                                            <div class="col-md-3 single-img">
                                                                <div class="form-group">
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px;">
                                                                            <img src="{{url('public/images/no-image.png')}}" style="max-width: 200px;" alt= />
                                                                        </div>
                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select Image / Video </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="media_files[]" >
                                                                            <input type="hidden" name="ids[]" value=""></span>
                                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a href="javascript:void(0);" class="btn btn-info pull-left addmore">
                                                            <i class="ft-plus mr-1"></i> Add More Files
                                                        </a>
                                                    </div>
                                                </div>
                                            @endif
                                            @if ( isset($view) && in_array($view, ['add new','edit draft']) )
                                                <div class="row">
                                                    <div class="col-md-6">    
                                                        <div class="form-group">
                                                            <label for="is_active">Status</label>
                                                            <select class="form-control round" name="is_active" id="is_active">
                                                                <option value="4" @if(isset($productVendor) && $productVendor->is_active == "4") selected @endif>Save as Draft</option>
                                                                <option value="2" @if(isset($productVendor) && $productVendor->is_active == "2") selected @endif>Save and finish</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        @if ( isset($view) && in_array($view, ['add new','edit draft','edit']) )
                                            <div class="form-actions">
                                                <a href="{{url($sectionSlug)}}" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        @elseif ( isset($view) && in_array($view, ['draft']) )    
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="{{$singleSection}}-table" class="table table-striped table-bordered text-inputs-searching">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Brand</th>
                                                    <th>Product Code</th>
                                                    <th>Product Quantity</th>
                                                    <th>Retailer Min. Order Quantity</th>
                                                    <th>Retailer Cost for order 10</th>
                                                    <th>Retailer Cost for order 20</th>
                                                    <th>Retailer Cost for order 50</th>
                                                    <th>Bulk Min. Order Quantity</th>
                                                    <th>Bulk Cost for order 10</th>
                                                    <th>Bulk Cost for order 20</th>
                                                    <th>Bulk Cost for order 50</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Brand</th>
                                                    <th>Product Code</th>
                                                    <th>Product Quantity</th>
                                                    <th>Retailer Min. Order Quantity</th>
                                                    <th>Retailer Cost for order 10</th>
                                                    <th>Retailer Cost for order 20</th>
                                                    <th>Retailer Cost for order 50</th>
                                                    <th>Bulk Min. Order Quantity</th>
                                                    <th>Bulk Cost for order 10</th>
                                                    <th>Bulk Cost for order 20</th>
                                                    <th>Bulk Cost for order 50</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                @if(isset($data) && count($data) > 0)
                                                    @foreach($data as $key => $row)
                                                        <tr>
                                                            <td>
                                                                @if( isset($row->product->product_image->media) && $row->product->product_image->media != '' )
                                                                    <img class="media-object width-100" style="max-width: 80px; max-height: 80px;" src="{{ $row->product->product_image->media }}" alt="">
                                                                @else
                                                                    <img class="media-object width-100" style="max-width: 80px; max-height: 80px;" src="{{url('public/images/no-image.png')}}" alt="">
                                                                @endif
                                                            </td>
                                                            <td>{{ isset($row->product->product_name) ? $row->product->product_name : '' }}</td>
                                                            <td>{{ isset($row->product->category->cat_name) ? $row->product->category->cat_name : '' }}</td>
                                                            <td>{{ isset($row->product->brand->brand_name) ? $row->product->brand->brand_name : '' }}</td>
                                                            <td>{{ $row->product->barcode }}</td>
                                                            <td>{{ isset($row->product->vender_details->inv_qty) ? $row->product->vender_details->inv_qty : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->ret_minord_qnty) ? $row->product->vender_details->ret_minord_qnty : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->ret_minord_qnty_cost_a) ? $row->product->vender_details->ret_minord_qnty_cost_a : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->ret_minord_qnty_cost_b) ? $row->product->vender_details->ret_minord_qnty_cost_b : '' }}</td><td>{{ isset($row->product->vender_details->ret_minord_qnty_cost_c) ? $row->product->vender_details->ret_minord_qnty_cost_c : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->whs_minord_qnty) ? $row->product->vender_details->whs_minord_qnty : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->whs_minord_qnty_cost_a) ? $row->product->vender_details->whs_minord_qnty_cost_a : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->whs_minord_qnty_cost_b) ? $row->product->vender_details->whs_minord_qnty_cost_b : '' }}</td><td>{{ isset($row->product->vender_details->whs_minord_qnty_cost_c) ? $row->product->vender_details->whs_minord_qnty_cost_c : '' }}</td>
                                                            <td><a href="{{ url( $sectionSlug , [ 'editdraft' , $row->id ] ) }}"><i class="la la-pencil"></i></a></td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="9" class="text-center">No data found.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="{{$singleSection}}-table" class="table table-striped table-bordered text-inputs-searching">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Brand</th>
                                                    <th>Product Code</th>
                                                    <th>Product Quantity</th>
                                                    <th>Retailer Min. Order Quantity</th>
                                                    <th>Retailer Cost for order 10</th>
                                                    <th>Retailer Cost for order 20</th>
                                                    <th>Retailer Cost for order 50</th>
                                                    <th>Bulk Min. Order Quantity</th>
                                                    <th>Bulk Cost for order 10</th>
                                                    <th>Bulk Cost for order 20</th>
                                                    <th>Bulk Cost for order 50</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Brand</th>
                                                    <th>Product Code</th>
                                                    <th>Product Quantity</th>
                                                    <th>Retailer Min. Order Quantity</th>
                                                    <th>Retailer Cost for order 10</th>
                                                    <th>Retailer Cost for order 20</th>
                                                    <th>Retailer Cost for order 50</th>
                                                    <th>Bulk Min. Order Quantity</th>
                                                    <th>Bulk Cost for order 10</th>
                                                    <th>Bulk Cost for order 20</th>
                                                    <th>Bulk Cost for order 50</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                @if(isset($data) && count($data) > 0)
                                                    @foreach($data as $key => $row)
                                                        <tr>
                                                            <td>
                                                                @if( isset($row->product->product_image->media) && $row->product->product_image->media != '' )
                                                                    <img class="media-object width-100" style="max-width: 80px; max-height: 80px;" src="{{ $row->product->product_image->media }}" alt="">
                                                                @else
                                                                    <img class="media-object width-100" style="max-width: 80px; max-height: 80px;" src="{{url('public/images/no-image.png')}}" alt="">
                                                                @endif
                                                            </td>
                                                            <td>{{ isset($row->product->product_name) ? $row->product->product_name : '' }}</td>
                                                            <td>{{ isset($row->product->category->cat_name) ? $row->product->category->cat_name : '' }}</td>
                                                            <td>{{ isset($row->product->brand->brand_name) ? $row->product->brand->brand_name : '' }}</td>
                                                            <td>{{ $row->product->barcode }}</td>
                                                            <td>{{ isset($row->product->vender_details->inv_qty) ? $row->product->vender_details->inv_qty : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->ret_minord_qnty) ? $row->product->vender_details->ret_minord_qnty : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->ret_minord_qnty_cost_a) ? $row->product->vender_details->ret_minord_qnty_cost_a : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->ret_minord_qnty_cost_b) ? $row->product->vender_details->ret_minord_qnty_cost_b : '' }}</td><td>{{ isset($row->product->vender_details->ret_minord_qnty_cost_c) ? $row->product->vender_details->ret_minord_qnty_cost_c : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->whs_minord_qnty) ? $row->product->vender_details->whs_minord_qnty : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->whs_minord_qnty_cost_a) ? $row->product->vender_details->whs_minord_qnty_cost_a : '' }}</td>
                                                            <td>{{ isset($row->product->vender_details->whs_minord_qnty_cost_b) ? $row->product->vender_details->whs_minord_qnty_cost_b : '' }}</td><td>{{ isset($row->product->vender_details->whs_minord_qnty_cost_c) ? $row->product->vender_details->whs_minord_qnty_cost_c : '' }}</td>
                                                            <td>{{ $row->status ?? ''}}</td>
                                                            <td><a href="{{ url( $sectionSlug , [ 'edit' , $row->id ] ) }}"><i class="la la-pencil"></i></a>
                                                             @if( ($row->is_active == '0' || $row->is_active == '1') )
                                                                | <a href="{{ url( $sectionSlug ,[ $row->id , $row->is_active, 'status']) }}" title="Change Status for active or inactive" >
                                                                    @if( $row->is_active == '0' )
                                                                        <i class="la la-check"></i>
                                                                    @else
                                                                        <i class="la la-times"></i>
                                                                    @endif
                                                                </a>
                                                            @endif
                                                            | <a class="delete-icon" href="javascript:void(0);" onclick="showConfirmModal('{{ url( $sectionSlug ,[ 'action' , 'delete', $row->id]) }}')" data-toggle="tooltip" title="Delete"><i class="la la-trash"></i></a>   
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="16" class="text-center">No data found.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('jspage')
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/forms/tags/tagging.min.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // case sensitive
        $(".case-sensitive").tagging({
            "case-sensitive": false,
            "no-duplicate": false,
        });
        @if(isset($view) && in_array($view, ['add','edit','add new','edit draft']))
            getSubSelectOption($('#cat_id').val(),"{{url('sub-category')}}");
            $('#cat_id').change(function() {
                getSubSelectOption($('#cat_id').val(),"{{url('sub-category')}}");
            });
            function getSubSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'cat_id':val}).done(function(response) {
                    $('#sub_cat_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#sub_cat_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    @if(isset($view) && in_array($view, ['edit draft','edit']))
                        $('#sub_cat_id').val("{{$data->sub_cat_id ?? ""}}");
                    @endif
                    //getChildSelectOption($('#sub_cat_id').val(),"{{url('seller/sub-child-category/child-category')}}");    
                });
            }
            // $('#sub_cat_id').change(function() {
            //     getChildSelectOption($('#sub_cat_id').val(),"{{url('seller/sub-child-category/child-category')}}");
            // });
            // function getChildSelectOption(val,url) {
            //     cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'sub_cat_id':val}).done(function(response) {
            //         $('#child_cat_id option:gt(0)').remove();
            //         $.each(response, function(key, value) {
            //             $('#child_cat_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
            //         });
            //         @if(isset($view) && in_array($view, ['edit']))
            //             $('#child_cat_id').val("{{$data->child_cat_id ?? ""}}");
            //         @endif
            //         getChildSubSelectOption($('#child_cat_id').val(),"{{url('seller/sub-child-category/child-sub-category')}}");
            //     });
            // }
            // $('#child_cat_id').change(function() {
            //     getChildSubSelectOption($('#child_cat_id').val(),"{{url('seller/sub-child-category/child-sub-category')}}");
            // });
            // function getChildSubSelectOption(val,url) {
            //     cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'child_cat_id':val}).done(function(response) {
            //         $('#child_sub_cat_id option:gt(0)').remove();
            //         $.each(response, function(key, value) {
            //             $('#child_sub_cat_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
            //         });
            //         @if(isset($view) && in_array($view, ['edit']))
            //             $('#child_sub_cat_id').val("{{$data->child_sub_cat_id ?? ""}}");
            //         @endif
            //     });
            // }
        @endif
        $(".addmore").click(function(event) {
            var ele = $('.multi-imgs');
            var str = '<div class="col-md-3 single-img">'
                            +'<div class="form-group">'
                                +'<div class="fileinput fileinput-new" data-provides="fileinput">'
                                    +'<div class="fileinput-new thumbnail" style="width: 200px;">'
                                        +'<img src="{{url('public/images/no-image.png')}}" style="max-width: 200px;" alt= />'
                                    +'</div>'
                                    +'<div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>'
                                    +'<div>'
                                        +'<span class="btn default btn-file">'
                                        +'<span class="fileinput-new"> Select Image / Video </span>'
                                        +'<span class="fileinput-exists"> Change </span>'
                                        +'<input type="file" name="media_files[]" >'
                                        +'<input type="hidden" name="ids[]" value=""></span>'
                                        +'<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        +'</div>';
                ele.append(str);
        });
    });
    function TrashMedia(id) {
        cp_app.ajaxRequest("{{url('seller/products/trashmedia')}}",{_token:cp_token.getToken(),'id':id}).done(function(response) {
            $('.trashmedia-'+id).remove();
        });
    }
    $('#q').keyup(function(){  
           var query = $(this).val();  
           if(query != '')  
           {  
                cp_app.ajaxRequest("{{url('seller/products/autocomplete')}}",{_token:cp_token.getToken(),q:query}).done(function(response) {
                        $('#productsList').fadeIn();  
                        $('#productsList').html(response);
                    });  
           }  
      });  
      $(document).on('click', 'li', function(){  
           $('#q').val($(this).text());  
           $('#productsList').fadeOut();  
      }); 
</script>
@stop

<?php

namespace App\Models\Category;

use Eloquent, Request;

class Common extends Eloquent {

	protected $table = 'plt_common';

	public $timestamps = true;

	protected $fillable = [
		'plt_name',
		'plt_image',
		'plt_disp_val',
		'plt_val',
		'plt_disp_priority',
		'plt_data_type',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $primaryKey = "ptl_id";
	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

	protected $casts = ['ptl_img' => 'array'];

	// public function sub_category_active()
    // {
    //     return $this->hasMany('App\Models\Category\SubCategory','cat_id','cat_id')->where('is_active','1');
    // }

    // public function sub_category()
    // {
    //     return $this->hasMany('App\Models\Category\SubCategory','cat_id','cat_id');
    // }
}
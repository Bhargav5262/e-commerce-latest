<?php
namespace App\Http\Controllers\Admin;
use Request, Lang, File, Storage;
use App\Http\Controllers\Controller;
use App\Models\Region as RegionModel;
use App\Models\Zone as ZoneModel;

class Region extends Controller {

	protected $section;
	protected $singleSection;
    protected $viewPath;
    protected $sectionSlug;

	public function __construct(){
		$this->section = 'Region';
		$this->singleSection = 'Region';
        $this->viewPath = 'admin/region';
        $this->sectionSlug = 'admin/region';
	}

	public function index() {
		$data = RegionModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status, (CASE WHEN is_active = "0" THEN "1" WHEN is_active = "1" THEN "0" END) AS updatestatus')->orderBy('id',"DESC")->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"list",
			'data'=>$data,
		);
		return view($this->viewPath, $_data);
	}

	public function Add() {
		
	$zones = ZoneModel::where('is_active','1')->orderBy('zone_name','ASC')->get();

		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"add",
			'zones' => $zones,
		);
		return view($this->viewPath, $_data);
	}
	
	public function Edit($id="") {
        $data = RegionModel::where("id",$id)->first();
        if(isset($data) && !empty($data)):

    		$zones = ZoneModel::where('is_active','1')->orderBy('zone_name','ASC')->get();
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"edit",
	            'data'=>$data,
	            'zones' => $zones,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function View($id="") {
        $data = RegionModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->where("id",$id)->first();
        if(isset($data) && !empty($data)):

        	$zones = ZoneModel::where('is_active','1')->orderBy('zone_name','ASC')->get();
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"view",
	            'data'=>$data,
	            'zones' => $zones,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function Action($action="",$id="") {
		$post_data = Request::all();
		unset($post_data['_token']);
		$userId = 0;
		if( auth()->guard('admin')->check() ):
			$userId = auth()->guard('admin')->user()->id;
		endif;
		if($action=="add"):
			$existRegion = RegionModel::where('region_code',$post_data['region_code'])->get();
          	if(count($existRegion)):
            	return redirect($this->sectionSlug)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section.' Code' ])); 
          	endif;

          	$existRegion = RegionModel::where('region_name',$post_data['region_name'])->get();
          	if(count($existRegion)):
            	return redirect($this->sectionSlug)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section.' Name' ])); 
          	endif;
			
		    $post_data['created_by'] = $userId;
		    $post_data['updated_by'] = $userId;
            RegionModel::create($post_data);
        	return redirect($this->sectionSlug)->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ])); 
        elseif($action=="edit"):
        	$existRegion = RegionModel::where('region_code',$post_data['region_code'])->where('region_name',$post_data['region_name'])->where('id',"!=",$id)->get();
          	if(count($existRegion)): 
            	return redirect($this->sectionSlug.'/edit/'.$id)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section.' Name' ])); 
          	endif;
        	
        	$post_data['updated_by'] = $userId;
    		RegionModel::find($id)->update($post_data);
            return redirect($this->sectionSlug.'/edit/'.$id)->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ]));
		elseif($action=="delete"):
            $_data = RegionModel::where('id',$id)->first();
        	if(isset($_data)):
                $_data->delete();
                $arr = array("success"=>"true","message"=>Lang::get('message.detailDeleted', [ 'section' => $this->section ]));
            else:
                $arr = array("success"=>"false","message"=>"Something Went Wrong.");
            endif;
            return response()->json($arr);
        endif; 
    }
    
    public function status( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '0'];
			$message = Lang::get('message.statusInactive', ['section' => $this->section]);
		elseif($status == '0'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusActive', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			RegionModel::where('id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}

}

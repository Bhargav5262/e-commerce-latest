<?php
namespace App\Http\Controllers\Admin\Users;
use Request, Lang, File, Storage;
use App\Http\Controllers\Controller;
use App\Models\Users\Users as UsersModel;

class Users extends Controller {

	protected $section;
	protected $singleSection;
    protected $viewPath;
    protected $sectionSlug;

	public function __construct(){
		$this->section = 'Users';
		$this->singleSection = 'Users';
        $this->viewPath = 'admin/users/users';
        $this->sectionSlug = 'admin/users';
	}

	public function index() {
		$q_data = Request::all();
		$data = UsersModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Approved" WHEN is_active = "2" THEN "Request for review" WHEN is_active = "3" THEN "Disapproved" WHEN is_active = "4" THEN "Draft" END) AS status');
		if(isset($q_data['zone']) && !empty($q_data['zone'])):
			$data = $data->where(function($query)use($q_data){
	            $query->where('zone_id',$q_data['zone']);
	            if(isset($q_data['region']) && !empty($q_data['region'])):
		            $query->orWhere('region_id',$q_data['region']);
		        endif;
		        if(isset($q_data['area']) && !empty($q_data['area'])):
		            $query->orWhere('area_id',$q_data['area']);
		        endif;
	        });
		endif;
		if(isset($q_data['route']) && !empty($q_data['route'])):
        	$data = $data->where(function($query)use($q_data){
        		foreach (explode(',', $q_data['route']) as $key => $value):
					if($key == 0):
						$query->where('route_id', $value);
					else:
						$query->orWhere('route_id', $value);
					endif;
				endforeach;
			});
		endif;
		$data = $data->orderBy('user_id',"DESC")->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"list",
			'data'=>$data,
		);
		return view($this->viewPath, $_data);
	}

    public function status( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '0'];
			$message = Lang::get('message.statusInactive', ['section' => $this->section]);
		elseif($status == '0'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusActive', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			UsersModel::where('user_id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}

	public function applicationstatus( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusApproved', ['section' => $this->section]);
		elseif($status == '3'):
			$data = ['is_active' => '3'];
			$message = Lang::get('message.statusDisapproved', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			UsersModel::where('user_id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}

}

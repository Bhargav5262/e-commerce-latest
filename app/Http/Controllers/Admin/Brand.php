<?php
namespace App\Http\Controllers\Admin;
use Request, Lang, File, Storage;
use App\Http\Controllers\Controller;
use App\Models\Brand as BrandModel;

class Brand extends Controller {

	protected $section;
	protected $singleSection;
    protected $viewPath;
    protected $sectionSlug;

	public function __construct(){
		$this->section = 'Brand';
		$this->singleSection = 'Brand';
        $this->viewPath = 'admin/brand';
        $this->sectionSlug = 'admin/brand';
	}

	public function index() {
		$data = BrandModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status, (CASE WHEN is_active = "0" THEN "1" WHEN is_active = "1" THEN "0" END) AS updatestatus')->orderBy('brand_id',"DESC")->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"list",
			'data'=>$data,
		);
		return view($this->viewPath, $_data);
	}

	public function Add() {
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"add",
		);
		return view($this->viewPath, $_data);
	}
	
	public function Edit($id="") {
        $data = BrandModel::where('brand_id',$id)->first();
        if(isset($data) && !empty($data)):
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"edit",
	            'data'=>$data,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function View($id="") {
        $data = BrandModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->where('brand_id',$id)->first();
        if(isset($data) && !empty($data)):
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"view",
	            'data'=>$data,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function Action($action="",$id="") {
		$post_data = Request::all();
		unset($post_data['_token']);
		$userId = 0;
		if( auth()->guard('admin')->check() ):
			$userId = auth()->guard('admin')->user()->id;
		endif;
		if($action=="add"):
			$existBrand = BrandModel::where('brand_name',$post_data['brand_name'])->get();
          	if(count($existBrand)):
            	return redirect($this->sectionSlug)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section.' Name' ])); 
          	endif;
			##  Images
          	if(isset($post_data['brand_img_files']) && $post_data['brand_img_files'] != ''):
		        if( Request::file() ):
		            $images = Request::file('brand_img_files');
		            if($images):
		            	$imagesdata = array();
		            	foreach($images as $key => $row):
		            		$fileName = 'img_'.$userId.'_'.time().'_'.$row->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/brand/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($row));
           					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/brand/' . $fileName;
			                $imagesdata[] = _preg_replace($filePath);
		            	endforeach;	
			        	$post_data['brand_img'] = $imagesdata;
		            endif;
		        endif;
		    endif;
		    #end
		    ##  Banner
          	if(isset($post_data['brand_banner_files']) && $post_data['brand_banner_files'] != ''):
		        if( Request::file() ):
		            $images = Request::file('brand_banner_files');
		            if($images):
		            	$bannerdata = array();
		            	foreach($images as $key => $row):
		            		$fileName = 'banner_'.$userId.'_'.time().'_'.$row->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/brand/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($row));
           					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/brand/' . $fileName;
			                $bannerdata[] = _preg_replace($filePath);
		            	endforeach;	
			        	$post_data['brand_banner'] = $bannerdata;
		            endif;
		        endif;
		    endif;
		    #end
		    $post_data['created_by'] = $userId;
		    $post_data['updated_by'] = $userId;
            BrandModel::create($post_data);
        	return redirect($this->sectionSlug)->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ])); 
        elseif($action=="edit"):
        	$existBrand = BrandModel::where('brand_name',$post_data['brand_name'])->where('brand_id',"!=",$id)->get();
          	if(count($existBrand)): 
            	return redirect($this->sectionSlug.'/edit/'.$id)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section.' Name' ])); 
          	endif;
        	##  Images
          	if(isset($post_data['brand_img_files']) && $post_data['brand_img_files'] != ''):
		        if( Request::file() ):
		            $images = Request::file('brand_img_files');
		            if($images):
		            	$imagesdata = array();
		            	foreach($images as $key => $row):
		            		$fileName = 'img_'.$userId.'_'.time().'_'.$row->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/brand/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($row));
           					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/brand/' . $fileName;
			                $imagesdata[] = _preg_replace($filePath);
		            	endforeach;	
			        	$post_data['brand_img'] = $imagesdata;
		            endif;
		        endif;
		    endif;
		    #end
		    ##  Banner
          	if(isset($post_data['brand_banner_files']) && $post_data['brand_banner_files'] != ''):
		        if( Request::file() ):
		            $images = Request::file('brand_banner_files');
		            if($images):
		            	$bannerdata = array();
		            	foreach($images as $key => $row):
		            		$fileName = 'banner_'.$userId.'_'.time().'_'.$row->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/brand/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($row));
           					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/brand/' . $fileName;
			                $bannerdata[] = _preg_replace($filePath);
		            	endforeach;	
			        	$post_data['brand_banner'] = $bannerdata;
		            endif;
		        endif;
		    endif;
		    #end
        	$post_data['updated_by'] = $userId;
    		BrandModel::find($id)->update($post_data);
            return redirect($this->sectionSlug.'/edit/'.$id)->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ]));
		elseif($action=="delete"):
            $_data = BrandModel::where('brand_id',$id)->first();
        	if(isset($_data)):
                $_data->delete();
                $arr = array("success"=>"true","message"=>Lang::get('message.detailDeleted', [ 'section' => $this->section ]));
            else:
                $arr = array("success"=>"false","message"=>"Something Went Wrong.");
            endif;
            return response()->json($arr);
        endif; 
    }
    
    public function status( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '0'];
			$message = Lang::get('message.statusInactive', ['section' => $this->section]);
		elseif($status == '0'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusActive', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			BrandModel::where('brand_id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}

}

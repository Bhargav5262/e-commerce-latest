<?php
namespace App\Http\Controllers\Admin;
use Request, Lang, File, Storage;
use App\Http\Controllers\Controller;
use App\Models\Zone as ZoneModel;

class Zone extends Controller {

	protected $section;
	protected $singleSection;
    protected $viewPath;
    protected $sectionSlug;

	public function __construct(){
		$this->section = 'Zone';
		$this->singleSection = 'Zone';
        $this->viewPath = 'admin/zone';
        $this->sectionSlug = 'admin/zone';
	}

	public function index() {
		$data = ZoneModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status, (CASE WHEN is_active = "0" THEN "1" WHEN is_active = "1" THEN "0" END) AS updatestatus')->orderBy('id',"DESC")->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"list",
			'data'=>$data,
		);
		return view($this->viewPath, $_data);
	}

	public function Add() {
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"add",
		);
		return view($this->viewPath, $_data);
	}
	
	public function Edit($id="") {
        $data = ZoneModel::where("id",$id)->first();
        if(isset($data) && !empty($data)):
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"edit",
	            'data'=>$data,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function View($id="") {
        $data = ZoneModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->where("id",$id)->first();
        if(isset($data) && !empty($data)):
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"view",
	            'data'=>$data,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function Action($action="",$id="") {
		$post_data = Request::all();
		unset($post_data['_token']);
		$userId = 0;
		if( auth()->guard('admin')->check() ):
			$userId = auth()->guard('admin')->user()->id;
		endif;
		if($action=="add"):
          	$existZone = ZoneModel::where('zone_name',$post_data['zone_name'])->get();
          	if(count($existZone)):
            	return redirect($this->sectionSlug)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section.' Name' ])); 
          	endif;
			
		    $post_data['created_by'] = $userId;
		    $post_data['updated_by'] = $userId;
            ZoneModel::create($post_data);
        	return redirect($this->sectionSlug)->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ])); 
        elseif($action=="edit"):
        	$existZone = ZoneModel::where('zone_name',$post_data['zone_name'])->where('id',"!=",$id)->get();
          	if(count($existZone)): 
            	return redirect($this->sectionSlug.'/edit/'.$id)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section.' Name' ])); 
          	endif;
        	
        	$post_data['updated_by'] = $userId;
    		ZoneModel::find($id)->update($post_data);
            return redirect($this->sectionSlug.'/edit/'.$id)->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ]));
		elseif($action=="delete"):
            $_data = ZoneModel::where('id',$id)->first();
        	if(isset($_data)):
                $_data->delete();
                $arr = array("success"=>"true","message"=>Lang::get('message.detailDeleted', [ 'section' => $this->section ]));
            else:
                $arr = array("success"=>"false","message"=>"Something Went Wrong.");
            endif;
            return response()->json($arr);
        endif; 
    }

    public function status( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '0'];
			$message = Lang::get('message.statusInactive', ['section' => $this->section]);
		elseif($status == '0'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusActive', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			ZoneModel::where('id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}

}

<?php

namespace App\Models\Users;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Authenticatable {

	use SoftDeletes;

	protected $table = 'mst_user';

	public $timestamps = true;

	protected $fillable = [
		'name',
		'mobile',
		'password',
		'remember_token',
		'otp',
		'otp_verified',
		'altr_mob_no',
		'whatsapp_mob_no',
		'email',
		'profile',
		'seller_type',
		'buyer_type',
		'buyer_img',
		'buyer_fcm',
		'role_id',
		'gst_no',
		'comp_name',
		'geo_loc',
		'zone_id',
		'region_id',
		'area_id',
		'route_id',
		'is_active',
		'created_by',
		'updated_by',
	];
	protected $primaryKey = "user_id";
	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
	protected $dates = ['deleted_at'];

	public function role()
	{
		return $this->hasOne('App\Models\Team\UserRole','id','role_id');
	}
	public function zone()
	{
		return $this->hasOne('App\Models\Zone','id','zone_id');
	}
	public function region()
	{
		return $this->hasOne('App\Models\Region','id','region_id');
	}
	public function area(){
		return $this->hasOne('App\Models\Area','id','area_id');
	}
	public function route(){
		return $this->hasOne('App\Models\Route','id','route_id');
	}
}
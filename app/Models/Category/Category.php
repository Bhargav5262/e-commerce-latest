<?php

namespace App\Models\Category;

use Eloquent, Request;

class Category extends Eloquent {

	protected $table = 'mst_cat';

	public $timestamps = true;

	protected $fillable = [
		'cat_name',
		'cat_img',
		'cat_banner',
		'cat_dec',
		'order_seq',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $primaryKey = "cat_id";
	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

	protected $casts = ['cat_img' => 'array','cat_banner' => 'array'];

	public function sub_category_active()
    {
        return $this->hasMany('App\Models\Category\SubCategory','cat_id','cat_id')->where('is_active','1');
    }

    public function sub_category()
    {
        return $this->hasMany('App\Models\Category\SubCategory','cat_id','cat_id');
    }
}
<?php
namespace App\Http\Controllers\Seller\Products;
use Request, Lang, File, Storage;
use App\Http\Controllers\Controller;
use App\Models\Category\Category as CategoryModel;
use App\Models\Brand as BrandModel;
use App\Models\Products\Products as ProductsModel;
use App\Models\Products\ProductsUOM as ProductsUOMModel;
use App\Models\Products\ProductsMedia as ProductsMediaModel;
use App\Models\Products\ProductsVendors as ProductsVendorsModel;

class Products extends Controller {

	protected $section;
	protected $singleSection;
    protected $viewPath;
    protected $sectionSlug;

	public function __construct(){
		$this->section = 'Products';
		$this->singleSection = 'Product';
        $this->viewPath = 'seller/products/products';
        $this->sectionSlug = 'seller/products';
	}

	public function index() {
		$userId = 0;
		if( auth()->guard('seller')->check() ):
			$userId = auth()->guard('seller')->user()->user_id;
		endif;
		$data = ProductsVendorsModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Approved" WHEN is_active = "2" THEN "Request for review" WHEN is_active = "3" THEN "Disapproved" WHEN is_active = "4" THEN "Draft" END) AS status')->where('is_active', '!=', 4)->where('created_by',$userId)->orderBy('id',"DESC")->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"list",
			'data'=>$data,
		);
		return view($this->viewPath, $_data);
	}

	public function draft() {
		$userId = 0;
		if( auth()->guard('seller')->check() ):
			$userId = auth()->guard('seller')->user()->user_id;
		endif;
		$data = ProductsVendorsModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->where('is_active', 4)->where('created_by',$userId)->orderBy('id',"DESC")->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"draft",
			'data'=>$data,
		);
		return view($this->viewPath, $_data);
	}

	public function productSearch() {
		$data = Request::all();
		$products = null;
		$defaultImage = null;
		if(isset($data['q']) && !empty($data['q'])):
			$products = ProductsModel::query();
			$products = $products->where(function($query)use($data){
	            $query->where('barcode',$data['q']);
	            $query->orWhere('hsn_no',$data['q']);
	            $query->orWhereRaw('LOWER(product_name) LIKE ?',strtolower("%{$data['q']}%"));
	        })->get();
		endif;
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"search",
			'data'=>$data,
			'products'=>$products,
			'defaultImage'=>$defaultImage,
		);
		return view($this->viewPath, $_data);
	}

	public function autocomplete() {
		$data = Request::all();
		if(isset($data['q']) && !empty($data['q'])):
			$products = ProductsModel::query();
			$products = $products->where(function($query)use($data){
	            $query->where('is_active',1);
	            $query->where('barcode',$data['q']);
	            $query->orWhere('hsn_no',$data['q']);
	            $query->orWhereRaw('LOWER(product_name) LIKE ?',strtolower("%{$data['q']}%"));
	        })->get();
	        $output = '';
	        $output = '<ul class="list-unstyled">'; 
	        if(isset($products) && count($products) > 0):
	        	foreach ($products as $key => $row):
	        		$output .= '<li>'.$row->product_name.'</li>'; 
	        	endforeach;
	        else:
	        	 $output .= '<li>Product Not Found</li>';  
	    	endif;
	    	$output .= '</ul>';  
      		echo $output;
		endif;
	}

	public function Add($id="") {
		$userId = 0;
		if( auth()->guard('seller')->check() ):
			$userId = auth()->guard('seller')->user()->user_id;
		endif;
		$data = ProductsModel::query();
		$data = $data->where(function($query)use($id){
            $query->where('barcode',$id);
            $query->orWhere('hsn_no',$id);
        })->first();
		if(isset($data) && !empty($data) && isset($data->vender_details) && !empty($data->vender_details)):
			$existProduct = ProductsVendorsModel::where('prod_id',$data->vender_details->prod_id)->where('created_by',$userId)->get();
			if(count($existProduct)):
				return redirect($this->sectionSlug)->with( 'warning', 'Product Item Already Exists Please edit your product.'); 
			endif;
			$defaultImage = ProductsMediaModel::where(['prod_id'=>$data->id,'is_default'=>'1'])->first();
			$_data=array(
				'section'=>$this->section,
				'singleSection'=>$this->singleSection,
				'sectionSlug'=>$this->sectionSlug,
				'view'=>"add",
				'defaultImage'=>$defaultImage,
				'data'=>$data,
			);
			return view($this->viewPath, $_data);
		else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;	
	}
	
	public function Edit($id="") {
        $userId = 0;
		if( auth()->guard('seller')->check() ):
			$userId = auth()->guard('seller')->user()->user_id;
		endif;
        $productVendor = ProductsVendorsModel::where(['id'=>$id,'created_by'=>$userId])->first();
        if(isset($productVendor) && !empty($productVendor)):
        	$data = ProductsModel::where("id",$productVendor->prod_id)->first();
        	if(isset($productVendor) && !empty($productVendor) && isset($data) && !empty($data) && $productVendor->created_by == $data->created_by):
        		$flag = true;
        	else:
        		$flag = false;
        	endif;
        	$category = CategoryModel::where('is_active','1')->orderBy('cat_name','ASC')->get();
        	$brands = BrandModel::where('is_active','1')->orderBy('brand_name','ASC')->get();
        	$uom = ProductsUOMModel::where('is_active','1')->orderBy('uom_name','ASC')->get();
        	$defaultImage = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'1'])->first();
        	$productImages = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'0'])->get();
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"edit",
	            'data'=>$data,
	            'category' => $category,
	            'brands' => $brands,
	            'uom' => $uom,
	            'defaultImage' => $defaultImage,
	            'productImages' => $productImages,
	            'productVendor' => $productVendor,
	            'flag' => $flag,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function AddNew() {
		$category = CategoryModel::where('is_active','1')->orderBy('cat_name','ASC')->get();
		$brands = BrandModel::where('is_active','1')->orderBy('brand_name','ASC')->get();
		$uom = ProductsUOMModel::where('is_active','1')->orderBy('uom_name','ASC')->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"add new",
			'category' => $category,
			'brands' => $brands,
			'uom' => $uom,
			'flag' => true,
		);
		return view($this->viewPath, $_data);
	}

	public function EditDraft($id="") {
    	$userId = 0;
		if( auth()->guard('seller')->check() ):
			$userId = auth()->guard('seller')->user()->user_id;
		endif;
        $productVendor = ProductsVendorsModel::where(['id'=>$id,'created_by'=>$userId])->first();
        if(isset($productVendor) && !empty($productVendor)):
        	$data = ProductsModel::where("id",$productVendor->prod_id)->first();
        	if(isset($productVendor) && !empty($productVendor) && isset($data) && !empty($data) && $productVendor->created_by == $data->created_by):
        		$flag = true;
        	else:
        		$flag = false;
        	endif;
        	$category = CategoryModel::where('is_active','1')->orderBy('cat_name','ASC')->get();
        	$brands = BrandModel::where('is_active','1')->orderBy('brand_name','ASC')->get();
        	$uom = ProductsUOMModel::where('is_active','1')->orderBy('uom_name','ASC')->get();
        	$defaultImage = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'1'])->first();
        	$productImages = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'0'])->get();
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"edit draft",
	            'data'=>$data,
	            'category' => $category,
	            'brands' => $brands,
	            'uom' => $uom,
	            'defaultImage' => $defaultImage,
	            'productImages' => $productImages,
	            'productVendor' => $productVendor,
	            'flag' => $flag,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function View($id="") {
        $data = ProductsModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->where("id",$id)->first();
        if(isset($data) && !empty($data)):
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"view",
	            'data'=>$data,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function Action($action="",$id="") {
		$post_data = Request::all();
		unset($post_data['_token']);
		$userId = 0;
		if( auth()->guard('seller')->check() ):
			$userId = auth()->guard('seller')->user()->user_id;
		endif;
		if($action=="addnew"):
			$tags = [];
			if(isset($post_data['prod_tag']) && !empty($post_data['prod_tag'])):
				$customtags = _custom_tags($post_data['product_name'],$post_data['cat_id'],$post_data['sub_cat_id'],$post_data['brand_id'],$post_data['manufacturer']);
				$customtags = array_merge($customtags,$post_data['prod_tag']);
				$post_data['prod_tag'] = implode(', ', $customtags);
			else:
				$customtags = _custom_tags($post_data['product_name'],$post_data['cat_id'],$post_data['sub_cat_id'],$post_data['brand_id'],$post_data['manufacturer']);
				$post_data['prod_tag'] = implode(', ', $customtags);
			endif;
			## Start Default Media
          	if(isset($post_data['main_image']) && $post_data['main_image'] != ''):
		        if( Request::file('main_image') ):
		            $images = Request::file('main_image');
		            if($images):
		            	$fileName = 'img_'.$userId.'_'.time().'_'.$images->getClientOriginalName();
	            		$fileName = str_replace(' ', '_', $fileName);
		                $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
		                Storage::disk('s3')->put($filePath, file_get_contents($images));
	   					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
		                $media_data['prod_id'] = $product->id;
		                $media_data['media'] = $filePath;
		                $media_data['media_type'] = $userId;
		                $media_data['is_default'] = '1';
		                $existImage = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'1'])->first();
		                if(isset($existImage) && !empty($existImage)):
		                	Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $existImage->media));
		                	$existImage->update($media_data);
			            else:
		                	ProductsMediaModel::create($media_data);
			            endif;
		            endif;
		        endif;
		    endif;
		    # End Default Media
    		##  Images
          	if(isset($post_data['media_files']) && $post_data['media_files'] != ''):
		        $ids = $post_data['ids'];
				if(Request::file()):
					for ($i=0; $i < count($post_data['ids']); $i++):
						if(isset($post_data['media_files'][$i])):
							$productImages = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'0'])->where('id', $ids[$i])->first();
							if(isset($productImages)):
								if ( isset($productImages->media) && $productImages->media != '' ):
									$imageExists = $productImages->media;
									if( isset($imageExists) && $imageExists != '' ):
										Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $imageExists));
									endif;
								endif;
								$fileName = 'img_'.$userId.'_'.time().'_'.$post_data['media_files'][$i]->getClientOriginalName();
					     		$fileName = str_replace(' ', '_', $fileName);
					             $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
					             Storage::disk('s3')->put($filePath, file_get_contents($post_data['media_files'][$i]));
					  					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
					             $media_data['media'] = $filePath;
					             $media_data['media_type'] = $userId;
								$productImages->update($media_data);
							endif;
						endif;
						if(isset($post_data['media_files'][$i]) && $post_data['ids'][$i] == ""):
							$fileName = 'img_'.$userId.'_'.time().'_'.$post_data['media_files'][$i]->getClientOriginalName();
				     		$fileName = str_replace(' ', '_', $fileName);
				             $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
				             Storage::disk('s3')->put($filePath, file_get_contents($post_data['media_files'][$i]));
				  					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
				             $media_data['prod_id'] = $product->id;
				             $media_data['media'] = $filePath;
				             $media_data['media_type'] = $userId;
				             $media_data['is_default'] = '0';
				             $media_data['order_by'] = $i+1;
				             ProductsMediaModel::create($media_data);
						endif;
					endfor;
				endif;
		    endif;
		    #end
		    $post_data['created_by'] = $userId;
		    $post_data['updated_by'] = $userId;
            $product = ProductsModel::create($post_data);
            $vpost_data['prod_id'] = $product->id;
            $vpost_data['mrp'] = $post_data['mrp'];
    		$vpost_data['inv_qty'] = $post_data['inv_qty'];
    		$vpost_data['ret_minord_qnty'] = $post_data['ret_minord_qnty'];
    		$vpost_data['ret_minord_qnty_cost_a'] = $post_data['ret_minord_qnty_cost_a'];
    		$vpost_data['ret_minord_qnty_cost_b'] = $post_data['ret_minord_qnty_cost_b'];
    		$vpost_data['ret_minord_qnty_cost_c'] = $post_data['ret_minord_qnty_cost_c'];
    		$vpost_data['whs_minord_qnty'] = $post_data['whs_minord_qnty'];
    		$vpost_data['whs_minord_qnty_cost_a'] = $post_data['whs_minord_qnty_cost_a'];
    		$vpost_data['whs_minord_qnty_cost_b'] = $post_data['whs_minord_qnty_cost_b'];
    		$vpost_data['whs_minord_qnty_cost_c'] = $post_data['whs_minord_qnty_cost_c'];
    		if(isset($post_data['is_active'])):
    			$vpost_data['is_active'] = $post_data['is_active'];
    		endif;
    		$vpost_data['created_by'] = $userId;
		    $vpost_data['updated_by'] = $userId;
            ProductsVendorsModel::create($vpost_data);
            if(isset($post_data['is_active']) && $post_data['is_active'] == '4'):
        		return redirect($this->sectionSlug.'/draft')->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ]));
        	else:
            	return redirect($this->sectionSlug)->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ]));
            endif;	

        elseif($action=="add"):
			$post_data['created_by'] = $userId;
		    $post_data['updated_by'] = $userId;
		    $post_data['is_active'] = '1';
            $post_data['prod_id'] = $id;
            ProductsVendorsModel::create($post_data);
        	return redirect($this->sectionSlug)->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ])); 	 
        elseif($action=="edit"):
        	$productVendor = ProductsVendorsModel::where(['id'=>$id,'created_by'=>$userId])->first();
        	if(isset($productVendor) && !empty($productVendor)):
				$product = ProductsModel::where("id",$productVendor->prod_id)->first();
        		if(isset($product) && !empty($product) && $productVendor->created_by == $product->created_by):
	        		$flag = true;
	        		if(isset($post_data['prod_tag']) && !empty($post_data['prod_tag'])):
						$customtags = $post_data['prod_tag'];
						$post_data['prod_tag'] = implode(', ', $customtags);
					else:
						$customtags = _custom_tags($post_data['product_name'],$post_data['cat_id'],$post_data['sub_cat_id'],$post_data['brand_id'],$post_data['manufacturer']);
						$post_data['prod_tag'] = implode(', ', $customtags);
					endif;
		        	$post_data['updated_by'] = $userId;
		    		$product->update($post_data);
	        	endif;
	    		$post_data['updated_by'] = $userId;
	    		$vpost_data['mrp'] = $post_data['mrp'];
	    		$vpost_data['inv_qty'] = $post_data['inv_qty'];
	    		$vpost_data['ret_minord_qnty'] = $post_data['ret_minord_qnty'];
	    		$vpost_data['ret_minord_qnty_cost_a'] = $post_data['ret_minord_qnty_cost_a'];
	    		$vpost_data['ret_minord_qnty_cost_b'] = $post_data['ret_minord_qnty_cost_b'];
	    		$vpost_data['ret_minord_qnty_cost_c'] = $post_data['ret_minord_qnty_cost_c'];
	    		$vpost_data['whs_minord_qnty'] = $post_data['whs_minord_qnty'];
	    		$vpost_data['whs_minord_qnty_cost_a'] = $post_data['whs_minord_qnty_cost_a'];
	    		$vpost_data['whs_minord_qnty_cost_b'] = $post_data['whs_minord_qnty_cost_b'];
	    		$vpost_data['whs_minord_qnty_cost_c'] = $post_data['whs_minord_qnty_cost_c'];
	    		if(isset($post_data['is_active'])):
	    			$vpost_data['is_active'] = $post_data['is_active'];
	    		endif;	
	    		ProductsVendorsModel::where('id',$id)->update($vpost_data);
	    		## Start Default Media
	          	if(isset($post_data['main_image']) && $post_data['main_image'] != ''):
			        if( Request::file('main_image') ):
			            $images = Request::file('main_image');
			            if($images):
			            	$fileName = 'img_'.$userId.'_'.time().'_'.$images->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($images));
		   					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
			                $media_data['prod_id'] = $product->id;
			                $media_data['media'] = $filePath;
			                $media_data['media_type'] = $userId;
			                $media_data['is_default'] = '1';
			                $existImage = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'1'])->first();
			                if(isset($existImage) && !empty($existImage)):
			                	Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $existImage->media));
			                	$existImage->update($media_data);
				            else:
			                	ProductsMediaModel::create($media_data);
				            endif;
			            endif;
			        endif;
			    endif;
			    # End Default Media
	    		##  Images
	          	if(isset($post_data['media_files']) && $post_data['media_files'] != ''):
			        $ids = $post_data['ids'];
					if(Request::file()):
						for ($i=0; $i < count($post_data['ids']); $i++):
							if(isset($post_data['media_files'][$i])):
								$productImages = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'0'])->where('id', $ids[$i])->first();
								if(isset($productImages)):
									if ( isset($productImages->media) && $productImages->media != '' ):
										$imageExists = $productImages->media;
										if( isset($imageExists) && $imageExists != '' ):
											Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $imageExists));
										endif;
									endif;
									$fileName = 'img_'.$userId.'_'.time().'_'.$post_data['media_files'][$i]->getClientOriginalName();
						     		$fileName = str_replace(' ', '_', $fileName);
						             $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
						             Storage::disk('s3')->put($filePath, file_get_contents($post_data['media_files'][$i]));
						  					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
						             $media_data['media'] = $filePath;
						             $media_data['media_type'] = $userId;
									$productImages->update($media_data);
								endif;
							endif;
							if(isset($post_data['media_files'][$i]) && $post_data['ids'][$i] == ""):
								$fileName = 'img_'.$userId.'_'.time().'_'.$post_data['media_files'][$i]->getClientOriginalName();
					     		$fileName = str_replace(' ', '_', $fileName);
					             $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
					             Storage::disk('s3')->put($filePath, file_get_contents($post_data['media_files'][$i]));
					  					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
					             $media_data['prod_id'] = $product->id;
					             $media_data['media'] = $filePath;
					             $media_data['media_type'] = $userId;
					             $media_data['is_default'] = '0';
					             $media_data['order_by'] = $i+1;
					             ProductsMediaModel::create($media_data);
							endif;
						endfor;
					endif;
			    endif;
			    #end
        	endif;
            return redirect($this->sectionSlug.'/edit/'.$id)->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ]));
        elseif($action=="editdraft"):
        	$productVendor = ProductsVendorsModel::where(['id'=>$id,'created_by'=>$userId])->first();
        	if(isset($productVendor) && !empty($productVendor)):
				$product = ProductsModel::where("id",$productVendor->prod_id)->first();
        		if(isset($product) && !empty($product) && $productVendor->created_by == $product->created_by):
	        		$flag = true;
	        		if(isset($post_data['prod_tag']) && !empty($post_data['prod_tag'])):
						$customtags = $post_data['prod_tag'];
						$post_data['prod_tag'] = implode(', ', $customtags);
					else:
						$customtags = _custom_tags($post_data['product_name'],$post_data['cat_id'],$post_data['sub_cat_id'],$post_data['brand_id'],$post_data['manufacturer']);
						$post_data['prod_tag'] = implode(', ', $customtags);
					endif;
		        	$post_data['updated_by'] = $userId;
		    		$product->update($post_data);
	        	endif;
	    		$post_data['updated_by'] = $userId;
	    		$vpost_data['mrp'] = $post_data['mrp'];
	    		$vpost_data['inv_qty'] = $post_data['inv_qty'];
	    		$vpost_data['ret_minord_qnty'] = $post_data['ret_minord_qnty'];
	    		$vpost_data['ret_minord_qnty_cost_a'] = $post_data['ret_minord_qnty_cost_a'];
	    		$vpost_data['ret_minord_qnty_cost_b'] = $post_data['ret_minord_qnty_cost_b'];
	    		$vpost_data['ret_minord_qnty_cost_c'] = $post_data['ret_minord_qnty_cost_c'];
	    		$vpost_data['whs_minord_qnty'] = $post_data['whs_minord_qnty'];
	    		$vpost_data['whs_minord_qnty_cost_a'] = $post_data['whs_minord_qnty_cost_a'];
	    		$vpost_data['whs_minord_qnty_cost_b'] = $post_data['whs_minord_qnty_cost_b'];
	    		$vpost_data['whs_minord_qnty_cost_c'] = $post_data['whs_minord_qnty_cost_c'];
	    		$vpost_data['is_active'] = $post_data['is_active'];
	    		ProductsVendorsModel::where('id',$id)->update($vpost_data);
	    		## Start Default Media
	          	if(isset($post_data['main_image']) && $post_data['main_image'] != ''):
			        if( Request::file('main_image') ):
			            $images = Request::file('main_image');
			            if($images):
			            	$fileName = 'img_'.$userId.'_'.time().'_'.$images->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($images));
		   					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
			                $media_data['prod_id'] = $id;
			                $media_data['media'] = $filePath;
			                $media_data['media_type'] = $userId;
			                $media_data['is_default'] = '1';
			                $existImage = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'1'])->first();
			                if(isset($existImage) && !empty($existImage)):
			                	Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $existImage->media));
			                	$existImage->update($media_data);
				            else:
			                	ProductsMediaModel::create($media_data);
				            endif;
			            endif;
			        endif;
			    endif;
			    # End Default Media
	    		##  Images
	          	if(isset($post_data['media_files']) && $post_data['media_files'] != ''):
			        $ids = $post_data['ids'];
					if(Request::file()):
						for ($i=0; $i < count($post_data['ids']); $i++):
							if(isset($post_data['media_files'][$i])):
								$productImages = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'0'])->where('id', $ids[$i])->first();
								if(isset($productImages)):
									if ( isset($productImages->media) && $productImages->media != '' ):
										$imageExists = $productImages->media;
										if( isset($imageExists) && $imageExists != '' ):
											Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $imageExists));
										endif;
									endif;
									$fileName = 'img_'.$userId.'_'.time().'_'.$post_data['media_files'][$i]->getClientOriginalName();
						     		$fileName = str_replace(' ', '_', $fileName);
						             $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
						             Storage::disk('s3')->put($filePath, file_get_contents($post_data['media_files'][$i]));
						  					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
						             $media_data['media'] = $filePath;
						             $media_data['media_type'] = $userId;
									$productImages->update($media_data);
								endif;
							endif;
							if(isset($post_data['media_files'][$i]) && $post_data['ids'][$i] == ""):
								$fileName = 'img_'.$userId.'_'.time().'_'.$post_data['media_files'][$i]->getClientOriginalName();
					     		$fileName = str_replace(' ', '_', $fileName);
					             $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
					             Storage::disk('s3')->put($filePath, file_get_contents($post_data['media_files'][$i]));
					  					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
					             $media_data['prod_id'] = $id;
					             $media_data['media'] = $filePath;
					             $media_data['media_type'] = $userId;
					             $media_data['is_default'] = '0';
					             $media_data['order_by'] = $i+1;
					             ProductsMediaModel::create($media_data);
							endif;
						endfor;
					endif;
			    endif;
			    #end
        	endif;
        	if(isset($post_data['is_active']) && $post_data['is_active'] == '4'):
        		return redirect($this->sectionSlug.'/draft')->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ]));
        	else:
            	return redirect($this->sectionSlug)->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ]));
            endif;	
		elseif($action=="delete"):
            $_data = ProductsVendorsModel::where('id',$id)->first();
        	if(isset($_data)):
                $_data->delete();
                $arr = array("success"=>"true","message"=>Lang::get('message.detailDeleted', [ 'section' => $this->section ]));
            else:
                $arr = array("success"=>"false","message"=>"Something Went Wrong.");
            endif;
            return response()->json($arr);
        endif; 
    }
    
    public function status( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '0'];
			$message = Lang::get('message.statusInactive', ['section' => $this->section]);
		elseif($status == '0'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusActive', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			ProductsVendorsModel::where('id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}    

    public function trashmedia() {
        $post_data = Request::all();
        $arr = array("success"=>"false");
        $productImages = ProductsMediaModel::where(['id'=>$post_data['id'],'is_default'=>'0'])->first();
        if ( isset($productImages->media) && $productImages->media != '' ):
			$imageExists = $productImages->media;
			if( isset($imageExists) && $imageExists != '' ):
				Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $imageExists));
				$productImages->delete();
				$arr = array("success"=>"true");
			endif;
		endif;
		return response()->json($arr);
    }

}

@extends('admin.app.index')

@section('title')
    {{$section ?? 'Admin'}}
@stop

@section('css')

@stop

@section('model')

@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row mb-1">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Master Management</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">{{$section}} Management</a></li>
                        <li class="breadcrumb-item"><a href="{{url($sectionSlug)}}">{{$section}}</a></li>
                        <li class="breadcrumb-item active">{{ isset($view) ? ucfirst($view) : 'List'}}</li>
                    </ol>
                </div>
            </div>
            <h3 class="content-header-title mb-0">{{$singleSection}}</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ isset($view) ? ucfirst($view) : 'List'}} {{$singleSection}}
                                @if ( isset($view) && in_array($view, ['add','edit','view']) )
                                    <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary pull-right"><i class="ft-arrow-left"></i> Back To List</a>
                                @else
                                    <a href="{{ url( $sectionSlug , 'add') }}" class="btn btn-primary pull-right"><i class="ft-plus"></i> Add {{ $singleSection }}</a>
                                @endif
                            </h4>
                        </div>    
                        @if ( isset($view) && in_array($view, ['add','edit']) )
                            {{--<link href="{{$ADMIN_THEME_PATH}}/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />--}}
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST" name="subcategory_form" id="subcategory_form" class="form" action="{{url($sectionSlug.'/action/')}}/{{$view}}/{{isset($data->sub_cat_id) ? $data->sub_cat_id: '0'}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label for="cat_id">Category</label>
                                                <select class="form-control round" name="cat_id" id="cat_id">
                                                    <option value="">Select category</option>
                                                    @foreach($category as $cat)
                                                        <option value="{{ $cat->cat_id }}" @if(isset($data) && $cat->cat_id == $data->cat_id) selected @endif>{{ $cat->cat_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="sub_cat_name">Sub Category Name</label>
                                                <input type="text" name="sub_cat_name" id="sub_cat_name" value="{{$data->sub_cat_name ?? old('sub_cat_name')}}" class="form-control round" placeholder="sub category name">
                                                <input type="hidden" name="_sub_cat_name" id="_sub_cat_name" value="{{$data->sub_cat_name ?? ''}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="cat_dec">Description</label>
                                                <textarea name="cat_dec" id="cat_dec" class="form-control round" placeholder="Description">{{$data->cat_dec ?? old('cat_dec')}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="sub_cat_img_files">Sub Category Images</label>
                                                <input type="file" name="sub_cat_img_files[]" id="sub_cat_img_files" multiple="" class="form-control round @if(isset($view) && in_array($view, ['edit']) ) ignore @endif">
                                            </div>
                                            @if(isset($data->sub_cat_img) && !empty($data->sub_cat_img))
                                                <div class="row">
                                                    @foreach($data->sub_cat_img as $key => $row)
                                                        <figure class="col-lg-1 col-md-6 col-12">
                                                           <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                        </figure>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <label for="sub_cat_banner_files">Sub Category Banner</label>
                                                <input type="file" name="sub_cat_banner_files[]" id="sub_cat_banner_files" multiple="" class="form-control round @if(isset($view) && in_array($view, ['edit']) ) ignore @endif">
                                            </div>
                                            @if(isset($data->sub_cat_banner) && !empty($data->sub_cat_banner))
                                                <div class="row">
                                                    @foreach($data->sub_cat_banner as $key => $row)
                                                        <figure class="col-lg-1 col-md-6 col-12">
                                                           <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                        </figure>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <label for="order_seq">Order Seq </label>
                                                <input type="number" name="order_seq" id="order_seq" value="{{$data->order_seq ?? old('order_seq')}}" class="form-control round" placeholder="Order Seq   ">
                                            </div>
                                            <div class="form-group">
                                                <label for="is_active">Status</label>
                                                <select class="form-control round" name="is_active" id="is_active">
                                                    <option value="1" @if(isset($data) && $data->is_active == "1") selected @endif>Active</option>
                                                    <option value="0" @if(isset($data) && $data->is_active == "0") selected @endif>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                        @if ( isset($view) && in_array($view, ['add','edit']) )
                                            <div class="form-actions">
                                                <a href="{{url($sectionSlug)}}" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        @endif
                                    </form>
                                </div>
                            </div>      
                            {{--<script src="{{$ADMIN_THEME_PATH}}/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>--}}
                        @elseif ( isset($view) && in_array($view, ['view']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Category:</label>
                                                    <label class="col-md-8 label-control">{{$data->category->cat_name ?? ""}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Sub Category Name:</label>
                                                    <label class="col-md-8 label-control">{{$data->sub_cat_name ?? ""}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Description:</label>
                                                    <label class="col-md-8 label-control">{{$data->cat_dec ?? old('cat_dec')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Sub Category Images:</label>
                                                    @if(isset($data->sub_cat_img) && !empty($data->sub_cat_img))
                                                        <div class="row">
                                                            @foreach($data->sub_cat_img as $key => $row)
                                                                <figure class="col-lg-1 col-md-6 col-12">
                                                                   <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                                </figure>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Sub Category Banner:</label>
                                                    @if(isset($data->sub_cat_banner) && !empty($data->sub_cat_banner))
                                                        <div class="row">
                                                            @foreach($data->sub_cat_banner as $key => $row)
                                                                <figure class="col-lg-1 col-md-6 col-12">
                                                                   <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                                </figure>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Order Seq:</label>
                                                    <label class="col-md-8 label-control">{{$data->order_seq ?? old('order_seq')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Status:</label>
                                                    <label class="col-md-8 label-control">{{$data->status ?? ""}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary"><i class="ft-arrow-left"></i> Back To List</a>
                                    </div>
                                </div>
                            </div>    
                        @else
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered text-inputs-searching">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                @if(isset($data) && count($data) > 0)
                                                    @foreach($data as $key => $row)
                                                        <tr>
                                                            <td>{{ $row->sub_cat_name ?? ''}}</td>
                                                            <td>{{ $row->category->cat_name ?? '' }}</td>
                                                            <td>{{ $row->status }}</td>
                                                            <td>
                                                                <a href="{{ url( $sectionSlug , [ 'view' , $row->sub_cat_id] ) }}" title="View"><i class="ft-eye"></i></a> | 
                                                                <a href="{{ url( $sectionSlug , [ 'edit' , $row->sub_cat_id ] ) }}" title="Edit"><i class="la la-pencil"></i></a> |
                                                                <a href="{{ url( $sectionSlug ,[ $row->sub_cat_id , $row->is_active, 'status']) }}" title="Change Status" >
                                                                    @if( $row->is_active == '0' )
                                                                        <i class="la la-check"></i>
                                                                    @else
                                                                        <i class="la la-times"></i>
                                                                    @endif
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4" class="text-center">No data found.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('jspage')

@stop

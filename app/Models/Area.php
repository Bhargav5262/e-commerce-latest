<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class Area extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_area';

	public $timestamps = true;

	protected $fillable = [
		'region_id',
		'zone_id',
		'area_name',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $dates = ['deleted_at'];

	public function regions()
	{
		return $this->hasOne('App\Models\Region','id','region_id');
	}

	public function zones()
	{
		return $this->hasOne('App\Models\Zone','id','zone_id');
	}
}
$(function() {
    cp.init();
});
cp = {
    ajaxRequest: function (reqURL,reqData,reqType,reqDataType) {
        reqType = "";
        reqDataType = "";
        return $.ajax({
            url: reqURL,
            //headers: {"Access-Control-Allow-Origin": "*"},
            data: reqData,
            type: (reqType != "") ? reqType : "POST",
            datatype: (reqDataType != "") ? reqDataType : "application/json"
        });
    },
    notifyWithtEle: function(msg,type,pos="",timeout="") {
        var noty = new Noty({
            theme:'metroui',
            text: msg,
            type: type,
            layout: (pos != "") ? pos : 'topRight',
            timeout: (timeout != "") ? timeout : 1500,
            closeWith: ['click'],
            animation: {
                open: 'animated-slow fadeInRight',
                close: 'animated-slow fadeOutRight'
            }
        });
        noty.show();
    },
    init: function () {
        // var fdata = {
        //     "email":"lalit.nyusoft@gmail.com",
        //     "password":"123456"
        // };
        // cp.ajaxRequest("http://13.235.246.198/ikontra-web/api/login",fdata).done(function(data) {
        //     console.log(data);
        //     //l.stop();
        //     // if(data.success == "true"){
        //     //     cp.notifyWithtEle(data.message,'success');
        //     //     setTimeout(function(){window.location = data.redirect},1000);
        //     // }else{
        //     //     cp.notifyWithtEle(data.message,'error');
        //     // }
        // });

        // var fdata = {
        //     "fcm_token":"testting data",
        //     "pin":"123456",
        //     "mobile":9619026616
        // };
        // cp.ajaxRequest("http://13.234.178.43:3016/tra/usr/loginusr",fdata).done(function(data) {
        //     console.log(data);
        //     //l.stop();
        //     // if(data.success == "true"){
        //     //     cp.notifyWithtEle(data.message,'success');
        //     //     setTimeout(function(){window.location = data.redirect},1000);
        //     // }else{
        //     //     cp.notifyWithtEle(data.message,'error');
        //     // }
        // });
        $('#loginform').validate({
            rules: {
                mobile: {
                    required: true,
                },
                pin:{
                    required: true,
                },
            },
            messages: {
                mobile: {
                    required: "Please enter email or number."
                },
                pin:{
                    required: "Please enter password.",
                },
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function (form) {
                var l = Ladda.create($(form).find('button').get(0));
                l.start();
                cp.ajaxRequest($(form).attr("action"),$(form).serialize()).done(function(data) {
                    l.stop();
                    if(data.success == "true"){
                        cp.notifyWithtEle(data.message,'success');
                        setTimeout(function(){window.location = data.redirect},1000);
                    }else{
                        cp.notifyWithtEle(data.message,'error');
                    }
                });
                return false;
            }
        });
        $('#forgotpasswordform').validate({
            rules: {
                email: {
                    required: true,
                }
            },
            messages: {
                email: {
                    required: "Please enter email or number."
                }
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form){
                var l = Ladda.create($(form).find('button').get(0));
                l.start();
                cp.ajaxRequest($(form).attr("action"),$(form).serialize()).done(function(data) {
                    l.stop();
                    if(data.success == "true"){
                        cp.notifyWithtEle(data.message,'success');
                        setTimeout(function(){window.location = data.redirect},1000);
                    }else{
                        cp.notifyWithtEle(data.message,'error');
                    }
                });
                return false;
            }
        });
        $('#confirmnewpasswordform').validate({
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                },
                confirmpassword: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    required: "Please enter new password.",
                    minlength: "Please enter minimum 6 character.",
                    maxlength: "Please enter maximum 20 character.",
                },
                confirmpassword: {
                    required: "Please enter confirm password.",
                    minlength: "Please enter minimum 6 character.",
                    maxlength: "Please enter maximum 20 character.",
                    equalTo: "Please enter same as new password."
                },
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form){
                var l = Ladda.create($(form).find('button').get(0));
                l.start();
                form.submit();
            }
        });

        $('#verifyOTP').validate({
            ignore: '.ignore',
            rules: {
                otp: {
                    required: true,
                },
                mobile: {
                    required: true,
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                },
            },
            messages: {
                otp: {
                    required: "Please enter OTP.",
                },
                mobile: {
                    required: "Please enter mobile No.",
                },
                password: {
                    required: "Please enter password.",
                    minlength: "Please enter minimum 6 character.",
                    maxlength: "Please enter maximum 20 character.",
                },
            },
            errorPlacement: function(error, element) {
                if (element.attr("type") == "input") {
                    error.insertAfter(element.parent().parent());
                }else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                var l = Ladda.create($(form).find('button').get(0));
                l.start();
                cp.ajaxRequest($(form).attr("action"),$(form).serialize()).done(function(data) {
                    l.stop();
                    if(data.success == "true"){
                        cp.notifyWithtEle(data.message,'success');
                        setTimeout(function(){window.location = data.redirect},1000);
                    }else{
                        cp.notifyWithtEle(data.message,'error');
                    }
                });
                return false;
            }
        });

        $('#confirmOTPform').validate({
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                },
                confirmpassword: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    required: "Please enter new password.",
                    minlength: "Please enter minimum 6 character.",
                    maxlength: "Please enter maximum 20 character.",
                },
                confirmpassword: {
                    required: "Please enter confirm password.",
                    minlength: "Please enter minimum 6 character.",
                    maxlength: "Please enter maximum 20 character.",
                    equalTo: "Please enter same as new password."
                },
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form){
                var l = Ladda.create($(form).find('button').get(0));
                l.start();
                form.submit();
            }
        });
    }
};
$(function() {
    cp.init();
});
cp = {
    notifyWithtEle: function(msg,type,pos="",timeout="") {
        var noty = new Noty({
            theme:'metroui',
            text: msg,
            type: type,
            layout: (pos != "") ? pos : 'topRight',
            timeout: (timeout != "") ? timeout : 1500,
            closeWith: ['click'],
            animation: {
                open: 'animated-slow fadeInRight',
                close: 'animated-slow fadeOutRight'
            }
        });
        noty.show();
    },
    init: function () {
        $('#loginform').validate({
            rules: {
                email: {
                    required: true,
                },
                password:{
                    required: true,
                },
            },
            messages: {
                email: {
                    required: "Please enter email or number."
                },
                password:{
                    required: "Please enter password.",
                },
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form){
                var l = Ladda.create($(form).find('button').get(0));
                l.start();
                form.submit();
            }
        });
        $('#forgotpasswordform').validate({
            rules: {
                email: {
                    required: true,
                }
            },
            messages: {
                email: {
                    required: "Please enter email or number."
                }
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form){
                var l = Ladda.create($(form).find('button').get(0));
                l.start();
                form.submit();
            }
        });
        $('#confirmnewpasswordform').validate({
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                },
                confirmpassword: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    required: "Please enter new password.",
                    minlength: "Please enter minimum 6 character.",
                    maxlength: "Please enter maximum 20 character.",
                },
                confirmpassword: {
                    required: "Please enter confirm password.",
                    minlength: "Please enter minimum 6 character.",
                    maxlength: "Please enter maximum 20 character.",
                    equalTo: "Please enter same as new password."
                },
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form){
                var l = Ladda.create($(form).find('button').get(0));
                l.start();
                form.submit();
            }
        });

        $('#verifyOTP').validate({
            ignore: '.ignore',
            rules: {
                otp: {
                    required: true,
                }
            },
            messages: {
                otp: {
                    required: "Please enter OTP.",
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("type") == "input") {
                    error.insertAfter(element.parent().parent());
                }else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                if($(form).valid()){
                    var l = Ladda.create($(form).find('button').get(0));
                    l.start();
                    return true;
                }else{
                    return false;
                }
            }
        });

        $('#confirmOTPform').validate({
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                },
                confirmpassword: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    required: "Please enter new password.",
                    minlength: "Please enter minimum 6 character.",
                    maxlength: "Please enter maximum 20 character.",
                },
                confirmpassword: {
                    required: "Please enter confirm password.",
                    minlength: "Please enter minimum 6 character.",
                    maxlength: "Please enter maximum 20 character.",
                    equalTo: "Please enter same as new password."
                },
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function(form){
                var l = Ladda.create($(form).find('button').get(0));
                l.start();
                form.submit();
            }
        });
    }
};
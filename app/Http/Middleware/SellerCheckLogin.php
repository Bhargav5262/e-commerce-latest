<?php

namespace App\Http\Middleware;

use Closure;

class SellerCheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Cookie::get('seller_login') != '1'){
            return redirect('seller');
        }
        return $next($request);
    }
}

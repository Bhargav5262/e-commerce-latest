<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class EmailTemplateHeader extends Model {

	use SoftDeletes;
	protected $table = 'mst_email_template_header';

	public $timestamps = true;

	protected $fillable = [
		'title',
		'description',
		'status'
	];	
	protected $dates = ['deleted_at'];
}

<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class ProductsUOM extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_uom';

	public $timestamps = true;

	protected $fillable = [
		'uom_name',
		'is_active',
	];

	protected $dates = ['deleted_at'];
}
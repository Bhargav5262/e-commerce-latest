<?php
namespace App\Http\Controllers\Admin\Products;
use Request, Lang, File, Storage;
use App\Http\Controllers\Controller;
use App\Models\Category\Category as CategoryModel;
use App\Models\Brand as BrandModel;
use App\Models\Products\Products as ProductsModel;
use App\Models\Products\ProductsUOM as ProductsUOMModel;
use App\Models\Products\ProductsMedia as ProductsMediaModel;

class Products extends Controller {

	protected $section;
	protected $singleSection;
    protected $viewPath;
    protected $sectionSlug;

	public function __construct(){
		$this->section = 'Products';
		$this->singleSection = 'Product';
        $this->viewPath = 'admin/products/products';
        $this->sectionSlug = 'admin/products';
	}

	public function index() {
		$data = ProductsModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Approved" WHEN is_active = "2" THEN "Request for review" WHEN is_active = "3" THEN "Disapproved" WHEN is_active = "4" THEN "Draft" END) AS status')->where('is_active', '!=', 4)->orderBy('id',"DESC")->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"list",
			'data'=>$data,
		);
		return view($this->viewPath, $_data);
	}

	public function Add() {
		$category = CategoryModel::where('is_active','1')->orderBy('cat_name','ASC')->get();
		$brands = BrandModel::where('is_active','1')->orderBy('brand_name','ASC')->get();
		$uom = ProductsUOMModel::where('is_active','1')->orderBy('uom_name','ASC')->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"add",
			'category' => $category,
			'brands' => $brands,
			'uom' => $uom,
		);
		return view($this->viewPath, $_data);
	}
	
	public function Edit($id="") {
        $data = ProductsModel::where("id",$id)->first();
        if(isset($data) && !empty($data)):
        	$category = CategoryModel::where('is_active','1')->orderBy('cat_name','ASC')->get();
        	$brands = BrandModel::where('is_active','1')->orderBy('brand_name','ASC')->get();
        	$uom = ProductsUOMModel::where('is_active','1')->orderBy('uom_name','ASC')->get();
        	$defaultImage = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'1'])->first();
        	$productImages = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'0'])->get();
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"edit",
	            'data'=>$data,
	            'category' => $category,
	            'brands' => $brands,
	            'uom' => $uom,
	            'defaultImage' => $defaultImage,
	            'productImages' => $productImages,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function View($id="") {
        $data = ProductsModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->where("id",$id)->first();
        if(isset($data) && !empty($data)):
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	            'sectionSlug'=>$this->sectionSlug,
	            'view'=>"view",
	            'data'=>$data,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function Action($action="",$id="") {
		$post_data = Request::all();
		unset($post_data['_token']);
		$userId = 0;
		if( auth()->guard('admin')->check() ):
			$userId = auth()->guard('admin')->user()->id;
		endif;
		if($action=="add"):
			$tags = [];
			if(isset($post_data['prod_tag']) && !empty($post_data['prod_tag'])):
				$customtags = _custom_tags($post_data['product_name'],$post_data['cat_id'],$post_data['sub_cat_id'],$post_data['brand_id'],$post_data['manufacturer']);
				$customtags = array_merge($customtags,$post_data['prod_tag']);
				$post_data['prod_tag'] = implode(', ', $customtags);
			else:
				$customtags = _custom_tags($post_data['product_name'],$post_data['cat_id'],$post_data['sub_cat_id'],$post_data['brand_id'],$post_data['manufacturer']);
				$post_data['prod_tag'] = implode(', ', $customtags);
			endif;
			## Start Default Media
          	if(isset($post_data['main_image']) && $post_data['main_image'] != ''):
		        if( Request::file('main_image') ):
		            $images = Request::file('main_image');
		            if($images):
		            	$fileName = 'img_'.$userId.'_'.time().'_'.$images->getClientOriginalName();
	            		$fileName = str_replace(' ', '_', $fileName);
		                $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
		                Storage::disk('s3')->put($filePath, file_get_contents($images));
	   					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
		                $media_data['prod_id'] = $id;
		                $media_data['media'] = $filePath;
		                $media_data['media_type'] = $userId;
		                $media_data['is_default'] = '1';
		                $existImage = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'1'])->first();
		                if(isset($existImage) && !empty($existImage)):
		                	Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $existImage->media));
		                	$existImage->update($media_data);
			            else:
		                	ProductsMediaModel::create($media_data);
			            endif;
		            endif;
		        endif;
		    endif;
		    # End Default Media
    		##  Images
          	if(isset($post_data['media_files']) && $post_data['media_files'] != ''):
		        $ids = $post_data['ids'];
				if(Request::file()):
					for ($i=0; $i < count($post_data['ids']); $i++):
						if(isset($post_data['media_files'][$i])):
							$productImages = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'0'])->where('id', $ids[$i])->first();
							if(isset($productImages)):
								if ( isset($productImages->media) && $productImages->media != '' ):
									$imageExists = $productImages->media;
									if( isset($imageExists) && $imageExists != '' ):
										Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $imageExists));
									endif;
								endif;
								$fileName = 'img_'.$userId.'_'.time().'_'.$post_data['media_files'][$i]->getClientOriginalName();
					     		$fileName = str_replace(' ', '_', $fileName);
					             $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
					             Storage::disk('s3')->put($filePath, file_get_contents($post_data['media_files'][$i]));
					  					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
					             $media_data['media'] = $filePath;
					             $media_data['media_type'] = $userId;
								$productImages->update($media_data);
							endif;
						endif;
						if(isset($post_data['media_files'][$i]) && $post_data['ids'][$i] == ""):
							$fileName = 'img_'.$userId.'_'.time().'_'.$post_data['media_files'][$i]->getClientOriginalName();
				     		$fileName = str_replace(' ', '_', $fileName);
				             $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
				             Storage::disk('s3')->put($filePath, file_get_contents($post_data['media_files'][$i]));
				  					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
				             $media_data['prod_id'] = $id;
				             $media_data['media'] = $filePath;
				             $media_data['media_type'] = $userId;
				             $media_data['is_default'] = '0';
				             $media_data['order_by'] = $i+1;
				             ProductsMediaModel::create($media_data);
						endif;
					endfor;
				endif;
		    endif;
		    #end
		    // $post_data['created_by'] = $userId;
		    // $post_data['updated_by'] = $userId;
            ProductsModel::create($post_data);
        	return redirect($this->sectionSlug)->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ])); 
        elseif($action=="edit"):
        	if(isset($post_data['prod_tag']) && !empty($post_data['prod_tag'])):
				$customtags = $post_data['prod_tag'];
				$post_data['prod_tag'] = implode(', ', $customtags);
			else:
				$customtags = _custom_tags($post_data['product_name'],$post_data['cat_id'],$post_data['sub_cat_id'],$post_data['brand_id'],$post_data['manufacturer']);
				$post_data['prod_tag'] = implode(', ', $customtags);
			endif;
        	//$post_data['updated_by'] = $userId;
    		ProductsModel::find($id)->update($post_data);
    		## Start Default Media
          	if(isset($post_data['main_image']) && $post_data['main_image'] != ''):
		        if( Request::file('main_image') ):
		            $images = Request::file('main_image');
		            if($images):
		            	$fileName = 'img_'.$userId.'_'.time().'_'.$images->getClientOriginalName();
	            		$fileName = str_replace(' ', '_', $fileName);
		                $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
		                Storage::disk('s3')->put($filePath, file_get_contents($images));
	   					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
		                $media_data['prod_id'] = $id;
		                $media_data['media'] = $filePath;
		                $media_data['media_type'] = $userId;
		                $media_data['is_default'] = '1';
		                $existImage = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'1'])->first();
		                if(isset($existImage) && !empty($existImage)):
		                	Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $existImage->media));
		                	$existImage->update($media_data);
			            else:
		                	ProductsMediaModel::create($media_data);
			            endif;
		            endif;
		        endif;
		    endif;
		    # End Default Media
    		##  Images
          	if(isset($post_data['media_files']) && $post_data['media_files'] != ''):
		        $ids = $post_data['ids'];
				if(Request::file()):
					for ($i=0; $i < count($post_data['ids']); $i++):
						if(isset($post_data['media_files'][$i])):
							$productImages = ProductsMediaModel::where(['prod_id'=>$id,'is_default'=>'0'])->where('id', $ids[$i])->first();
							if(isset($productImages)):
								if ( isset($productImages->media) && $productImages->media != '' ):
									$imageExists = $productImages->media;
									if( isset($imageExists) && $imageExists != '' ):
										Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $imageExists));
									endif;
								endif;
								$fileName = 'img_'.$userId.'_'.time().'_'.$post_data['media_files'][$i]->getClientOriginalName();
					     		$fileName = str_replace(' ', '_', $fileName);
					             $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
					             Storage::disk('s3')->put($filePath, file_get_contents($post_data['media_files'][$i]));
					  					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
					             $media_data['media'] = $filePath;
					             $media_data['media_type'] = $userId;
								$productImages->update($media_data);
							endif;
						endif;
						if(isset($post_data['media_files'][$i]) && $post_data['ids'][$i] == ""):
							$fileName = 'img_'.$userId.'_'.time().'_'.$post_data['media_files'][$i]->getClientOriginalName();
				     		$fileName = str_replace(' ', '_', $fileName);
				             $filePath = '/'.env('S3_FOLDER').'/media/' . $fileName;
				             Storage::disk('s3')->put($filePath, file_get_contents($post_data['media_files'][$i]));
				  					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/media/' . $fileName;
				             $media_data['prod_id'] = $id;
				             $media_data['media'] = $filePath;
				             $media_data['media_type'] = $userId;
				             $media_data['is_default'] = '0';
				             $media_data['order_by'] = $i+1;
				             ProductsMediaModel::create($media_data);
						endif;
					endfor;
				endif;
		    endif;
		    #end
            return redirect($this->sectionSlug.'/edit/'.$id)->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ]));
		elseif($action=="delete"):
            $_data = ProductsModel::where('id',$id)->first();
        	if(isset($_data)):
                $_data->delete();
                $arr = array("success"=>"true","message"=>Lang::get('message.detailDeleted', [ 'section' => $this->section ]));
            else:
                $arr = array("success"=>"false","message"=>"Something Went Wrong.");
            endif;
            return response()->json($arr);
        endif; 
    }

    public function trashmedia() {
        $post_data = Request::all();
        $arr = array("success"=>"false");
        $productImages = ProductsMediaModel::where(['id'=>$post_data['id'],'is_default'=>'0'])->first();
        if ( isset($productImages->media) && $productImages->media != '' ):
			$imageExists = $productImages->media;
			if( isset($imageExists) && $imageExists != '' ):
				Storage::disk('s3')->delete(str_replace(env('S3_PATH').'/'.env('AWS_BUCKET'), '', $imageExists));
				$productImages->delete();
				$arr = array("success"=>"true");
			endif;
		endif;
		return response()->json($arr);
    }

    public function status( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '0'];
			$message = Lang::get('message.statusInactive', ['section' => $this->section]);
		elseif($status == '0'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusActive', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			ProductsModel::where('id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}

	public function applicationstatus( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusApproved', ['section' => $this->section]);
		elseif($status == '3'):
			$data = ['is_active' => '3'];
			$message = Lang::get('message.statusDisapproved', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			ProductsModel::where('id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}

}

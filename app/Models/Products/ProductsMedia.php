<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class ProductsMedia extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_catalog_media';

	public $timestamps = true;

	protected $fillable = [
		'prod_id',
		'media',
		'media_type',
		'is_default',
		'is_active',
	];

	protected $dates = ['deleted_at'];
}
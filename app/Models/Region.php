<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class Region extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_region';

	public $timestamps = true;

	protected $fillable = [
		'zone_id',
		'region_code',
		'region_name',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $dates = ['deleted_at'];

	public function zones()
	{
		return $this->hasOne('App\Models\Zone','id','zone_id');
	}
}
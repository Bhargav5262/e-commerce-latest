<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Request;
use App\Models\Category\SubCategory as SubCategoryModel;

class Common extends Controller
{
    public function subCategory(){
        $data = Request::all();
        $subcategory = SubCategoryModel::where('cat_id', $data['cat_id'])->where('is_active', 1)->orderBy('sub_cat_name','ASC')->get();
        $responseData = [];
        if(isset($subcategory) && count($subcategory)):
            foreach ($subcategory as $key => $val):
                $responseData[] = ['id'=>$val->sub_cat_id,'text'=>$val->sub_cat_name];
            endforeach;
        endif;
        return response()->json($responseData);
    }
}

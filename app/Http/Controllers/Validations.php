<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Products\Products as ProductsModel;
use App\Models\Team\Team as TeamModel;
use Illuminate\Http\Request;
use App\Models\Zone as ZoneModel;
use App\Models\Region as RegionModel;
use App\Models\Area as AreaModel;
use App\Models\Route as RouteModel;
use App\Models\Category\Category as CategoryModel;
use App\Models\Category\SubCategory as SubCategoryModel;
use App\Models\Category\ChildCategory as ChildCategoryModel;
use App\Models\Category\SubChildCategory as SubChildCategoryModel;
use App\Models\Brand as BrandModel;

class Validations extends Controller
{
    public function barcodeverify(Request $request)
    {
        if (isset($request->post('query')['_barcode']) && $request->post('query')['barcode'] == $request->post('query')['_barcode']):
            echo "true";
        else:
            $data = ProductsModel::where('barcode',$request->input('query')["barcode"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function hsnnoverify(Request $request)
    {
        if (isset($request->post('query')['_hsn_no']) && $request->post('query')['hsn_no'] == $request->post('query')['_hsn_no']):
            echo "true";
        else:
            $data = ProductsModel::where('hsn_no',$request->input('query')["hsn_no"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function productnameverify(Request $request)
    {
        if (isset($request->post('query')['_product_name']) && $request->post('query')['product_name'] == $request->post('query')['_product_name']):
            echo "true";
        else:
            $data = ProductsModel::where('product_name',$request->input('query')["product_name"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function teamcodeverify(Request $request)
    {
        if (isset($request->post('query')['_emp_code']) && $request->post('query')['emp_code'] == $request->post('query')['_emp_code']):
            echo "true";
        else:
            $data = TeamModel::where('emp_code',$request->input('query')["emp_code"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function teammobileverify(Request $request)
    {
        if (isset($request->post('query')['_mobile']) && $request->post('query')['mobile'] == $request->post('query')['_mobile']):
            echo "true";
        else:
            $data = TeamModel::where('mobile',$request->input('query')["mobile"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function teamemailverify(Request $request)
    {
        if (isset($request->post('query')['_email']) && $request->post('query')['email'] == $request->post('query')['_email']):
            echo "true";
        else:
            $data = TeamModel::where('email',$request->input('query')["email"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function categorynameverify(Request $request)
    {
        if (isset($request->post('query')['_cat_name']) && $request->post('query')['cat_name'] == $request->post('query')['_cat_name']):
            echo "true";
        else:
            $data = CategoryModel::where('cat_name',$request->input('query')["cat_name"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function subcategorynameverify(Request $request)
    {
        if (isset($request->post('query')['_sub_cat_name']) && $request->post('query')['sub_cat_name'] == $request->post('query')['_sub_cat_name']):
            echo "true";
        else:
            $data = SubCategoryModel::where('sub_cat_name',$request->input('query')["sub_cat_name"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function childcategorynameverify(Request $request)
    {
        if (isset($request->post('query')['_child_cat_name']) && $request->post('query')['child_cat_name'] == $request->post('query')['_child_cat_name']):
            echo "true";
        else:
            $data = ChildCategoryModel::where('child_cat_name',$request->input('query')["child_cat_name"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function subchildcategorynameverify(Request $request)
    {
        if (isset($request->post('query')['_sub_child_cat_name']) && $request->post('query')['sub_child_cat_name'] == $request->post('query')['_sub_child_cat_name']):
            echo "true";
        else:
            $data = SubChildCategoryModel::where('sub_child_cat_name',$request->input('query')["sub_child_cat_name"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function zonenameverify(Request $request)
    {
        if (isset($request->post('query')['_zone_name']) && $request->post('query')['zone_name'] == $request->post('query')['_zone_name']):
            echo "true";
        else:
            $data = ZoneModel::where('zone_name',$request->input('query')["zone_name"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function regionnameverify(Request $request)
    {
        if (isset($request->post('query')['_region_name']) && $request->post('query')['region_name'] == $request->post('query')['_region_name']):
            echo "true";
        else:
            $data = RegionModel::where('region_name',$request->input('query')["region_name"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function areanameverify(Request $request)
    {
        if (isset($request->post('query')['_area_name']) && $request->post('query')['area_name'] == $request->post('query')['_area_name']):
            echo "true";
        else:
            $data = AreaModel::where('area_name',$request->input('query')["area_name"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function routenameverify(Request $request)
    {
        if (isset($request->post('query')['_route_name']) && $request->post('query')['route_name'] == $request->post('query')['_route_name']):
            echo "true";
        else:
            $data = RouteModel::where('route_name',$request->input('query')["route_name"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }

    public function region(Request $request)
    {
        $data = RegionModel::where('zone_id', $request->post('zone_id'))->where('is_active', '1')->orderBy('region_name','ASC')->get();
        $responseData = [];
        if(isset($data) && count($data)):
            foreach ($data as $key => $val):
                $responseData[] = ['id'=>$val->id,'text'=>$val->region_name];
            endforeach;
        endif;
        return response()->json($responseData);
    }

    public function area(Request $request)
    {
        $data = AreaModel::where('region_id', $request->post('region_id'))->where('is_active', '1')->orderBy('area_name','ASC')->get();
        $responseData = [];
        if(isset($data) && count($data)):
            foreach ($data as $key => $val):
                $responseData[] = ['id'=>$val->id,'text'=>$val->area_name];
            endforeach;
        endif;
        return response()->json($responseData);
    }

    public function getroute(Request $request)
    {
        $data = RouteModel::where('area_id', $request->post('area_id'))->where('is_active', '1')->orderBy('route_name','ASC')->get();
        $responseData = [];
        if(isset($data) && count($data)):
            foreach ($data as $key => $val):
                $responseData[] = ['id'=>$val->id,'text'=>$val->route_name];
            endforeach;
        endif;
        return response()->json($responseData);
    }

    public function brandnamenameverify(Request $request)
    {
        if (isset($request->post('query')['_brand_name']) && $request->post('query')['brand_name'] == $request->post('query')['_brand_name']):
            echo "true";
        else:
            $data = BrandModel::where('brand_name',$request->input('query')["brand_name"])->first();
            if($data):
                echo 'false';
            else:
                echo 'true';
            endif;
        endif;
    }
}

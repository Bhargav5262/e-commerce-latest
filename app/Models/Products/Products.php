<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class Products extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_catalog';

	public $timestamps = true;

	protected $fillable = [
		'cat_id',
		'sub_cat_id',
		'child_cat_id',
		'child_sub_cat_id',
		'brand_id',
		'manufacturer',
		'product_type',
		'barcode',
		'hsn_no',
		'prod_desc',
		'product_name',
		'sku_size',
		'uom_id',
		'gst',
		'igst',
		'sgst',
		'case_size',
		'case_weight',
		'case_dimen',
		'prod_weight',
		'prod_dimen',
		'shelf_life',
		'ord_qnt_rtl_1',
		'ord_qnt_rtl_2',
		'ord_qnt_rtl_3',
		'ord_qnt_bulk_1',
		'ord_qnt_bulk_2',
		'ord_qnt_bulk_3',
		'margin_rtl',
		'margin_bulk',
		'comm_team_rtl',
		'comm_team_bulk',
		'comm_frl_rtl',
		'comm_frl_bulk',
		'prod_tag',
		'by_bulk_csv',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $dates = ['deleted_at'];

	public function category()
	{
		return $this->hasOne('App\Models\Category\Category','cat_id','cat_id');
	}

	public function subcategory()
	{
		return $this->hasOne('App\Models\Category\SubCategory','sub_cat_id','sub_cat_id');
	}

	public function childcategory()
	{
		return $this->hasOne('App\Models\Category\ChildCategory','child_cat_id','child_cat_id');
	}

	public function brand()
	{
		return $this->hasOne('App\Models\Brand','brand_id','brand_id');
	}

	public function product_image()
	{
		return $this->belongsTo('App\Models\Products\ProductsMedia','id','prod_id')->where('is_default','1');
	}

	public function vender_details()
	{
		return $this->belongsTo('App\Models\Products\ProductsVendors','id','prod_id');
	}
}
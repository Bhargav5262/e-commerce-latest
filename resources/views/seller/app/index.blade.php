<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
	@include('seller.app.top')
	@yield('css')
</head>
<body class="horizontal-layout horizontal-menu horizontal-menu-padding 2-columns  " data-open="click" data-menu="horizontal-menu" data-col="2-columns">
	@include('seller.app.header')
    <div class="app-content container center-layout mt-2 min_height">
    	@yield('content')
    </div>
    @include('seller.app.footer')
	@include('seller.app.bottom')
	@yield('jspage')
</body>
</html>
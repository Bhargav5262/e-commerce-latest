<?php

namespace App\Models;

use Eloquent, Request;

class Route extends Eloquent {

	protected $table = 'mst_route';

	public $timestamps = true;

	protected $fillable = [
		'area_id',
		'region_id',
		'zone_id',
		'route_name',
		'is_active',
		'created_by',
		'updated_by',
	];

	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

	public function areas(){
		return $this->hasOne('App\Models\Area','id','area_id');
	}

	public function regions()
	{
		return $this->hasOne('App\Models\Region','id','region_id');
	}

	public function zones()
	{
		return $this->hasOne('App\Models\Zone','id','zone_id');
	}
}
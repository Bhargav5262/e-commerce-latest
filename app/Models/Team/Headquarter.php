<?php

namespace App\Models\Team;

use Eloquent, Request;

class Headquarter extends Eloquent {

	protected $table = 'mst_hq';

	public $timestamps = true;

	protected $fillable = [
		'HQ_name',
		'HQ_code',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $primaryKey = "HQ_id";
	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
}
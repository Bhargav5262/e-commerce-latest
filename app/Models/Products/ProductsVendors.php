<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class ProductsVendors extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_vend_prod';

	public $timestamps = true;

	protected $fillable = [
		'prod_id',
		'mrp',
		'inv_qty',
		'ret_minord_qnty',
		'ret_minord_qnty_cost_a',
		'ret_minord_qnty_cost_b',
		'ret_minord_qnty_cost_c',
		'whs_minord_qnty',
		'whs_minord_qnty_cost_a',
		'whs_minord_qnty_cost_b',
		'whs_minord_qnty_cost_c',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $dates = ['deleted_at'];

	public function product()
	{
		return $this->hasOne('App\Models\Products\Products','id','prod_id');
	}
}
<?php

namespace App\Models\Category;

use Eloquent, Request;

class SubCategory extends Eloquent {

	protected $table = 'mst_sub_cat';

	public $timestamps = true;

	protected $fillable = [
		'cat_id',
		'sub_cat_name',
		'sub_cat_img',
		'sub_cat_banner',
		'cat_dec',
		'order_seq',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $primaryKey = "sub_cat_id";
	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

	protected $casts = ['sub_cat_img' => 'array','sub_cat_banner' => 'array'];

	public function category()
	{
		return $this->hasOne('App\Models\Category\Category','cat_id','cat_id');
	}
}
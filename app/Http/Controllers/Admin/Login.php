<?php
namespace App\Http\Controllers\Admin;
use Request, Auth, Lang, Hash;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\Admin\LoginRequest;
use App\Models\Admin as AdminModel;

class Login extends Controller {

	protected function getLogin() {
		if (auth()->guard('admin')->check()):
			return redirect('admin/dashboard');
		endif;
		return view('admin/login');
	}

	protected function postLogin(LoginRequest $request) {
		$remember_me = $request->only('remember') ? true : false;
		$auth = auth()->guard('admin');
		if(is_numeric($request->input('email'))):
		    $field = 'mobile';
		elseif(filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)):
		    $field = 'email';
		else:
			return redirect('admin/login')->with('warning', Lang::get('message.emailMobileNotValid'));    
		endif;
		$request->merge([$field => $request->input('email')]);
		if($auth->attempt($request->only($field, 'password'), $remember_me)):
			return redirect('admin/dashboard');
		else:
			return redirect('admin/login')->with( 'warning', Lang::get('message.emailAndPassword') ); 
		endif;	
	}
	
	protected function getLogout() {
		$auth = auth()->guard('admin');
		$auth->logout();
		return redirect('admin/login')->with(['success' => 'Logged Out']);
	}

	protected function getForgotPassword() {

		if (auth()->guard('admin')->check()) {
			return redirect('admin/dashboard');
		}
		return view('admin/forgotpassword');
	}

	protected function postForgotPassword( Request $request ) {
		$post_data = Request::All();
		if (auth()->guard('admin')->check()):
			return redirect('admin/dashboard');
		endif;
		$email = $post_data['email'];
		$data = [];
		if(filter_var($email, FILTER_VALIDATE_EMAIL)):
			$data = AdminModel::where('email',$email)->first();
			if(isset($data) && !empty($data)):
				$update=array('remember_token'=>md5($data->id));
				$link=url("admin/new-password").'/'.$update['remember_token'];
				/* Mail send start */
				$email = $data->email;
				$name = $data->name;
				$new_mail_data['NAME']		=	$name;
				$new_mail_data['LINK']		=	$link;
				$new_email_data = _email_template_content("1",$new_mail_data);
				// << Email Template Data
				$new_subject = isset( $new_email_data[0] ) ? $new_email_data[0] : '';
				$new_content = $new_email_data[1];
				// From, To and Send Email
				$new_fromdata = ['email' => $email,'name' => $name];
				$new_mailids = [$email => $name];
				_mail_send_general( $new_fromdata, $new_subject , $new_content , $new_mailids );
				/* Mail send end */
				AdminModel::find($data->id)->update($update);

				return redirect('admin/forgotpassword')->with('success', Lang::get('message.checkEmail'));
			else:
				return redirect('admin/forgotpassword')->with('warning', Lang::get('message.emailNotExist'));
			endif;
		elseif(preg_match('/^\d{10}$/',$email)): // phone number is valid
			$data = AdminModel::where('mobile',$email)->first();

			if(isset($data) && !empty($data)):
				$update = array('otp' => $this->generateNumericOTP(4));
				$link = url("admin/verifyotp").'/'.$update['otp'];
				/* SMS OTP send start */
				$api_key = '55ED29E5F22CA0';
				$contacts = $email;
				$from = 'TXTDMO';
				$sms_text = urlencode('Forgot password OTP is: '. $update['otp']);
				//Submit to server
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL, "http://bulksms.smsroot.com/app/smsapi/index.php");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=1&routeid=13&type=text&contacts=".$contacts."&senderid=".$from."&msg=".$sms_text);
				$response = curl_exec($ch);
				$response = curl_exec($ch);
				$err = curl_error($ch);
				curl_close($ch);
				if($err):
				  //echo "cURL Error #:" . $err;
				  return redirect('admin/forgotpassword')->with('warning', $err);
				  die;
				else:
				 //  	if(!is_array($response)):
					//   return redirect('admin/forgotpassword')->with('warning', $response);
					//   die;
					// endif;
				endif;				
				/* SMS OTP send end */
				$var = AdminModel::find($data->id)->update($update);
				return redirect('admin/verifyotp')->with('success', Lang::get('message.checkSMS'));
			else:
				return redirect('admin/forgotpassword')->with('warning', Lang::get('message.mobileNotExist'));
			endif;
		else:
			return redirect('admin/forgotpassword')->with('warning', Lang::get('message.emailMobileNotValid'));
		endif;
	}

	public function newPassword($id="") {
		$data = Request::all();
		if (auth()->guard('admin')->check()):
			return redirect('admin/dashboard');
		endif;
		$data = AdminModel::where("remember_token",$id)->first();
		if(isset($data) && !empty($data)):
			$_data=array('id'=>$id,'successlink'=>1);
	        return view( 'admin/new-password', $_data );
		else:
			$_data=array('id'=>$id,'successlink'=>0);
	        return view( 'admin/new-password', $_data);
		endif;
	}

	public function postConfirmNewPassword() {
		$post_data = Request::all();
		$data = AdminModel::where("remember_token",$post_data['id'])->first();
		if(isset($data) && !empty($data)):
			$update=array('remember_token'=>"",'password'=>Hash::make($post_data['password']));
			AdminModel::find($data->id)->update($update);
			$_data=array('id'=>$post_data['id'],'success'=>1);
			return redirect('admin/login')->with('success','Password has been successfully update.');
		else:
			$_data=array('id'=>$post_data['id'],'success'=>0);
			return redirect('admin/new-password/'.$post_data['id']);				
		endif;
		
	}

	public function getVerifyOTP($id="") {
		$data = Request::all();
		if (auth()->guard('admin')->check()):
			return redirect('admin/dashboard');
		endif;
		$data = AdminModel::where("id", $id)->first();
		if(isset($data) && !empty($data)):
			$_data = array('id' => $id, 'successotp' => 1);
	        return view( 'admin/verifyotp', $_data );
		else:
			$_data = array('id' => $id, 'successotp' => 0);
	        return view( 'admin/verifyotp', $_data );
		endif;
	}

	public function postVerifyOTP( Request $request ) {
		$post_data = Request::All();
		if (auth()->guard('admin')->check()):
			return redirect('admin/dashboard');
		endif;

		$otp = $post_data['otp'];

		if($otp != ''){
			$data = AdminModel::where('otp',$otp)->first();
			if(isset($data) && !empty($data)):

				$id = $data->id;

				if(isset($data) && !empty($data)):
					$_data = array('id'=>$id,'successotp'=>1);
			        return view( 'admin/new-password-otp', $_data );
				else:
					$_data=array('id'=>$id,'successotp'=>0);
			        return view( 'admin/new-password-otp', $_data);
				endif;
			else:
				return redirect('admin/verifyotp')->with('warning', Lang::get('message.invalidOTP'));
			endif;

			
		} else {
			return redirect('admin/verifyotp')->with('warning', Lang::get('message.requiredOTP'));
		}
	}

	public function postConfirmOTP() {
		$post_data = Request::all();
		$data = AdminModel::where("id",$post_data['id'])->first();
		if(isset($data) && !empty($data)):
			$update=array('otp'=>"",'password'=>Hash::make($post_data['password']));
			AdminModel::find($data->id)->update($update);
			$_data=array('id'=>$post_data['id'],'success'=>1);
			return redirect('admin/login')->with('success','Password has been successfully update.');
		else:
			$_data=array('id'=>$post_data['id'],'success'=>0);
			return redirect('admin/new-password-otp/'.$post_data['id']);				
		endif;
		
	}

	public function generateNumericOTP($n) { 
	    $generator = "1357902468"; 
	  
	    $result = ""; 
	    for ($i = 1; $i <= $n; $i++) { 
	        $result .= substr($generator, (rand()%(strlen($generator))), 1); 
	    } 
	    return $result; 
	} 
}

<?php

namespace App\Models\Category;

use Eloquent, Request;

class SubChildCategory extends Eloquent {

	protected $table = 'mst_sub_child_cat';

	public $timestamps = true;

	protected $fillable = [
		'child_cat_id',
		'sub_cat_id',
		'cat_id',
		'sub_child_cat_name',
		'sub_child_cat_img',
		'sub_child_cat_banner',
		'cat_dec',
		'order_seq',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $primaryKey = "sub_child_cat_id";
	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

	protected $casts = ['sub_child_cat_img' => 'array','sub_child_cat_banner' => 'array'];

	public function category()
	{
		return $this->hasOne('App\Models\Category\Category','cat_id','cat_id');
	}

	public function subcategory()
	{
		return $this->hasOne('App\Models\Category\SubCategory','sub_cat_id','sub_cat_id');
	}

	public function childcategory()
	{
		return $this->hasOne('App\Models\Category\ChildCategory','child_cat_id','child_cat_id');
	}
}
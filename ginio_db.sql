-- Adminer 4.7.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8;

DROP TABLE IF EXISTS `cron_maintenence`;
CREATE TABLE `cron_maintenence` (
  `id` int NOT NULL AUTO_INCREMENT,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `module` varchar(25) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `parent_table` varchar(25) DEFAULT NULL,
  `error` text,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `cron_maintenence` (`id`, `start_time`, `end_time`, `module`, `status`, `parent_table`, `error`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	'2020-06-18 20:35:37',	'2020-06-18 20:35:37',	'mapseller',	'failed',	'mapseller',	'\"TypeError: Cannot read property \'split\' of null\\n    at /home/one/Node/giniobcgnode/cron/cron.js:57:62\"',	NULL,	'mapseller',	'2020-06-18 20:35:37',	'2020-06-18 20:35:37'),
(2,	'2020-06-18 20:35:38',	'2020-06-18 20:35:38',	'mapseller',	'failed',	'mapseller',	'\"TypeError: Cannot read property \'split\' of null\\n    at /home/one/Node/giniobcgnode/cron/cron.js:57:62\"',	NULL,	'mapseller',	'2020-06-18 20:35:38',	'2020-06-18 20:35:38'),
(3,	'2020-06-18 20:35:39',	'2020-06-18 20:35:39',	'mapseller',	'failed',	'mapseller',	'\"TypeError: Cannot read property \'split\' of null\\n    at /home/one/Node/giniobcgnode/cron/cron.js:57:62\"',	NULL,	'mapseller',	'2020-06-18 20:35:39',	'2020-06-18 20:35:39'),
(4,	'2020-06-18 20:35:40',	'2020-06-18 20:35:40',	'mapseller',	'failed',	'mapseller',	'\"TypeError: Cannot read property \'split\' of null\\n    at /home/one/Node/giniobcgnode/cron/cron.js:57:62\"',	NULL,	'mapseller',	'2020-06-18 20:35:40',	'2020-06-18 20:35:40'),
(5,	'2020-06-18 20:35:41',	'2020-06-18 20:35:41',	'mapseller',	'failed',	'mapseller',	'\"TypeError: Cannot read property \'split\' of null\\n    at /home/one/Node/giniobcgnode/cron/cron.js:57:62\"',	NULL,	'mapseller',	'2020-06-18 20:35:41',	'2020-06-18 20:35:41'),
(6,	'2020-06-18 20:35:42',	'2020-06-18 20:35:42',	'mapseller',	'failed',	'mapseller',	'\"TypeError: Cannot read property \'split\' of null\\n    at /home/one/Node/giniobcgnode/cron/cron.js:57:62\"',	NULL,	'mapseller',	'2020-06-18 20:35:42',	'2020-06-18 20:35:42'),
(7,	'2020-06-18 20:35:43',	'2020-06-18 20:35:43',	'mapseller',	'failed',	'mapseller',	'\"TypeError: Cannot read property \'split\' of null\\n    at /home/one/Node/giniobcgnode/cron/cron.js:57:62\"',	NULL,	'mapseller',	'2020-06-18 20:35:43',	'2020-06-18 20:35:43'),
(8,	'2020-06-18 20:37:15',	'2020-06-18 20:37:15',	'mapseller',	'failed',	'mapseller',	'\"SequelizeDatabaseError: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \')\' at line 3\\n    at Query.formatError (/home/one/Node/giniobcgnode/node_modules/sequelize/lib/dialects/mysql/query.js:244:16)\\n    at Query.handler [as onResult] (/home/one/Node/giniobcgnode/node_modules/sequelize/lib/dialects/mysql/query.js:51:23)\\n    at Query.execute (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/commands/command.js:30:14)\\n    at Connection.handlePacket (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/connection.js:417:32)\\n    at PacketParser.onPacket (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/connection.js:75:12)\\n    at PacketParser.executeStart (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/packet_parser.js:75:16)\\n    at Socket.<anonymous> (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/connection.js:82:25)\\n    at Socket.emit (events.js:210:5)\\n    at Socket.EventEmitter.emit (domain.js:476:20)\\n    at addChunk (_stream_readable.js:308:12)\\n    at readableAddChunk (_stream_readable.js:289:11)\\n    at Socket.Readable.push (_stream_readable.js:223:10)\\n    at TCP.onStreamRead (internal/stream_base_commons.js:182:23)\"',	NULL,	'mapseller',	'2020-06-18 20:37:15',	'2020-06-18 20:37:15'),
(9,	'2020-06-18 20:37:16',	'2020-06-18 20:37:16',	'mapseller',	'failed',	'mapseller',	'\"SequelizeDatabaseError: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \')\' at line 3\\n    at Query.formatError (/home/one/Node/giniobcgnode/node_modules/sequelize/lib/dialects/mysql/query.js:244:16)\\n    at Query.handler [as onResult] (/home/one/Node/giniobcgnode/node_modules/sequelize/lib/dialects/mysql/query.js:51:23)\\n    at Query.execute (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/commands/command.js:30:14)\\n    at Connection.handlePacket (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/connection.js:417:32)\\n    at PacketParser.onPacket (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/connection.js:75:12)\\n    at PacketParser.executeStart (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/packet_parser.js:75:16)\\n    at Socket.<anonymous> (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/connection.js:82:25)\\n    at Socket.emit (events.js:210:5)\\n    at Socket.EventEmitter.emit (domain.js:476:20)\\n    at addChunk (_stream_readable.js:308:12)\\n    at readableAddChunk (_stream_readable.js:289:11)\\n    at Socket.Readable.push (_stream_readable.js:223:10)\\n    at TCP.onStreamRead (internal/stream_base_commons.js:182:23)\"',	NULL,	'mapseller',	'2020-06-18 20:37:16',	'2020-06-18 20:37:16'),
(10,	'2020-06-18 20:37:17',	'2020-06-18 20:37:17',	'mapseller',	'failed',	'mapseller',	'\"SequelizeDatabaseError: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \')\' at line 3\\n    at Query.formatError (/home/one/Node/giniobcgnode/node_modules/sequelize/lib/dialects/mysql/query.js:244:16)\\n    at Query.handler [as onResult] (/home/one/Node/giniobcgnode/node_modules/sequelize/lib/dialects/mysql/query.js:51:23)\\n    at Query.execute (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/commands/command.js:30:14)\\n    at Connection.handlePacket (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/connection.js:417:32)\\n    at PacketParser.onPacket (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/connection.js:75:12)\\n    at PacketParser.executeStart (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/packet_parser.js:75:16)\\n    at Socket.<anonymous> (/home/one/Node/giniobcgnode/node_modules/mysql2/lib/connection.js:82:25)\\n    at Socket.emit (events.js:210:5)\\n    at Socket.EventEmitter.emit (domain.js:476:20)\\n    at addChunk (_stream_readable.js:308:12)\\n    at readableAddChunk (_stream_readable.js:289:11)\\n    at Socket.Readable.push (_stream_readable.js:223:10)\\n    at TCP.onStreamRead (internal/stream_base_commons.js:182:23)\"',	NULL,	'mapseller',	'2020-06-18 20:37:17',	'2020-06-18 20:37:17'),
(11,	'2020-06-18 20:39:47',	'2020-06-18 20:39:48',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:39:47',	'2020-06-18 20:39:48'),
(12,	'2020-06-18 20:39:49',	'2020-06-18 20:39:49',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:39:49',	'2020-06-18 20:39:49'),
(13,	'2020-06-18 20:39:50',	'2020-06-18 20:39:50',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:39:50',	'2020-06-18 20:39:50'),
(14,	'2020-06-18 20:39:51',	'2020-06-18 20:39:51',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:39:51',	'2020-06-18 20:39:51'),
(15,	'2020-06-18 20:39:52',	'2020-06-18 20:39:52',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:39:52',	'2020-06-18 20:39:52'),
(16,	'2020-06-18 20:42:00',	'2020-06-18 20:42:00',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:42:00',	'2020-06-18 20:42:00'),
(17,	'2020-06-18 20:47:00',	'2020-06-18 20:47:01',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:47:00',	'2020-06-18 20:47:01'),
(18,	'2020-06-18 20:51:00',	'2020-06-18 20:51:00',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:51:00',	'2020-06-18 20:51:00'),
(19,	'2020-06-18 20:53:00',	'2020-06-18 20:53:00',	'mapseller',	'failed',	'mapseller',	'\"TypeError: Cannot read property \'update\' of undefined\\n    at /home/one/Node/giniobcgnode/cron/cron.js:100:39\"',	NULL,	'mapseller',	'2020-06-18 20:53:00',	'2020-06-18 20:53:00'),
(20,	'2020-06-18 20:54:00',	'2020-06-18 20:54:00',	'mapseller',	'failed',	'mapseller',	'\"TypeError: Cannot read property \'update\' of undefined\\n    at /home/one/Node/giniobcgnode/cron/cron.js:100:39\"',	NULL,	'mapseller',	'2020-06-18 20:54:00',	'2020-06-18 20:54:00'),
(21,	'2020-06-18 20:56:00',	'2020-06-18 20:56:01',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:56:00',	'2020-06-18 20:56:01'),
(22,	'2020-06-18 20:58:00',	'2020-06-18 20:58:01',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:58:00',	'2020-06-18 20:58:01'),
(23,	'2020-06-18 20:59:00',	'2020-06-18 20:59:01',	'mapseller',	'success',	'mapseller',	'\"\"',	NULL,	'mapseller',	'2020-06-18 20:59:00',	'2020-06-18 20:59:01');

DROP TABLE IF EXISTS `map_relative_city`;
CREATE TABLE `map_relative_city` (
  `map_relative_city_id` int NOT NULL AUTO_INCREMENT,
  `city_id` int DEFAULT NULL,
  `rel_city_id` int DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`map_relative_city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `map_relative_city` (`map_relative_city_id`, `city_id`, `rel_city_id`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	1,	2,	1,	'Nitesh',	NULL,	'2020-06-18 15:25:50',	'2020-06-18 15:25:50'),
(2,	1,	3,	1,	'Nitesh',	NULL,	'2020-06-18 15:25:50',	'2020-06-18 15:25:50');

DROP TABLE IF EXISTS `mst_admin`;
CREATE TABLE `mst_admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `remember_token` varchar(256) NOT NULL,
  `status` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1 for activate, 2 for deactivate',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `mst_admin` (`id`, `name`, `email`, `password`, `remember_token`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Admin',	'admin@example.com',	'$2y$10$451vabjPPX/BAd7NZtJjOu9F/GPLRzVdiZ.DSPA1jr1Obszc9Uqoy',	'',	'1',	'2019-10-01 00:00:00',	'2020-05-24 18:26:38',	NULL);

DROP TABLE IF EXISTS `mst_area`;
CREATE TABLE `mst_area` (
  `id` int NOT NULL AUTO_INCREMENT,
  `region_id` int NOT NULL,
  `zone_id` int NOT NULL,
  `area_name` varchar(50) NOT NULL,
  `is_active` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `mst_area` (`id`, `region_id`, `zone_id`, `area_name`, `is_active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1,	2,	1,	'sdfsdf',	1,	'2020-05-23 03:27:20',	1,	'2020-05-23 09:50:07',	1,	NULL),
(2,	2,	1,	'sdfsdfsdfsdf',	1,	'2020-05-23 04:10:52',	1,	'2020-05-23 09:49:56',	1,	NULL),
(3,	2,	1,	'sdsd',	1,	'2020-06-18 17:41:01',	1,	'2020-06-18 17:41:01',	1,	NULL),
(4,	1,	2,	'sdsds',	1,	'2020-06-18 17:43:18',	1,	'2020-06-18 17:43:18',	1,	NULL);

DROP TABLE IF EXISTS `mst_brand`;
CREATE TABLE `mst_brand` (
  `brand_id` int NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(50) NOT NULL,
  `brand_img` text NOT NULL,
  `brand_banner` text NOT NULL,
  `order_seq` int NOT NULL,
  `brand_code` varchar(10) DEFAULT NULL,
  `is_active` int NOT NULL DEFAULT '1' COMMENT '0 = inactive and 1 = active',
  `created_by` int NOT NULL,
  `updated_by` int NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_brand` (`brand_id`, `brand_name`, `brand_img`, `brand_banner`, `order_seq`, `brand_code`, `is_active`, `created_by`, `updated_by`, `deleted_at`, `createdAt`, `updatedAt`) VALUES
(1,	'test',	'[\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/img_1_1590259258_download.jpeg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/img_1_1590259259_6348851056_349ebbf3ab_b.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/img_1_1590259259_unnamed.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/img_1_1590259259_maxresdefault1.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/img_1_1590259259_maxresdefault.jpg\"]',	'[\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/banner_1_1590259259_1280px-Snake_River_(5mb).jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/banner_1_1590259260_1024px-Snake_River_(5mb).jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/banner_1_1590259260_maxresdefault_(2).jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/banner_1_1590259260_maxresdefault_(1).jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/banner_1_1590259260_maxresdefault-10.jpg\"]',	3,	'1',	1,	1,	1,	NULL,	'2020-05-23 18:37:03',	'2020-05-23 18:49:50'),
(2,	'dsdfsdf',	'[\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/img_1_1590259091_maxresdefault1.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/img_1_1590259092_maxresdefault.jpg\"]',	'[\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/brand\\/banner_1_1590259760_6348851056_349ebbf3ab_b.jpg\"]',	1,	'2',	1,	1,	1,	NULL,	'2020-05-23 18:38:12',	'2020-05-23 18:49:21'),
(3,	'Louis Bland',	'',	'',	4,	'123456',	1,	1,	1,	NULL,	'2020-06-18 16:23:42',	'2020-06-18 16:24:10');

DROP TABLE IF EXISTS `mst_cat`;
CREATE TABLE `mst_cat` (
  `cat_id` int NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(20) NOT NULL,
  `cat_img` text NOT NULL,
  `cat_banner` text NOT NULL,
  `is_active` int NOT NULL DEFAULT '1' COMMENT '0 = inactive and 1 = active',
  `created_by` int NOT NULL,
  `updated_by` int NOT NULL,
  `cat_dec` text,
  `order_seq` int DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_cat` (`cat_id`, `cat_name`, `cat_img`, `cat_banner`, `is_active`, `created_by`, `updated_by`, `cat_dec`, `order_seq`, `createdAt`, `updatedAt`) VALUES
(1,	'Test',	'[\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/img_1_1590043714_Jack-Jones-Beige-Round-Neck-SDL693531765-5-52974.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/img_1_1590043716_Jack-Jones-Beige-Round-Neck-SDL693531765-4-46b29.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/img_1_1590043716_Jack-Jones-Beige-Round-Neck-SDL693531765-3-1ac20.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/img_1_1590043717_Jack-Jones-Beige-Round-Neck-SDL693531765-2-66430.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/img_1_1590043717_Jack-Jones-Beige-Round-Neck-SDL693531765-1-6414a.jpg\"]',	'[\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/banner_1_1589889997_Jack-Jones-White-Round-Neck-SDL697586396-5-f5acc.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/banner_1_1589889998_Jack-Jones-White-Round-Neck-SDL697586396-4-0c562.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/banner_1_1589889999_Jack-Jones-White-Round-Neck-SDL697586396-3-e118e.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/banner_1_1589890000_Jack-Jones-White-Round-Neck-SDL697586396-2-a9e3c.jpg\",\"https:\\/s3.ap-south-1.amazonaws.com\\/pics.test.mm\\/lalit\\/category\\/banner_1_1589890002_Jack-Jones-White-Round-Neck-SDL697586396-1-97956.jpg\"]',	1,	1,	1,	'sfsdf',	4,	'2020-05-19 12:06:43',	'2020-06-18 15:16:13');

DROP TABLE IF EXISTS `mst_catalog`;
CREATE TABLE `mst_catalog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cat_id` int NOT NULL,
  `sub_cat_id` int NOT NULL,
  `child_cat_id` int NOT NULL,
  `child_sub_cat_id` int NOT NULL,
  `brand_id` int NOT NULL,
  `manufacturer` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `product_type` int NOT NULL COMMENT '1 for blubuck, 2 for Other',
  `barcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hsn_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prod_desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `product_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sku_size` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `uom_id` int NOT NULL,
  `gst` decimal(10,2) NOT NULL,
  `igst` decimal(10,2) NOT NULL,
  `sgst` decimal(10,2) NOT NULL,
  `case_size` int NOT NULL,
  `case_weight` decimal(10,2) NOT NULL,
  `case_dimen` decimal(10,2) NOT NULL,
  `prod_weight` decimal(10,2) NOT NULL,
  `prod_dimen` decimal(10,2) NOT NULL,
  `shelf_life` int NOT NULL,
  `ord_qnt_rtl_1` int NOT NULL,
  `ord_qnt_rtl_2` int NOT NULL,
  `ord_qnt_rtl_3` int NOT NULL,
  `ord_qnt_bulk_1` int NOT NULL,
  `ord_qnt_bulk_2` int NOT NULL,
  `ord_qnt_bulk_3` int NOT NULL,
  `margin_rtl` decimal(5,2) NOT NULL,
  `margin_bulk` decimal(5,2) NOT NULL,
  `comm_team_rtl` decimal(5,2) NOT NULL,
  `comm_team_bulk` decimal(5,2) NOT NULL,
  `comm_frl_rtl` decimal(5,2) NOT NULL,
  `comm_frl_bulk` decimal(5,2) NOT NULL,
  `prod_tag` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `by_bulk_csv` int NOT NULL,
  `is_active` int NOT NULL DEFAULT '4' COMMENT '0 for inactive, 1 for active, 2 for request for review (submit), 3 for disapproved by admin, 4 for draft',
  `created_by` int NOT NULL,
  `updated_by` int NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_catalog` (`id`, `cat_id`, `sub_cat_id`, `child_cat_id`, `child_sub_cat_id`, `brand_id`, `manufacturer`, `product_type`, `barcode`, `hsn_no`, `prod_desc`, `product_name`, `sku_size`, `uom_id`, `gst`, `igst`, `sgst`, `case_size`, `case_weight`, `case_dimen`, `prod_weight`, `prod_dimen`, `shelf_life`, `ord_qnt_rtl_1`, `ord_qnt_rtl_2`, `ord_qnt_rtl_3`, `ord_qnt_bulk_1`, `ord_qnt_bulk_2`, `ord_qnt_bulk_3`, `margin_rtl`, `margin_bulk`, `comm_team_rtl`, `comm_team_bulk`, `comm_frl_rtl`, `comm_frl_bulk`, `prod_tag`, `by_bulk_csv`, `is_active`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	1,	0,	0,	1,	'Test',	0,	'ABC',	'XYZ',	'dfn k dsfdsk fdsfhhkhjkh dfn k dsfdsk fdsfhhkhjkh dfn k dsfdsk fdsfhhkhjkh',	'Parle G',	'123456',	1,	18.00,	20.00,	18.00,	10,	20.00,	20.00,	10.00,	20.00,	10,	0,	0,	0,	0,	0,	0,	0.00,	0.00,	0.00,	0.00,	0.00,	0.00,	'ParleG, Testone, Testsubcategory, test, gg',	0,	1,	2,	2,	'2020-06-16 17:58:36',	'2020-06-16 19:04:16',	NULL),
(2,	1,	1,	0,	0,	2,	'Louis Bland',	0,	'Louis',	'Bland',	'lkkshdk skjh kjhsjkrh kjshth kjshh jshtth jkrhtkjhrshjkkhkhlkkshdk skjh kjhsjkrh kjshth kjshh jshtth jkrhtkjhrshjkkhkh',	'Panda graphic T-Shirt Item',	'100',	1,	18.00,	10.00,	10.00,	10,	10.00,	10.00,	10512.00,	20.00,	44510,	0,	0,	0,	0,	0,	0,	0.00,	0.00,	0.00,	0.00,	0.00,	0.00,	'pandagraphict-shirt, testone, testsubcategory, dsdfsdf, 44, 545j',	0,	4,	1,	1,	'2020-06-16 18:09:03',	'2020-06-19 16:08:24',	NULL),
(3,	1,	1,	0,	0,	2,	'Test',	0,	'DEF',	'OPQXYZPQR',	'Test product descriptionTest product descriptionTest product descriptionTest product description',	'Test product Item',	'10',	1,	10.00,	20.00,	20.00,	0,	0.00,	10.00,	10.00,	10.00,	10,	0,	0,	0,	0,	0,	0,	0.00,	0.00,	0.00,	0.00,	0.00,	0.00,	'testitem, testone, testsubcategory, dsdfsdf, 100, 0, 5, 05, 0',	0,	2,	1,	1,	'2020-06-16 18:14:33',	'2020-06-16 19:18:42',	NULL),
(4,	1,	1,	0,	0,	2,	'dfd',	0,	'456',	'456',	'f df e bsdjhgfshjdf sd jsd jsd fhjsgdghjf df e bsdjhgfshjdf sd jsd jsd fhjsgdghjf df e bsdjhgfshjdf sd jsd jsd fhjsgdghjf df e bsdjhgfshjdf sd jsd jsd fhjsgdghj',	'efset test item',	'123',	1,	10.00,	10.00,	10.00,	10,	10.00,	10.00,	10.00,	10.00,	1,	0,	0,	0,	0,	0,	0,	0.00,	0.00,	0.00,	0.00,	0.00,	0.00,	'efset, testone, testsubcategory, dsdfsdf, 0, 10, 1, 0, 1, 01, 0, 10, 1',	0,	2,	1,	1,	'2020-06-17 17:14:42',	'2020-06-19 16:06:26',	NULL),
(5,	1,	1,	1,	1,	3,	'Louis Bland',	1,	'Louis12345',	'Bland12345',	'Panda graphic T-Shirt Panda graphic T-ShirtPanda graphic T-ShirtPanda graphic T-Shirt',	'Panda graphic Crop Top',	'123',	1,	10.00,	10.00,	10.00,	10,	10.00,	10.00,	10.00,	10.00,	10,	10,	10,	10,	10,	10,	10,	10.00,	10.00,	10.00,	10.00,	10.00,	10.00,	'pandagraphiccroptop, test, testsubcategory, louisbland, 10, 10, 1',	0,	1,	0,	0,	'2020-06-18 16:28:50',	'2020-06-18 16:31:11',	NULL),
(6,	1,	1,	0,	0,	3,	'Test',	0,	't',	't',	'ts kljsdj lkjlkjts kljsdj lkjlkjts kljsdj lkjlkjts kljsdj lkjlkjts kljsdj lkjlkjts kljsdj lkjlkj',	't',	'10',	1,	10.00,	10.00,	10.00,	10,	454.00,	5454.00,	545.00,	45.00,	5,	0,	0,	0,	0,	0,	0,	0.00,	0.00,	0.00,	0.00,	0.00,	0.00,	't, test, louisbland, 4545, 5, 45, 4545',	0,	2,	1,	1,	'2020-06-18 18:16:49',	'2020-06-18 18:19:49',	NULL);

DROP TABLE IF EXISTS `mst_catalog_media`;
CREATE TABLE `mst_catalog_media` (
  `id` int NOT NULL AUTO_INCREMENT,
  `prod_id` int NOT NULL,
  `media` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `media_type` int NOT NULL COMMENT '1 for Image, 2 for Video',
  `is_default` int NOT NULL DEFAULT '0',
  `order_by` int NOT NULL,
  `is_active` int NOT NULL DEFAULT '1' COMMENT '	0 = inactive and 1 = active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_catalog_media` (`id`, `prod_id`, `media`, `media_type`, `is_default`, `order_by`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	'https://s3.ap-south-1.amazonaws.com/pics.test.mm/lalit/media/img_1_1590328615_Jack-Jones-White-T-Shirt-SDL642768022-5-3b604.jpg',	1,	1,	0,	1,	'2020-05-24 11:31:55',	'2020-05-24 13:56:57',	NULL),
(2,	4,	'https://s3.ap-south-1.amazonaws.com/pics.test.mm/lalit/media/img_1_1590328237_Fabstone-Collection-Blue-Round-T-SDL087851373-1-ce23a.jpg',	1,	0,	0,	1,	'2020-05-24 12:13:09',	'2020-05-24 14:31:24',	'2020-05-24 09:01:24'),
(3,	4,	'https://s3.ap-south-1.amazonaws.com/pics.test.mm/lalit/media/img_1_1590327763_Jack-Jones-White-Round-Neck-SDL697586396-1-97956.jpg',	1,	0,	0,	1,	'2020-05-24 12:18:59',	'2020-05-24 13:42:51',	NULL),
(4,	4,	'https://s3.ap-south-1.amazonaws.com/pics.test.mm/lalit/media/img_1_1590322740_Jack-Jones-White-T-Shirt-SDL642768022-1-c729e.jpg',	1,	0,	0,	1,	'2020-05-24 12:19:00',	'2020-05-24 12:19:00',	NULL),
(5,	4,	'https://s3.ap-south-1.amazonaws.com/pics.test.mm/lalit/media/img_1_1590323243_Jack-Jones-White-T-Shirt-SDL642768022-4-47080.jpg',	1,	0,	0,	1,	'2020-05-24 12:27:27',	'2020-05-24 12:27:27',	NULL),
(6,	5,	'https://s3.ap-south-1.amazonaws.com/pics.test.mm/lalit/media/img_1_1590325190_Jack-Jones-Beige-Round-Neck-SDL693531765-5-52974.jpg',	1,	1,	0,	1,	'2020-05-24 13:00:00',	'2020-05-24 13:00:00',	NULL),
(7,	4,	'https://s3.ap-south-1.amazonaws.com/pics.test.mm/lalit/media/img_1_1590326405_Jack-Jones-White-Round-Neck-SDL697586396-5-f5acc.jpg',	1,	0,	0,	1,	'2020-05-24 13:20:13',	'2020-05-24 14:35:31',	'2020-05-24 09:05:31');

DROP TABLE IF EXISTS `mst_child_cat`;
CREATE TABLE `mst_child_cat` (
  `child_cat_id` int NOT NULL AUTO_INCREMENT,
  `sub_cat_id` int NOT NULL,
  `cat_id` int NOT NULL,
  `child_cat_name` varchar(20) NOT NULL,
  `cat_dec` text,
  `child_cat_img` text,
  `child_cat_banner` text,
  `order_seq` int DEFAULT NULL,
  `is_active` int DEFAULT NULL COMMENT '0 = inactive and 1 = active',
  `created_by` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`child_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_child_cat` (`child_cat_id`, `sub_cat_id`, `cat_id`, `child_cat_name`, `cat_dec`, `child_cat_img`, `child_cat_banner`, `order_seq`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	1,	1,	'Test Child Category',	'dfdf',	'',	'',	4,	1,	1,	1,	'2020-05-22 16:14:32',	'2020-06-18 15:44:36');

DROP TABLE IF EXISTS `mst_city`;
CREATE TABLE `mst_city` (
  `city_id` int NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) NOT NULL,
  `city_code` varchar(20) DEFAULT NULL,
  `state_id` int DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `mst_email_template`;
CREATE TABLE `mst_email_template` (
  `id` int NOT NULL AUTO_INCREMENT,
  `header_id` int NOT NULL,
  `footer_id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `status` enum('1','2') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `mst_email_template` (`id`, `header_id`, `footer_id`, `title`, `subject`, `body`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	1,	'Reset password | Admin',	'Reset password',	'<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"text-align: center;\">\r\n			<div style=\"text-align: center; width: 500px; color: rgb(85, 85, 85);\">\r\n			<h1 style=\"margin-bottom: 10px; font-size: 36px; margin-top: 35px;\">Reset password</h1>\r\n			</div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<hr style=\"border: 1px solid #2668d8;\" /></div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 44px;width: 500px;\">\r\n	<tbody>\r\n		<tr>\r\n			<td><span>Dear </span><strong>{NAME},</strong><br />\r\n			&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Click the button below to reset your password.</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td align=\"center\"><br />\r\n			<a href=\"{LINK}\" style=\"text-align:center; background: linear-gradient(to right, #2667d8 0%, #0a2c7b 100%); padding: 10px 25px;color: white; font-family: sans-serif;text-decoration: none;text-align: center; border-radius: 50px;\">Reset password</a><br />\r\n			&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-bottom: 28px;\" width=\"500\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"padding-top: 20px;\"><strong>Thank you</strong></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>',	'1',	'2020-05-24 00:00:00',	'2020-05-24 00:00:00',	NULL);

DROP TABLE IF EXISTS `mst_email_template_footer`;
CREATE TABLE `mst_email_template_footer` (
  `id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` enum('1','2') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mst_email_template_footer` (`id`, `title`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Footer1',	'<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<hr style=\"border: 3px solid #2668d8;\" />\r\n<table style=\"margin-top: 30px; width: 100%;\">\r\n	<tbody>\r\n		<tr>\r\n			<td><img src=\"http://localhost/e-commerce/public/uploads/settings/logo-icon.png\" style=\" max-width: 333.333333333333px; margin-left: -12px;\" /></td>\r\n			<td style=\"font-size: 9px; text-align: right; line-height: 12px; width: 25%;\"><span></span><br />\r\n			<span>523 Sylvan Ave, </span><br />\r\n			<span>5th Floor Mountain View,</span><br />\r\n			<span>CA 94041, USA</span><br />\r\n			<span>Tel: <a href=\"#\" style=\"color: #0068A5;text-decoration: underline;\">+1(409)987-5874</a></span></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n\r\n<div style=\"width: 100%; max-width: 500px; margin: 0px auto;\">\r\n<table style=\"margin-top: 9px;\" width=\"100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"width: 100%; text-align: center;\">\r\n			<a href=\"#\"><img src=\"http://localhost/e-commerce/public/uploads/settings/facebook.png\" style=\"width: 32px;\" /></a> \r\n			<a href=\"#\"><img src=\"http://localhost/e-commerce/public/uploads/settings/instagram.png\" style=\"width: 32px;\" /></a> \r\n			<a href=\"#\"><img src=\"http://localhost/e-commerce/public/uploads/settings/twitter.png\" style=\"width: 32px;\" /></a> \r\n			<a href=\"#\"><img src=\"http://localhost/e-commerce/public/uploads/settings/linkedin.png\" style=\"width: 32px;\" /></a>\r\n			<a href=\"#\"><img src=\"http://localhost/e-commerce/public/uploads/settings/pinterest.png\" style=\"width: 32px;\" /></a></td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>',	'1',	'2017-07-22 09:23:35',	'2019-10-19 08:00:04',	NULL);

DROP TABLE IF EXISTS `mst_email_template_header`;
CREATE TABLE `mst_email_template_header` (
  `id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` enum('1','2') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mst_email_template_header` (`id`, `title`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Header 1',	'<table style=\"margin:0px auto; max-width:500px; width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<table align=\"center\">\r\n				<tbody>\r\n					<tr>\r\n						<td><a href=\"http://localhost/e-commerce\"><img src=\"http://localhost/e-commerce/public/uploads/settings/logo.png\" style=\"margin-left:-13px; width: 90px;\" /></a></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n\r\n			<hr style=\"border: 3px solid #2668d8;\" /></td>\r\n		</tr>\r\n	</tbody>\r\n</table>',	'1',	'2017-07-22 09:24:14',	'2019-11-28 05:22:38',	NULL);

DROP TABLE IF EXISTS `mst_functionality`;
CREATE TABLE `mst_functionality` (
  `func_id` int NOT NULL AUTO_INCREMENT,
  `func_name` varchar(45) DEFAULT NULL,
  `priority_level` varchar(5) DEFAULT NULL,
  `func_type` varchar(45) DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`func_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_functionality` (`func_id`, `func_name`, `priority_level`, `func_type`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	'tra/trans/dayStart',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(2,	'tra/trans/beatList',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(3,	'tra/trans/pltCommonData',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(4,	'tra/trans/beatStoreDtl',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(5,	'tra/trans/beatStoreList',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(6,	'tra/trans/beatStoreVisitStart',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(7,	'tra/ana/timeLine',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(8,	'tra/trans/changebeat',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(9,	'tra/mch/cat',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(10,	'tra/mch/subcat',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(11,	'tra/mch/childcat',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(12,	'tra/mch/subchildcat',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(13,	'tra/mch/brand',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(14,	'tra/ana/signup',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(15,	'tra/usr/signup2',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(16,	'tra/usr/signup3',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(17,	'tra/usr/bankDetails',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(18,	'tra/usr/addAdress',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(19,	'tra/trans/beatStoreVisitFinish',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(20,	'tra/trans/beatStoreVisitCancel',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(21,	'tra/trans/dayEnd',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(22,	'tra/trans/userinprocess',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(23,	'tra/trans/userDtl',	'1',	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01');

DROP TABLE IF EXISTS `mst_hq`;
CREATE TABLE `mst_hq` (
  `HQ_id` int NOT NULL AUTO_INCREMENT,
  `HQ_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `HQ_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int NOT NULL DEFAULT '1',
  `created_by` int NOT NULL,
  `updated_by` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`HQ_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_hq` (`HQ_id`, `HQ_name`, `HQ_code`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	'HQ 1',	'hq1',	1,	1,	1,	'2020-05-29 00:00:00',	'2020-05-29 00:00:00'),
(2,	'HQ 2',	'hq2',	1,	1,	1,	'2020-05-29 00:00:00',	'2020-05-29 00:00:00');

DROP TABLE IF EXISTS `mst_region`;
CREATE TABLE `mst_region` (
  `id` int NOT NULL AUTO_INCREMENT,
  `zone_id` int NOT NULL,
  `region_code` varchar(10) NOT NULL,
  `region_name` varchar(50) NOT NULL,
  `is_active` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `mst_region` (`id`, `zone_id`, `region_code`, `region_name`, `is_active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1,	2,	'test 123',	'test 123',	1,	'2020-05-22 09:40:05',	1,	'2020-05-23 09:49:21',	1,	NULL),
(2,	1,	'test 456',	'test 456',	1,	'2020-05-22 09:41:52',	1,	'2020-06-18 17:37:14',	1,	NULL),
(3,	2,	'tt',	'tt',	1,	'2020-06-18 17:36:54',	1,	'2020-06-18 17:37:35',	1,	NULL);

DROP TABLE IF EXISTS `mst_role`;
CREATE TABLE `mst_role` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) DEFAULT NULL,
  `role_access` text,
  `php_access` text,
  `mobile_access` text,
  `is_active` int DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_role` (`role_id`, `role_name`, `role_access`, `php_access`, `mobile_access`, `is_active`, `createdBy`, `updatedBy`, `createdAt`, `updatedAt`) VALUES
(1,	'Sales',	'{\"dayStart\":\"W\", \"beatList\":\"R\", \"pltCommonData\":\"R\", \"beatStoreDtl\":\"R\", \"beatStoreList\":\"R\", \"beatStoreVisitStart\":\"W\", \"timeLine\":\"R\", \"changebeat\":\"W\",  \"cat\":\"R\",   \"subcat\":\"R\",  \"childcat\":\"R\",  \"subchildcat\":\"R\", \"brand\":\"R\", \"signup\":\"W\", \"signup2\":\"W\", \"signup3\":\"W\", \"bankDetails\":\"W\", \"addAdress\":\"W\", \"beatStoreVisitFinish\":\"W\", \"beatStoreVisitCancel\":\"W\", \"dayEnd\":\"W\", \"userinprocess\":\"R\", \"userDtl\":\"R\"}',	NULL,	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22');

DROP TABLE IF EXISTS `mst_route`;
CREATE TABLE `mst_route` (
  `id` int NOT NULL AUTO_INCREMENT,
  `area_id` int DEFAULT NULL,
  `region_id` int DEFAULT NULL,
  `zone_id` int DEFAULT NULL,
  `route_name` varchar(50) DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_route` (`id`, `area_id`, `region_id`, `zone_id`, `route_name`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	1,	1,	1,	'Kandivali',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(2,	4,	1,	2,	'tttt',	1,	'1',	'1',	'2020-06-18 17:51:16',	'2020-06-18 17:51:16');

DROP TABLE IF EXISTS `mst_sales_team`;
CREATE TABLE `mst_sales_team` (
  `sales_team_id` int NOT NULL AUTO_INCREMENT,
  `sales_team_name` varchar(100) DEFAULT NULL,
  `emp_code` int DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `otp` varchar(6) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `zone_id` int DEFAULT NULL,
  `region_id` int DEFAULT NULL,
  `area_id` int DEFAULT NULL,
  `route_id` varchar(100) DEFAULT NULL,
  `HQ` int DEFAULT NULL,
  `user_img` varchar(50) DEFAULT NULL,
  `user_fcm` varchar(250) DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`sales_team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_sales_team` (`sales_team_id`, `sales_team_name`, `emp_code`, `mobile`, `otp`, `password`, `email`, `role_id`, `zone_id`, `region_id`, `area_id`, `route_id`, `HQ`, `user_img`, `user_fcm`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	'TestSale',	123456,	'9619026616',	NULL,	'$2b$12$zovQpxmyK9DcL1cFcCiwGu1JAQVrVeAH4sAb8k1/XmBnUyMehVufO',	'nitesh.cst@gmail.com',	1,	1,	1,	1,	'1,22,3',	1,	NULL,	NULL,	1,	'Nitesh',	NULL,	'2020-05-29 01:01:01',	'2020-06-15 10:29:07'),
(2,	'tirath',	123123,	'7977045133',	NULL,	'$2b$12$zovQpxmyK9DcL1cFcCiwGu1JAQVrVeAH4sAb8k1/XmBnUyMehVufO',	'tirath.cst@gmail.com',	1,	1,	1,	1,	'1,22,3',	1,	NULL,	NULL,	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-06-18 18:06:43'),
(3,	'Test',	5,	'1234567890',	NULL,	'eyJpdiI6IkV5a2Q2Ykt4dzFrL3E0MXNrcndNaHc9PSIsInZhbHVlIjoiSm9xVjhQSkxCbkpHRS9JUFdhbzBkZz09IiwibWFjIjoiZTM2MjQ2OTFiYjFjYTNlZGFkMWE5Y2Q0YzVjYmQxYTEzODA3MDg1MGFiZWRlOGFkY2ZhNmFiNTIwODI0NGEyNyJ9',	'test@gmail.com',	1,	1,	2,	1,	'1',	1,	NULL,	NULL,	1,	'1',	'1',	'2020-06-18 18:01:55',	'2020-06-18 18:06:10');

DROP TABLE IF EXISTS `mst_state`;
CREATE TABLE `mst_state` (
  `state_id` int NOT NULL AUTO_INCREMENT,
  `state_name` varchar(100) NOT NULL,
  `state_code` varchar(20) DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `mst_sub_cat`;
CREATE TABLE `mst_sub_cat` (
  `sub_cat_id` int NOT NULL AUTO_INCREMENT,
  `cat_id` int NOT NULL,
  `sub_cat_name` varchar(20) NOT NULL,
  `cat_dec` text,
  `sub_cat_img` text,
  `sub_cat_banner` text,
  `order_seq` int DEFAULT NULL,
  `is_active` int DEFAULT NULL COMMENT '0 = inactive and 1 = active',
  `created_by` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`sub_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_sub_cat` (`sub_cat_id`, `cat_id`, `sub_cat_name`, `cat_dec`, `sub_cat_img`, `sub_cat_banner`, `order_seq`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	1,	'Test sub category',	'd sdbjbjds jfkds',	'',	'',	4,	1,	1,	1,	'2020-05-22 16:13:53',	'2020-06-18 15:30:30');

DROP TABLE IF EXISTS `mst_sub_child_cat`;
CREATE TABLE `mst_sub_child_cat` (
  `sub_child_cat_id` int NOT NULL AUTO_INCREMENT,
  `child_cat_id` int NOT NULL,
  `sub_cat_id` int NOT NULL,
  `cat_id` int NOT NULL,
  `sub_child_cat_name` varchar(20) NOT NULL,
  `cat_dec` text,
  `sub_child_cat_img` text,
  `sub_child_cat_banner` text,
  `order_seq` int DEFAULT NULL,
  `is_active` int DEFAULT NULL COMMENT '0 = inactive and 1 = active',
  `created_by` int DEFAULT NULL,
  `updated_by` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`sub_child_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_sub_child_cat` (`sub_child_cat_id`, `child_cat_id`, `sub_cat_id`, `cat_id`, `sub_child_cat_name`, `cat_dec`, `sub_child_cat_img`, `sub_child_cat_banner`, `order_seq`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	1,	1,	1,	'Test Sub-Child Categ',	'sadsadsadsa',	'',	'',	4,	1,	1,	1,	'2020-05-22 16:15:10',	'2020-06-18 15:55:34');

DROP TABLE IF EXISTS `mst_systemcontrol`;
CREATE TABLE `mst_systemcontrol` (
  `id` int NOT NULL AUTO_INCREMENT,
  `run_cron_flag` int NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_systemcontrol` (`id`, `run_cron_flag`, `createdAt`, `updatedAt`) VALUES
(1,	1,	'2020-06-03 17:54:40',	'2020-06-03 17:54:40');

DROP TABLE IF EXISTS `mst_uom`;
CREATE TABLE `mst_uom` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uom_name` varchar(50) NOT NULL,
  `is_active` int NOT NULL DEFAULT '1' COMMENT '0 = inactive and 1 = active',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_uom` (`id`, `uom_name`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'UOM 1',	1,	'2020-05-24 00:00:00',	'2020-05-24 00:00:00',	NULL),
(2,	'UOM 2',	1,	'2020-05-24 00:00:00',	'2020-05-24 00:00:00',	NULL);

DROP TABLE IF EXISTS `mst_user`;
CREATE TABLE `mst_user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remember_token` varchar(200) NOT NULL DEFAULT '0',
  `otp` varchar(6) DEFAULT NULL,
  `otp_verified` int DEFAULT '0',
  `altr_mob_no` varchar(15) DEFAULT NULL,
  `whatsapp_mob_no` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `profile` varchar(20) DEFAULT NULL,
  `seller_type` varchar(15) DEFAULT NULL,
  `buyer_type` varchar(15) DEFAULT NULL,
  `gst_no` varchar(20) DEFAULT NULL,
  `comp_name` varchar(150) DEFAULT NULL,
  `geo_loc` varchar(100) DEFAULT NULL,
  `zone_id` int DEFAULT NULL,
  `region_id` int DEFAULT NULL,
  `area_id` int DEFAULT NULL,
  `route_id` int DEFAULT NULL,
  `buyer_img` varchar(100) DEFAULT NULL,
  `buyer_fcm` varchar(250) DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `possible_seller` varchar(5000) DEFAULT NULL,
  `not_possible_seller` varchar(5000) DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `mst_user` (`user_id`, `name`, `mobile`, `password`, `remember_token`, `otp`, `otp_verified`, `altr_mob_no`, `whatsapp_mob_no`, `email`, `profile`, `seller_type`, `buyer_type`, `gst_no`, `comp_name`, `geo_loc`, `zone_id`, `region_id`, `area_id`, `route_id`, `buyer_img`, `buyer_fcm`, `role_id`, `possible_seller`, `not_possible_seller`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`, `deleted_at`) VALUES
(1,	'NiteshTestOnboard',	'9619026616',	'$2b$12$PydoAzrbJ7YsKGRAexR7o./IOZbgAfBKv3Fgz9wTnyEDcrt0UIAVa',	'5rI4pmYiWiydfHpx4k1skhfeP4QTraNZvCRQRbUD5jtnVNEVLNgOhz6fjEoc',	'4572',	1,	'9807413225',	'2580796413',	'jj.f@gm.com',	'seller',	'',	'mf',	'123567890765432',	'Tttttt',	'-84.6620018',	1,	1,	1,	1,	NULL,	NULL,	1,	'2,2,',	NULL,	1,	'1',	'1',	'2020-06-03 17:54:40',	'2020-06-19 19:03:12',	NULL),
(2,	'NiteshTestOnboard',	'9920194974',	NULL,	'',	'5071',	0,	NULL,	NULL,	NULL,	'seller',	'testcrom',	NULL,	NULL,	NULL,	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	'1',	'1',	'2020-06-11 13:10:34',	'2020-06-18 17:57:14',	NULL),
(3,	'NiteshTestOnboard',	'9619026612',	NULL,	'0',	'2429',	0,	NULL,	NULL,	NULL,	'buyer',	'',	'mf',	NULL,	NULL,	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	4,	'1',	'1',	'2020-06-18 19:38:28',	'2020-06-18 19:41:10',	NULL),
(4,	'NiteshTestOnboard',	'8689903688',	NULL,	'0',	'1735',	0,	NULL,	NULL,	NULL,	'buyer',	'',	'mf',	NULL,	NULL,	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	4,	'1',	'1',	'2020-06-18 19:38:59',	'2020-06-19 12:13:00',	NULL),
(5,	'zeba',	'6565656565',	NULL,	'0',	'8332',	0,	NULL,	NULL,	NULL,	'buyer',	'',	'plastic',	NULL,	NULL,	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	4,	'1',	NULL,	'2020-06-18 19:43:19',	'2020-06-18 19:43:19',	NULL),
(6,	'ddd',	'2580147369',	NULL,	'0',	'5755',	0,	NULL,	NULL,	NULL,	'buyer',	NULL,	'plastic',	NULL,	NULL,	NULL,	2,	1,	4,	2,	NULL,	NULL,	NULL,	NULL,	NULL,	4,	'1',	NULL,	'2020-06-18 20:50:25',	'2020-06-18 20:50:25',	NULL),
(7,	'222',	'2222222222',	NULL,	'0',	'2381',	0,	NULL,	NULL,	NULL,	'buyer',	NULL,	'plastic',	NULL,	NULL,	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	4,	'1',	'1',	'2020-06-18 20:55:39',	'2020-06-18 20:55:43',	NULL),
(8,	'jilll',	'1234533589',	NULL,	'0',	'6442',	0,	NULL,	NULL,	NULL,	'buyer',	NULL,	'plastic',	NULL,	NULL,	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	4,	'1',	NULL,	'2020-06-18 21:00:37',	'2020-06-18 21:00:37',	NULL),
(9,	'tirath',	'7977045133',	NULL,	'0',	'1151',	0,	NULL,	NULL,	NULL,	'buyer',	NULL,	'plastic',	NULL,	NULL,	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	4,	'2',	'2',	'2020-06-19 13:32:46',	'2020-06-19 13:38:52',	NULL),
(10,	'wwww',	'2580369741',	NULL,	'0',	'5071',	0,	NULL,	NULL,	NULL,	'buyer',	NULL,	'plastic',	NULL,	NULL,	NULL,	1,	1,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	4,	'1',	NULL,	'2020-06-19 13:46:03',	'2020-06-19 13:46:03',	NULL);

DROP TABLE IF EXISTS `mst_vend_prod`;
CREATE TABLE `mst_vend_prod` (
  `id` int NOT NULL AUTO_INCREMENT,
  `prod_id` int NOT NULL,
  `mrp` decimal(10,0) NOT NULL,
  `inv_qty` int NOT NULL,
  `ret_minord_qnty` int NOT NULL,
  `ret_minord_qnty_cost_a` decimal(10,0) NOT NULL,
  `ret_minord_qnty_cost_b` decimal(10,0) NOT NULL,
  `ret_minord_qnty_cost_c` decimal(10,0) NOT NULL,
  `whs_minord_qnty` int NOT NULL,
  `whs_minord_qnty_cost_a` decimal(10,0) NOT NULL,
  `whs_minord_qnty_cost_b` decimal(10,0) NOT NULL,
  `whs_minord_qnty_cost_c` decimal(10,0) NOT NULL,
  `is_active` int NOT NULL DEFAULT '4' COMMENT '0 for inactive, 1 for active, 2 for request for review (submit), 3 for disapproved by admin, 4 for draft',
  `created_by` int NOT NULL,
  `updated_by` int NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `mst_vend_prod` (`id`, `prod_id`, `mrp`, `inv_qty`, `ret_minord_qnty`, `ret_minord_qnty_cost_a`, `ret_minord_qnty_cost_b`, `ret_minord_qnty_cost_c`, `whs_minord_qnty`, `whs_minord_qnty_cost_a`, `whs_minord_qnty_cost_b`, `whs_minord_qnty_cost_c`, `is_active`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	10,	10000,	10,	9,	8,	7,	20,	19,	18,	17,	4,	2,	2,	'2020-06-16 17:58:37',	'2020-06-16 17:58:37',	NULL),
(2,	2,	499,	10000,	10,	449,	439,	429,	100,	399,	389,	379,	4,	1,	1,	'2020-06-16 18:09:03',	'2020-06-16 18:09:03',	NULL),
(3,	3,	2000,	1000,	1001,	10,	10,	10,	200,	200,	200,	200,	2,	1,	1,	'2020-06-16 18:14:33',	'2020-06-16 18:17:40',	NULL),
(4,	1,	9,	200,	200,	199,	148,	147,	500,	200,	199,	198,	1,	1,	1,	'2020-06-16 18:21:22',	'2020-06-17 16:36:27',	NULL),
(5,	2,	150,	1500,	1000,	1100,	100,	100,	101,	1,	10,	10,	1,	1,	1,	'2020-06-17 17:12:00',	'2020-06-19 16:08:24',	NULL),
(6,	4,	10,	1,	1,	1,	0,	10,	10,	1,	1,	0,	2,	1,	1,	'2020-06-17 17:14:45',	'2020-06-19 16:06:26',	NULL),
(7,	1,	8,	1000,	100,	100,	100,	100,	100,	100,	100,	100,	1,	1,	1,	'2020-06-18 18:08:21',	'2020-06-18 18:21:03',	'2020-06-18 18:21:03'),
(8,	6,	55454,	4545,	454654,	64564,	65454,	6454,	54,	5465,	6554564,	56456,	2,	1,	1,	'2020-06-18 18:16:49',	'2020-06-18 18:20:40',	'2020-06-18 18:20:40');

DROP TABLE IF EXISTS `mst_zone`;
CREATE TABLE `mst_zone` (
  `id` int NOT NULL AUTO_INCREMENT,
  `zone_code` varchar(10) NOT NULL,
  `zone_name` varchar(50) NOT NULL,
  `is_active` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `mst_zone` (`id`, `zone_code`, `zone_name`, `is_active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1,	'Test Code',	'Test Name',	1,	'2020-05-21 12:00:15',	1,	'2020-06-18 17:36:11',	1,	NULL),
(2,	'dadsd',	'asdasdasd',	1,	'2020-05-21 12:07:34',	1,	'2020-06-18 17:36:05',	1,	NULL);

DROP TABLE IF EXISTS `plt_common`;
CREATE TABLE `plt_common` (
  `ptl_id` int NOT NULL AUTO_INCREMENT,
  `plt_name` varchar(50) DEFAULT NULL,
  `plt_val` varchar(15) DEFAULT NULL,
  `plt_disp_val` varchar(50) DEFAULT NULL,
  `plt_disp_priority` int DEFAULT NULL,
  `plt_data_type` varchar(25) DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`ptl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `plt_common` (`ptl_id`, `plt_name`, `plt_val`, `plt_disp_val`, `plt_disp_priority`, `plt_data_type`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	'seller_type',	'mf',	'Manufacturer',	1,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(2,	'other_act',	'ho_vst',	'Head Office Visit',	2,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(3,	'other_act',	'dp_vst',	'Depot Visit',	1,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(4,	'other_act',	'gt_vst',	'Gate Visit',	3,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(5,	'other_act',	'ss_vst',	'SuperStockist Visit',	4,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(6,	'other_act',	'oth_vst',	'Other',	5,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(7,	'off_work',	'meeting',	'Meeting',	1,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(8,	'off_work',	'training',	'Training',	2,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(9,	'off_work',	'prom_act',	'Promotional Activity',	3,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(10,	'off_work',	'oth_off',	'Others',	4,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(11,	'buyer_type',	'plastic',	'Plastic wala',	1,	'String',	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22');

DROP TABLE IF EXISTS `ref_user_address`;
CREATE TABLE `ref_user_address` (
  `addr_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `addr_title` varchar(100) DEFAULT NULL,
  `Addr_1` varchar(300) DEFAULT NULL,
  `Addr_2` varchar(300) DEFAULT NULL,
  `area_id` int DEFAULT NULL,
  `area_name` varchar(100) DEFAULT NULL,
  `city_id` int DEFAULT NULL,
  `city_name` varchar(100) DEFAULT NULL,
  `state_id` int DEFAULT NULL,
  `state_name` varchar(100) DEFAULT NULL,
  `pincode` varchar(7) DEFAULT NULL,
  `geo_loc` varchar(100) DEFAULT NULL,
  `is_default` int DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`addr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `ref_user_address` (`addr_id`, `user_id`, `addr_title`, `Addr_1`, `Addr_2`, `area_id`, `area_name`, `city_id`, `city_name`, `state_id`, `state_name`, `pincode`, `geo_loc`, `is_default`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	1,	'Test Title',	'Kandivali1',	'Kandivali2',	1,	'Kandivali',	1,	'Mumbai',	1,	'Maharashtra',	'400101',	'1.12424234,2.6586868',	1,	1,	'Nitesh',	NULL,	'2020-05-29 14:04:22',	'2020-05-29 14:04:22'),
(2,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'1.12424234,2.6586868',	NULL,	1,	'1',	NULL,	'2020-06-03 18:36:00',	'2020-06-03 18:36:00'),
(3,	2,	'Testing field entry',	'Kandivali east',	'Kandivali west',	1,	'Thakur mall',	1,	'Kandivali',	1,	'Maharashtra',	'400101',	'1.12424234,2.6586868',	1,	1,	'1',	NULL,	'2020-06-03 18:39:28',	'2020-06-03 18:39:28');

DROP TABLE IF EXISTS `ref_user_bank`;
CREATE TABLE `ref_user_bank` (
  `bank_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `bank_ifsc` varchar(50) DEFAULT NULL,
  `acct_no` varchar(50) DEFAULT NULL,
  `branch` varchar(50) DEFAULT NULL,
  `customer_name` varchar(50) DEFAULT NULL,
  `verification_comment` varchar(500) DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `ref_user_bank` (`bank_id`, `user_id`, `bank_name`, `bank_ifsc`, `acct_no`, `branch`, `customer_name`, `verification_comment`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	1,	NULL,	'SBI123456',	'12345675431123',	NULL,	'Nitesh',	NULL,	1,	'1',	NULL,	'2020-06-03 18:27:25',	'2020-06-03 18:27:25');

DROP TABLE IF EXISTS `ref_user_doc`;
CREATE TABLE `ref_user_doc` (
  `doc_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `doc_name` varchar(50) DEFAULT NULL,
  `doc_type` varchar(50) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL,
  `verification_comment` varchar(500) DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `ref_user_doc` (`doc_id`, `user_id`, `doc_name`, `doc_type`, `path`, `verification_comment`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	1,	'25 kw',	'25 kw',	'11591188250078.png',	NULL,	1,	'1',	NULL,	'2020-06-03 18:14:11',	'2020-06-03 18:14:11'),
(2,	1,	'AddEquipment',	'AddEquipment',	'11591188250775.svg',	NULL,	1,	'1',	NULL,	'2020-06-03 18:14:11',	'2020-06-03 18:14:11'),
(3,	1,	'HPhome',	'HPhome',	'11591188251068.svg',	NULL,	1,	'1',	NULL,	'2020-06-03 18:14:11',	'2020-06-03 18:14:11'),
(4,	1,	'time_table_1',	'time_table_1',	'11591189041687.jpeg',	NULL,	1,	'1',	NULL,	'2020-06-03 18:27:25',	'2020-06-03 18:27:25'),
(5,	1,	'BigCross',	'BigCross',	'11592573739242.svg',	NULL,	1,	'1',	NULL,	'2020-06-19 19:05:39',	'2020-06-19 19:05:39'),
(6,	1,	'BigCross',	'BigCross',	'11592573917837.svg',	NULL,	1,	'1',	NULL,	'2020-06-19 19:08:38',	'2020-06-19 19:08:38');

DROP TABLE IF EXISTS `routing`;
CREATE TABLE `routing` (
  `id` int NOT NULL AUTO_INCREMENT,
  `function` varchar(30) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `requiredParams` varchar(200) DEFAULT NULL,
  `access` varchar(255) DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `routing` (`id`, `function`, `path`, `requiredParams`, `access`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	'tra/trans/dayStart',	'/tra/trans/dayStart',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(2,	'tra/trans/beatList',	'/tra/trans/beatList',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(3,	'tra/trans/pltCommonData',	'/tra/trans/pltCommonData',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(4,	'tra/trans/beatStoreDtl',	'/tra/trans/beatStoreDtl',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(5,	'tra/trans/beatStoreList',	'/tra/trans/beatStoreList',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(6,	'tra/trans/beatStoreVisitStart',	'/tra/trans/beatStoreVisitStart',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(7,	'tra/ana/timeLine',	'/tra/ana/timeLine',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(8,	'tra/trans/changebeat',	'/tra/trans/changebeat',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(9,	'tra/mch/cat',	'/tra/mch/cat',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(10,	'tra/mch/subcat',	'/tra/mch/subcat',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(11,	'tra/mch/childcat',	'/tra/mch/childcat',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(12,	'tra/mch/subchildcat',	'/tra/mch/subchildcat',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(13,	'tra/mch/brand',	'/tra/mch/brand',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(14,	'tra/ana/signup',	'/tra/ana/signup',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(15,	'tra/usr/signup2',	'/tra/usr/signup2',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(16,	'tra/usr/signup3',	'/tra/usr/signup3',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(17,	'tra/usr/bankDetails',	'/tra/usr/bankDetails',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(18,	'tra/usr/addAdress',	'/tra/usr/addAdress',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(19,	'tra/trans/beatStoreVisitFinish',	'/tra/trans/beatStoreVisitFinish',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(20,	'tra/trans/beatStoreVisitCancel',	'/tra/trans/beatStoreVisitCancel',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(21,	'tra/trans/dayEnd',	'/tra/trans/dayEnd',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(22,	'tra/trans/userinprocess',	'/tra/trans/userinprocess',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01'),
(23,	'tra/trans/userDtl',	'/tra/trans/userDtl',	NULL,	'R',	1,	NULL,	NULL,	'2020-05-29 01:01:01',	'2020-05-29 01:01:01');

DROP TABLE IF EXISTS `trans_day_track`;
CREATE TABLE `trans_day_track` (
  `day_track_id` int NOT NULL AUTO_INCREMENT,
  `sales_team_id` int DEFAULT NULL,
  `sales_date` datetime DEFAULT NULL,
  `day_st_time` datetime DEFAULT NULL,
  `day_end_time` datetime DEFAULT NULL,
  `rout_id` int DEFAULT NULL,
  `visit_with_sales_team_id` int DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `geo_location_start` varchar(50) DEFAULT NULL,
  `geo_location_end` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`day_track_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `trans_day_track` (`day_track_id`, `sales_team_id`, `sales_date`, `day_st_time`, `day_end_time`, `rout_id`, `visit_with_sales_team_id`, `is_active`, `geo_location_start`, `geo_location_end`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	2,	'2020-06-19 13:27:11',	'2020-06-19 13:27:11',	NULL,	1,	NULL,	1,	'-84.662002',	NULL,	'2',	NULL,	'2020-06-19 13:27:11',	'2020-06-19 13:27:11');

DROP TABLE IF EXISTS `trans_day_visit`;
CREATE TABLE `trans_day_visit` (
  `day_visit_id` int NOT NULL AUTO_INCREMENT,
  `day_track_id` int DEFAULT NULL,
  `sales_team_id` int DEFAULT NULL,
  `st_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `visit_type` varchar(25) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `res_no_bus` varchar(25) DEFAULT NULL,
  `geo_location` varchar(50) DEFAULT NULL,
  `buyer_id` int DEFAULT NULL,
  `is_active` int DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`day_visit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `trans_day_visit` (`day_visit_id`, `day_track_id`, `sales_team_id`, `st_time`, `end_time`, `visit_type`, `comment`, `res_no_bus`, `geo_location`, `buyer_id`, `is_active`, `created_by`, `updated_by`, `createdAt`, `updatedAt`) VALUES
(1,	1,	1,	'2020-06-11 17:06:33',	'2020-06-11 17:10:07',	'mf',	'sasasas',	'Mind your own business',	'1.2345,3.5432',	1,	0,	'1',	'1',	'2020-06-11 17:06:32',	'2020-06-11 22:27:31'),
(2,	1,	1,	'2020-06-11 17:11:21',	NULL,	NULL,	'lllll',	NULL,	'-84.6620017',	1,	0,	'1',	'1',	'2020-06-11 17:11:21',	'2020-06-11 17:16:06'),
(3,	1,	1,	'2020-06-11 17:21:29',	NULL,	NULL,	'Ffffffffff',	NULL,	'-84.6620017',	1,	1,	'1',	NULL,	'2020-06-11 17:21:29',	'2020-06-11 17:21:29'),
(4,	1,	1,	'2020-06-11 17:25:19',	NULL,	NULL,	'jjjjj',	NULL,	'-84.6620017',	1,	1,	'1',	NULL,	'2020-06-11 17:25:19',	'2020-06-11 17:25:19'),
(5,	1,	1,	'2020-06-11 18:24:52',	NULL,	'meeting',	'gffgf',	NULL,	'92.0815133',	NULL,	1,	'1',	NULL,	'2020-06-11 18:24:52',	'2020-06-11 18:24:52'),
(6,	1,	1,	'2020-06-11 18:28:08',	NULL,	'training',	'2323',	NULL,	'92.0815133',	NULL,	1,	'1',	NULL,	'2020-06-11 18:28:08',	'2020-06-11 18:28:08'),
(7,	1,	1,	'2020-06-11 18:37:14',	NULL,	'training',	'232323',	NULL,	'92.0815133',	NULL,	1,	'1',	NULL,	'2020-06-11 18:37:14',	'2020-06-11 18:37:14'),
(8,	1,	1,	'2020-06-11 18:39:20',	NULL,	'prom_act',	'232323',	NULL,	'92.0815133',	NULL,	1,	'1',	NULL,	'2020-06-11 18:39:20',	'2020-06-11 18:39:20'),
(9,	1,	1,	'2020-06-11 18:59:39',	NULL,	NULL,	'xxxxx',	NULL,	'-84.6620017',	1,	1,	'1',	NULL,	'2020-06-11 18:59:39',	'2020-06-11 18:59:39'),
(10,	1,	1,	'2020-06-11 19:00:12',	NULL,	NULL,	'Dd',	NULL,	'-84.6620017',	1,	1,	'1',	NULL,	'2020-06-11 19:00:12',	'2020-06-11 19:00:12'),
(11,	1,	1,	'2020-06-11 19:53:38',	NULL,	'meeting',	'121',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-11 19:53:38',	'2020-06-11 19:53:38'),
(12,	1,	1,	'2020-06-11 21:32:48',	NULL,	'prom_act',	'qwqwqw',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-11 21:32:48',	'2020-06-11 21:32:48'),
(13,	1,	1,	'2020-06-11 21:42:26',	NULL,	'training',	'sas',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-11 21:42:26',	'2020-06-11 21:42:26'),
(14,	1,	1,	'2020-06-11 21:43:13',	NULL,	'training',	'sas',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-11 21:43:13',	'2020-06-11 21:43:13'),
(15,	1,	1,	'2020-06-11 23:05:47',	NULL,	'meeting',	'sasasasasn dacbadbads',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-11 23:05:47',	'2020-06-11 23:05:47'),
(16,	2,	1,	'2020-06-12 08:57:02',	NULL,	'meeting',	'1212',	NULL,	'-84.6620017',	NULL,	0,	'1',	'1',	'2020-06-12 08:57:02',	'2020-06-12 08:59:20'),
(17,	1,	1,	'2020-06-12 08:59:37',	NULL,	'prom_act',	'323232',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-12 08:59:37',	'2020-06-12 08:59:37'),
(18,	1,	1,	'2020-06-12 09:00:44',	NULL,	'meeting',	'1212122',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-12 09:00:44',	'2020-06-12 09:00:44'),
(19,	1,	1,	'2020-06-12 10:19:11',	NULL,	'bb',	'Loreum epsum',	NULL,	'123,321',	1,	1,	'1',	NULL,	'2020-06-12 10:19:11',	'2020-06-12 10:19:11'),
(20,	1,	1,	'2020-06-12 10:19:20',	NULL,	'1',	'Loreum epsum',	NULL,	'123,321',	1,	1,	'1',	NULL,	'2020-06-12 10:19:20',	'2020-06-12 10:19:20'),
(21,	1,	1,	'2020-06-12 11:55:37',	NULL,	'meeting',	'aasas',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-12 11:55:37',	'2020-06-12 11:55:37'),
(22,	1,	1,	'2020-06-12 12:10:09',	NULL,	'meeting',	'22121',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-12 12:10:09',	'2020-06-12 12:10:09'),
(23,	1,	1,	'2020-06-12 12:40:22',	NULL,	'meeting',	'eeee',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-12 12:40:22',	'2020-06-12 12:40:22'),
(24,	1,	1,	'2020-06-12 13:36:57',	NULL,	NULL,	'T',	NULL,	'-84.6620017',	1,	0,	'1',	'1',	'2020-06-12 13:36:57',	'2020-06-12 13:37:06'),
(25,	1,	1,	'2020-06-12 14:14:49',	NULL,	'training',	'ytd',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-12 14:14:49',	'2020-06-12 14:14:49'),
(26,	1,	1,	'2020-06-12 14:20:05',	NULL,	'prom_act',	'fghfhf',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-12 14:20:05',	'2020-06-12 14:20:05'),
(27,	1,	1,	'2020-06-12 14:21:28',	NULL,	'1',	'Loreum epsum',	NULL,	'123,321',	1,	1,	'1',	NULL,	'2020-06-12 14:21:27',	'2020-06-12 14:21:27'),
(28,	1,	1,	'2020-06-12 14:23:14',	NULL,	'HO',	NULL,	NULL,	'1.24242,2.34234234',	NULL,	1,	'1',	NULL,	'2020-06-12 14:23:14',	'2020-06-12 14:23:14'),
(29,	1,	1,	'2020-06-12 14:25:18',	NULL,	'HO',	NULL,	NULL,	'1.24242,2.34234234',	NULL,	1,	'1',	NULL,	'2020-06-12 14:25:18',	'2020-06-12 14:25:18'),
(30,	1,	1,	'2020-06-12 14:25:51',	NULL,	'HO',	NULL,	NULL,	'1.24242,2.34234234',	NULL,	1,	'1',	NULL,	'2020-06-12 14:25:50',	'2020-06-12 14:25:50'),
(31,	1,	1,	'2020-06-12 14:26:18',	NULL,	NULL,	'dss',	NULL,	'-84.6620017',	1,	1,	'1',	NULL,	'2020-06-12 14:26:18',	'2020-06-12 14:26:18'),
(32,	1,	1,	'2020-06-12 14:27:38',	NULL,	'training',	'fdg',	NULL,	'-84.6620017',	NULL,	0,	'1',	'1',	'2020-06-12 14:27:38',	'2020-06-12 14:27:44'),
(33,	1,	1,	'2020-06-12 14:28:13',	'2020-06-12 14:42:23',	'training',	'ccbv',	'',	'37.4219983,-122.084',	NULL,	0,	'1',	'1',	'2020-06-12 14:28:13',	'2020-06-12 15:10:41'),
(34,	1,	1,	'2020-06-12 14:53:40',	'2020-06-12 14:53:48',	'training',	'gfhgf',	'',	'37.4219983,-122.084',	NULL,	0,	'1',	'1',	'2020-06-12 14:53:40',	'2020-06-12 15:07:29'),
(35,	1,	1,	'2020-06-12 15:53:31',	NULL,	'training',	'asasa',	NULL,	'-84.6620017',	NULL,	0,	'1',	'1',	'2020-06-12 15:53:31',	'2020-06-12 15:53:35'),
(36,	1,	1,	'2020-06-12 16:15:33',	'2020-06-12 16:15:44',	NULL,	'wqwqwqw',	'',	'37.4219983,-122.084',	1,	1,	'1',	'1',	'2020-06-12 16:15:33',	'2020-06-12 16:15:44'),
(37,	1,	1,	'2020-06-12 16:26:51',	'2020-06-12 16:27:05',	'training',	'1212',	'',	'37.4219983,-122.084',	NULL,	1,	'1',	'1',	'2020-06-12 16:26:51',	'2020-06-12 16:27:05'),
(38,	1,	1,	'2020-06-12 16:27:29',	NULL,	NULL,	'asasa',	NULL,	'-84.6620017',	1,	1,	'1',	NULL,	'2020-06-12 16:27:29',	'2020-06-12 16:27:29'),
(39,	1,	1,	'2020-06-12 20:18:24',	NULL,	NULL,	'wew',	NULL,	'-84.6620552',	1,	0,	'1',	'1',	'2020-06-12 20:18:24',	'2020-06-12 20:20:36'),
(40,	1,	1,	'2020-06-12 20:22:16',	NULL,	NULL,	'ewew',	NULL,	'-84.6620552',	1,	0,	'1',	'1',	'2020-06-12 20:22:16',	'2020-06-12 20:22:21'),
(41,	1,	1,	'2020-06-13 01:41:08',	NULL,	'training',	'121',	NULL,	'-84.6620017',	NULL,	0,	'1',	'1',	'2020-06-13 01:41:08',	'2020-06-13 01:41:15'),
(42,	1,	1,	'2020-06-13 01:41:36',	NULL,	NULL,	'22',	NULL,	'-84.6620552',	1,	0,	'1',	'1',	'2020-06-13 01:41:36',	'2020-06-13 01:41:41'),
(43,	1,	1,	'2020-06-13 01:42:06',	'2020-06-13 01:43:31',	NULL,	'33',	'',	'37.4219983,-122.084',	1,	1,	'1',	'1',	'2020-06-13 01:42:06',	'2020-06-13 01:43:31'),
(44,	1,	1,	'2020-06-13 01:43:56',	NULL,	NULL,	'212',	NULL,	'-84.6620017',	1,	0,	'1',	'1',	'2020-06-13 01:43:56',	'2020-06-13 01:44:02'),
(45,	1,	1,	'2020-06-13 02:00:54',	NULL,	'prom_act',	'Ggvh',	NULL,	'92.17709690000001',	NULL,	0,	'1',	'1',	'2020-06-13 02:00:54',	'2020-06-13 02:00:57'),
(46,	1,	1,	'2020-06-13 02:01:26',	NULL,	NULL,	'Ghh',	NULL,	'92.17709690000001',	1,	0,	'1',	'1',	'2020-06-13 02:01:26',	'2020-06-13 02:01:30'),
(47,	1,	1,	'2020-06-13 02:01:42',	'2020-06-13 02:01:55',	NULL,	'Gg',	'',	'19.2899193,72.8871776',	1,	1,	'1',	'1',	'2020-06-13 02:01:42',	'2020-06-13 02:01:55'),
(48,	1,	1,	'2020-06-13 02:07:10',	NULL,	'training',	'2222',	NULL,	'-84.6620552',	NULL,	0,	'1',	'1',	'2020-06-13 02:07:10',	'2020-06-13 02:07:29'),
(49,	1,	1,	'2020-06-13 02:07:47',	NULL,	NULL,	'2222',	NULL,	'-84.6620552',	1,	0,	'1',	'1',	'2020-06-13 02:07:47',	'2020-06-13 10:40:36'),
(50,	1,	1,	'2020-06-13 02:08:10',	NULL,	NULL,	'G',	NULL,	'92.1715303',	1,	0,	'1',	'1',	'2020-06-13 02:08:10',	'2020-06-13 02:13:42'),
(51,	1,	1,	'2020-06-13 02:13:47',	NULL,	NULL,	'Vv',	NULL,	'92.1715303',	1,	0,	'1',	'1',	'2020-06-13 02:13:47',	'2020-06-13 09:33:26'),
(52,	1,	1,	'2020-06-13 10:39:47',	'2020-06-13 10:40:00',	NULL,	'Ohj',	'',	'19.1846114,72.8196059',	1,	1,	'1',	'1',	'2020-06-13 10:39:47',	'2020-06-13 10:40:00'),
(53,	1,	1,	'2020-06-13 18:14:40',	NULL,	'meeting',	'Geyehe',	NULL,	'91.9842309',	NULL,	0,	'1',	'1',	'2020-06-13 18:14:40',	'2020-06-13 18:15:00'),
(54,	2,	1,	'2020-06-13 18:15:16',	'2020-06-13 18:15:54',	'prom_act',	'Eehej',	'',	'19.1495619,72.834669',	NULL,	1,	'1',	'1',	'2020-06-13 18:15:16',	'2020-06-13 18:15:54'),
(55,	1,	1,	'2020-06-13 18:16:51',	'2020-06-13 18:16:55',	NULL,	'Hs',	'',	'19.1495619,72.834669',	1,	1,	'1',	'1',	'2020-06-13 18:16:51',	'2020-06-13 18:16:55'),
(56,	1,	1,	'2020-06-13 19:36:16',	'2020-06-13 19:36:30',	NULL,	'Rf',	'',	'19.149563,72.8346674',	1,	1,	'1',	'1',	'2020-06-13 19:36:16',	'2020-06-13 19:36:30'),
(57,	1,	1,	'2020-06-13 19:39:41',	NULL,	NULL,	'Cgu',	NULL,	'91.9842304',	1,	0,	'1',	'1',	'2020-06-13 19:39:41',	'2020-06-13 19:39:49'),
(58,	1,	1,	'2020-06-13 19:40:48',	'2020-06-13 19:40:58',	NULL,	'Vnb',	'',	'19.1495608,72.8346689',	1,	1,	'1',	'1',	'2020-06-13 19:40:48',	'2020-06-13 19:40:58'),
(59,	1,	1,	'2020-06-13 19:45:02',	NULL,	NULL,	'Fyfg',	NULL,	'91.9842297',	1,	0,	'1',	'1',	'2020-06-13 19:45:02',	'2020-06-13 19:45:12'),
(60,	1,	1,	'2020-06-13 19:45:03',	NULL,	NULL,	'Fyfg',	NULL,	'91.9842297',	1,	1,	'1',	NULL,	'2020-06-13 19:45:03',	'2020-06-13 19:45:03'),
(61,	1,	1,	'2020-06-13 19:45:29',	'2020-06-13 19:45:36',	NULL,	'Gb',	'',	'19.1495608,72.8346689',	1,	1,	'1',	'1',	'2020-06-13 19:45:29',	'2020-06-13 19:45:36'),
(62,	1,	1,	'2020-06-13 19:46:38',	'2020-06-13 19:46:43',	NULL,	'Vv',	'',	'19.1495608,72.8346689',	1,	1,	'1',	'1',	'2020-06-13 19:46:38',	'2020-06-13 19:46:43'),
(63,	1,	1,	'2020-06-13 19:46:39',	NULL,	NULL,	'Vv',	NULL,	'91.9842297',	1,	1,	'1',	NULL,	'2020-06-13 19:46:39',	'2020-06-13 19:46:39'),
(64,	1,	1,	'2020-06-13 19:49:14',	'2020-06-13 19:49:26',	NULL,	'Xvd',	'',	'19.1495607,72.834669',	1,	1,	'1',	'1',	'2020-06-13 19:49:14',	'2020-06-13 19:49:26'),
(65,	1,	1,	'2020-06-13 19:50:36',	'2020-06-13 19:50:56',	NULL,	'Gjgt',	'',	'19.1495607,72.834669',	1,	1,	'1',	'1',	'2020-06-13 19:50:36',	'2020-06-13 19:50:56'),
(66,	2,	1,	'2020-06-15 01:59:22',	'2020-06-15 01:59:43',	'training',	'Djdjd',	'',	'19.2096594,72.8386709',	NULL,	1,	'1',	'1',	'2020-06-15 01:59:22',	'2020-06-15 01:59:43'),
(67,	3,	1,	'2020-06-15 10:33:02',	NULL,	'meeting',	'123',	NULL,	'-84.6620017',	NULL,	0,	'1',	'1',	'2020-06-15 10:33:02',	'2020-06-15 10:33:12'),
(68,	1,	1,	'2020-06-15 10:35:58',	NULL,	'training',	'1234',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-15 10:35:58',	'2020-06-15 10:35:58'),
(69,	1,	1,	'2020-06-15 10:40:12',	NULL,	'prom_act',	'3232',	NULL,	'-84.6620017',	NULL,	0,	'1',	'1',	'2020-06-15 10:40:12',	'2020-06-15 10:40:17'),
(70,	1,	1,	'2020-06-15 19:08:54',	'2020-06-15 19:09:00',	NULL,	'Bn',	'',	'19.14956,72.8346689',	1,	1,	'1',	'1',	'2020-06-15 19:08:54',	'2020-06-15 19:09:00'),
(71,	1,	1,	'2020-06-15 20:34:10',	NULL,	'oth_off',	'hh',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-15 20:34:10',	'2020-06-15 20:34:10'),
(72,	4,	1,	'2020-06-16 13:09:36',	'2020-06-16 13:14:29',	'prom_act',	'sasa',	'',	'37.4219983,-122.084',	NULL,	1,	'1',	'1',	'2020-06-16 13:09:36',	'2020-06-16 13:14:29'),
(73,	1,	2,	'2020-06-16 16:38:34',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-16 16:38:34',	'2020-06-16 16:38:34'),
(74,	1,	2,	'2020-06-16 16:41:27',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-16 16:41:27',	'2020-06-16 16:41:27'),
(75,	1,	2,	'2020-06-16 16:41:39',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-16 16:41:39',	'2020-06-16 16:41:39'),
(76,	1,	2,	'2020-06-16 16:52:06',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-16 16:52:06',	'2020-06-16 16:52:06'),
(77,	3,	1,	'2020-06-16 16:52:38',	NULL,	'training',	'kkkk',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-16 16:52:38',	'2020-06-16 16:52:38'),
(78,	1,	2,	'2020-06-16 16:53:27',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-16 16:53:27',	'2020-06-16 16:53:27'),
(79,	1,	2,	'2020-06-16 16:54:33',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-16 16:54:33',	'2020-06-16 16:54:33'),
(80,	1,	2,	'2020-06-16 16:54:47',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-16 16:54:47',	'2020-06-16 16:54:47'),
(81,	1,	2,	'2020-06-16 16:55:11',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-16 16:55:11',	'2020-06-16 16:55:11'),
(82,	1,	2,	'2020-06-16 16:55:25',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-16 16:55:25',	'2020-06-16 16:55:25'),
(83,	1,	2,	'2020-06-16 16:55:45',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-16 16:55:45',	'2020-06-16 16:55:45'),
(84,	4,	1,	'2020-06-16 17:47:39',	NULL,	'prom_act',	'kkkk',	NULL,	'-84.6620017',	NULL,	1,	'1',	NULL,	'2020-06-16 17:47:39',	'2020-06-16 17:47:39'),
(85,	1,	2,	'2020-06-17 09:21:22',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 09:21:22',	'2020-06-17 09:21:22'),
(86,	1,	2,	'2020-06-17 09:22:01',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 09:22:01',	'2020-06-17 09:22:01'),
(87,	1,	2,	'2020-06-17 09:22:25',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 09:22:25',	'2020-06-17 09:22:25'),
(88,	1,	2,	'2020-06-17 09:23:31',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 09:23:31',	'2020-06-17 09:23:31'),
(89,	1,	2,	'2020-06-17 09:23:47',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 09:23:47',	'2020-06-17 09:23:47'),
(90,	1,	2,	'2020-06-17 09:41:31',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 09:41:31',	'2020-06-17 09:41:31'),
(91,	1,	2,	'2020-06-17 09:46:27',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 09:46:27',	'2020-06-17 09:46:27'),
(92,	1,	2,	'2020-06-17 09:57:39',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 09:57:39',	'2020-06-17 09:57:39'),
(93,	1,	2,	'2020-06-17 09:58:09',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 09:58:09',	'2020-06-17 09:58:09'),
(94,	1,	2,	'2020-06-17 10:03:16',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 10:03:16',	'2020-06-17 10:03:16'),
(95,	1,	2,	'2020-06-17 10:41:45',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-17 10:41:45',	'2020-06-17 10:41:45'),
(96,	11,	2,	'2020-06-18 11:41:23',	NULL,	NULL,	NULL,	NULL,	'19.283001,72.8885492',	1,	1,	'2',	NULL,	'2020-06-18 11:41:23',	'2020-06-18 11:41:23'),
(97,	11,	2,	'2020-06-18 11:57:00',	NULL,	NULL,	NULL,	NULL,	'19.283267,72.8890235',	1,	1,	'2',	NULL,	'2020-06-18 11:57:00',	'2020-06-18 11:57:00'),
(98,	11,	2,	'2020-06-18 14:03:55',	NULL,	NULL,	NULL,	NULL,	'19.283267,72.8890235',	1,	1,	'2',	NULL,	'2020-06-18 14:03:55',	'2020-06-18 14:03:55'),
(99,	1,	2,	'2020-06-18 15:25:56',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-18 15:25:56',	'2020-06-18 15:25:56'),
(100,	1,	2,	'2020-06-18 15:47:32',	NULL,	NULL,	NULL,	NULL,	'37.4219983,-122.084',	1,	1,	'2',	NULL,	'2020-06-18 15:47:32',	'2020-06-18 15:47:32');

-- 2020-06-19 18:23:20

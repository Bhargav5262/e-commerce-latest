<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateAdmin {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'admin') {

		if (Auth::guard('admin')->check()) {

			return $next($request);
		}

		return redirect('admin/')->with([
				'warning' => 'You must have to be logged in.',
			]);
	}   
}

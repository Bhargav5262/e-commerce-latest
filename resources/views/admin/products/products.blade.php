@extends('admin.app.index')

@section('title')
    {{$section ?? 'Admin'}}
@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{$ADMIN_THEME_PATH}}/app-assets/vendors/css/forms/tags/tagging.css">
<link href="{{$ADMIN_THEME_PATH}}/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
@stop

@section('model')

@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row mb-1">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Master Management</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">{{$section}} Management</a></li>
                        <li class="breadcrumb-item"><a href="{{url($sectionSlug)}}">{{$section}}</a></li>
                        <li class="breadcrumb-item active">{{ isset($view) ? ucfirst($view) : 'List'}}</li>
                    </ol>
                </div>
            </div>
            <h3 class="content-header-title mb-0">{{$singleSection}}</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ isset($view) ? ucfirst($view) : 'List'}} {{$singleSection}}
                                @if ( isset($view) && in_array($view, ['add','edit','view']) )
                                    <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary pull-right"><i class="ft-arrow-left"></i> Back To List</a>
                                @else
                                    <a href="{{ url( $sectionSlug , 'add') }}" class="btn btn-primary pull-right"><i class="ft-plus"></i> Add {{ $singleSection }}</a>
                                @endif
                            </h4>
                        </div>    
                        @if ( isset($view) && in_array($view, ['add','edit']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST" name="product_form" id="product_form" class="form" action="{{url($sectionSlug.'/action/')}}/{{$view}}/{{isset($data->id) ? $data->id: '0'}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-body my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="cat_id">Category</label>
                                                        <select class="form-control round" name="cat_id" id="cat_id">
                                                            <option value="">Select category</option>
                                                            @foreach($category as $cat)
                                                                <option value="{{ $cat->cat_id }}" @if(isset($data) && $cat->cat_id == $data->cat_id) selected @endif>{{ $cat->cat_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>    
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="sub_cat_id">Sub Category</label>
                                                        <select class="form-control round" name="sub_cat_id" id="sub_cat_id">
                                                            <option value="">Select sub category</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="child_cat_id">Child Category</label>
                                                        <select class="form-control round" name="child_cat_id" id="child_cat_id">
                                                            <option value="">Select child category</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="child_sub_cat_id">Child Sub Category</label>
                                                        <select class="form-control round" name="child_sub_cat_id" id="child_sub_cat_id">
                                                            <option value="">Select child sub category</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="brand_id">Brand *</label>
                                                        <select class="form-control round" name="brand_id" id="brand_id">
                                                            <option value="">Select Brand</option>
                                                            @if(isset($brands) && count($brands) > 0)
                                                                @foreach($brands as $brand)
                                                                    <option value="{{ $brand->brand_id }}" @if(isset($data) && $brand->brand_id == $data->brand_id) selected @endif>{{ $brand->brand_name }}</option>
                                                                @endforeach
                                                            @endif    
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="manufacturer">Manufacturer Name*</label>
                                                        <input type="text" name="manufacturer" id="manufacturer" value="{{$data->manufacturer ?? old('manufacturer')}}" class="form-control round" placeholder="manufacturer name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="product_type">Products Type</label><br>
                                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                                            <input type="radio" class="custom-control-input" name="product_type" value="1" @if(isset($data) && $data->product_type == "1") checked @endif id="radio1">
                                                            <label class="custom-control-label" for="radio1">Blubuck Products</label>
                                                        </div>
                                                        <div class="d-inline-block custom-control custom-radio mr-1">
                                                            <input type="radio" class="custom-control-input" name="product_type" value="2" @if(isset($data) && $data->product_type == "2") checked @endif id="radio2">
                                                            <label class="custom-control-label" for="radio2">Other Products</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="barcode">BARCODE/EAN NO*</label>
                                                        <input type="text" name="barcode" id="barcode" value="{{$data->barcode ?? old('barcode')}}" class="form-control round" placeholder="BARCODE/EAN NO">
                                                        <input type="hidden" name="_barcode" id="_barcode" value="{{$data->barcode ?? old('barcode')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="hsn_no">HSN No*</label>
                                                        <input type="text" name="hsn_no" id="hsn_no" value="{{$data->hsn_no ?? old('hsn_no')}}" class="form-control round" placeholder="HSN No">
                                                        <input type="hidden" name="_hsn_no" id="_hsn_no" value="{{$data->hsn_no ?? old('hsn_no')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="product_name">ITEM FULL NAME*</label>
                                                        <input type="text" name="product_name" id="product_name" value="{{$data->product_name ?? old('product_name')}}" class="form-control round" placeholder="ITEM FULL NAME">
                                                        <input type="hidden" name="_product_name" id="_product_name" value="{{$data->product_name ?? old('product_name')}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">    
                                                    <div class="form-group">
                                                        <label for="prod_desc">Product description*</label>
                                                        <textarea name="prod_desc" id="prod_desc" class="form-control round" placeholder="Product description">{{$data->prod_desc ?? old('prod_desc')}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="sku_size">Size (sku size)</label>
                                                        <input type="text" name="sku_size" id="sku_size" value="{{$data->sku_size ?? old('sku_size')}}" class="form-control round" placeholder="Size (sku size)">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="uom_id">UOM</label>
                                                        <select class="form-control round" name="uom_id" id="uom_id">
                                                            <option value="">Select UOM</option>
                                                            @foreach($uom as $row)
                                                                <option value="{{ $row->id }}" @if(isset($data) && $row->id == $data->uom_id) selected @endif>{{ $row->uom_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="gst">GST%</label>
                                                        <input type="text" name="gst" id="gst" value="{{$data->gst ?? old('gst')}}" class="form-control round" placeholder="GST%">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="igst">IGST</label>
                                                        <input type="text" name="igst" id="igst" value="{{$data->igst ?? old('igst')}}" class="form-control round" placeholder="IGST">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="sgst">SGST</label>
                                                        <input type="text" name="sgst" id="sgst" value="{{$data->sgst ?? old('sgst')}}" class="form-control round" placeholder="SGST">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="case_size">Case Size means no of unit in one case</label>
                                                        <input type="text" name="case_size" id="case_size" value="{{$data->case_size ?? old('case_size')}}" class="form-control round" placeholder="Case Size means no of unit in one case">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="case_weight">Case Weight(actual)</label>
                                                        <input type="text" name="case_weight" id="case_weight" value="{{$data->case_weight ?? old('case_weight')}}" class="form-control round" placeholder="Case Weight(actual)">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="case_dimen">Case dimension(volume) H*W*L</label>
                                                        <input type="text" name="case_dimen" id="case_dimen" value="{{$data->case_dimen ?? old('case_dimen')}}" class="form-control round" placeholder="Case dimension(volume) H*W*L">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="prod_weight">Product Weight(actual)</label>
                                                        <input type="text" name="prod_weight" id="prod_weight" value="{{$data->prod_weight ?? old('prod_weight')}}" class="form-control round" placeholder="Product Weight(actual)">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="prod_dimen">Product dimension(volume) H*W*L</label>
                                                        <input type="text" name="prod_dimen" id="prod_dimen" value="{{$data->prod_dimen ?? old('prod_dimen')}}" class="form-control round" placeholder="Product dimension(volume) H*W*L">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="shelf_life">Shelf Life In Days( day,month,Year )</label>
                                                        <input type="text" name="shelf_life" id="shelf_life" value="{{$data->shelf_life ?? old('shelf_life')}}" class="form-control round" placeholder="Shelf Life In Days( daya,month,Year )">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ord_qnt_rtl_1">Set No For Ord Qnt retail 1</label>
                                                        <input type="text" name="ord_qnt_rtl_1" id="ord_qnt_rtl_1" value="{{$data->ord_qnt_rtl_1 ?? old('ord_qnt_rtl_1')}}" class="form-control round" placeholder="Set No For Ord Qnt retail 1">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ord_qnt_rtl_2">Set No For Ord Qnt retail 2</label>
                                                        <input type="text" name="ord_qnt_rtl_2" id="ord_qnt_rtl_2" value="{{$data->ord_qnt_rtl_2 ?? old('ord_qnt_rtl_2')}}" class="form-control round" placeholder="Set No For Ord Qnt retail 2">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ord_qnt_rtl_3">Set No For Ord Qnt retail 3</label>
                                                        <input type="text" name="ord_qnt_rtl_3" id="ord_qnt_rtl_3" value="{{$data->ord_qnt_rtl_3 ?? old('ord_qnt_rtl_3')}}" class="form-control round" placeholder="Set No For Ord Qnt retail 3">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ord_qnt_bulk_1">Set No For Ord Qnt Bulk 1</label>
                                                        <input type="text" name="ord_qnt_bulk_1" id="ord_qnt_bulk_1" value="{{$data->ord_qnt_bulk_1 ?? old('ord_qnt_bulk_1')}}" class="form-control round" placeholder="Set No For Ord Qnt Bulk 1">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ord_qnt_bulk_2">Set No For Ord Qnt Bulk 2</label>
                                                        <input type="text" name="ord_qnt_bulk_2" id="ord_qnt_bulk_2" value="{{$data->ord_qnt_bulk_2 ?? old('ord_qnt_bulk_2')}}" class="form-control round" placeholder="Set No For Ord Qnt Bulk 2">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="ord_qnt_bulk_3">Set No For Ord Qnt Bulk 3</label>
                                                        <input type="text" name="ord_qnt_bulk_3" id="ord_qnt_bulk_3" value="{{$data->ord_qnt_bulk_3 ?? old('ord_qnt_bulk_3')}}" class="form-control round" placeholder="Set No For Ord Qnt Bulk 3">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="margin_rtl">Margine Set For retail</label>
                                                        <input type="text" name="margin_rtl" id="margin_rtl" value="{{$data->margin_rtl ?? old('margin_rtl')}}" class="form-control round" placeholder="Margine Set For retail">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="margin_bulk">Margin Set for Bulk</label>
                                                        <input type="text" name="margin_bulk" id="margin_bulk" value="{{$data->margin_bulk ?? old('margin_bulk')}}" class="form-control round" placeholder="Margin Set for Bulk">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="comm_team_rtl">Commision Set for retails</label>
                                                        <input type="text" name="comm_team_rtl" id="comm_team_rtl" value="{{$data->comm_team_rtl ?? old('comm_team_rtl')}}" class="form-control round" placeholder="Commision Set for retails">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="comm_team_bulk">Comission Set For Bulk</label>
                                                        <input type="text" name="comm_team_bulk" id="comm_team_bulk" value="{{$data->comm_team_bulk ?? old('comm_team_bulk')}}" class="form-control round" placeholder="Comission Set For Bulk">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="comm_frl_rtl">Comission Set For freelancer</label>
                                                        <input type="text" name="comm_frl_rtl" id="comm_frl_rtl" value="{{$data->comm_frl_rtl ?? old('comm_frl_rtl')}}" class="form-control round" placeholder="Comission Set For freelancer">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="comm_frl_bulk">Comission Set For Freelancer Bulk</label>
                                                        <input type="text" name="comm_frl_bulk" id="comm_frl_bulk" value="{{$data->comm_frl_bulk ?? old('comm_frl_bulk')}}" class="form-control round" placeholder="Comission Set For Freelancer Bulk">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="prod_tag">Search Key word tag</label>
                                                        <div class="case-sensitive form-control round" data-tags-input-name="prod_tag" style="min-height: 40px;">{{$data->prod_tag ?? old('prod_tag')}}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">    
                                                    <div class="form-group">
                                                        <label for="is_active">Status</label>
                                                        <select class="form-control round" name="is_active" id="is_active">
                                                            <option value="1" @if(isset($data) && $data->is_active == "1") selected @endif>Active</option>
                                                            <option value="0" @if(isset($data) && $data->is_active == "0") selected @endif>Inactive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> 
                                            <h4 class="form-section"><i class="ft-image"></i> Catalog Media</h4>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="media_files">Default Media</label>
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px;">
                                                                        @if( isset($defaultImage->media) && $defaultImage->media != '' )
                                                                        <img src="{{ $defaultImage->media }}" style="max-width: 200px;" alt= />
                                                                        @else
                                                                        <img src="{{url('public/images/no-image.png')}}" style="max-width: 200px;"  alt= />  
                                                                        @endif
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select Image / Video </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="main_image" class="@if(isset($view) && in_array($view, ['edit']) ) ignore @endif"> </span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="form-group">
                                                                <label>Your image recommendations</label>
                                                                <div class="card-text">
                                                                    <p>1) Choose images that are clear, information-rich and attractive.</p>
                                                                    <p>2) Products must fill at least 85% of the image.</p>
                                                                    <p>3) Images must show only the product that is for sale, with few or no props and with no logos, watermarks or inset images.</p>
                                                                    <p>4) Images must be at least 1000 pixels on the longest side and at least 500 pixels on the shortest side to be zoom-able.</p>
                                                                    <p>5) Images must not exceed 10000 pixels on the longest side.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div> 

                                            @if(isset($productImages) && count($productImages))
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                        @foreach($productImages as $key => $val)
                                                            <div class="col-md-3 trashmedia-{{$val->id}}">
                                                                <a href="javascript:void(0);" class="pull-right" onclick="TrashMedia('{{$val->id}}');"><i class="ft-trash mr-1"></i></a>
                                                                <div class="form-group">
                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                        <div class="fileinput-new thumbnail" style="width: 200px;">
                                                                            @if( isset($val->media) && $val->media != '' )
                                                                                <img src="{{ $val->media }}" style="max-width: 200px;" alt= />
                                                                            @else
                                                                                <img src="{{url('public/images/no-image.png')}}" style="max-width: 200px;" alt= />
                                                                            @endif    
                                                                        </div>
                                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>
                                                                        <div>
                                                                            <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> Select Image / Video </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="media_files[]" >
                                                                            <input type="hidden" name="ids[]" value="{{$key}}"></span>
                                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row multi-imgs">
                                                        <div class="col-md-3 single-img">
                                                            <div class="form-group">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px;">
                                                                        <img src="{{url('public/images/no-image.png')}}" style="max-width: 200px;" alt= />
                                                                    </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                        <span class="fileinput-new"> Select Image / Video </span>
                                                                        <span class="fileinput-exists"> Change </span>
                                                                        <input type="file" name="media_files[]" >
                                                                        <input type="hidden" name="ids[]" value=""></span>
                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="javascript:void(0);" class="btn btn-info pull-left addmore">
                                                        <i class="ft-plus mr-1"></i> Add More Files
                                                    </a>
                                                </div>
                                            </div>


                                        </div>
                                        @if ( isset($view) && in_array($view, ['add','edit']) )
                                            <div class="form-actions">
                                                <a href="{{url($sectionSlug)}}" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        @elseif ( isset($view) && in_array($view, ['view']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Category Name:</label>
                                                    <label class="col-md-8 label-control">{{$data->cat_name ?? old('cat_name')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Category Images:</label>
                                                    @if(isset($data->cat_img) && !empty($data->cat_img))
                                                        <div class="row">
                                                            @foreach($data->cat_img as $key => $row)
                                                                <figure class="col-lg-1 col-md-6 col-12">
                                                                   <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                                </figure>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Category Banner:</label>
                                                    @if(isset($data->cat_banner) && !empty($data->cat_banner))
                                                        <div class="row">
                                                            @foreach($data->cat_banner as $key => $row)
                                                                <figure class="col-lg-1 col-md-6 col-12">
                                                                   <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                                </figure>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Status:</label>
                                                    <label class="col-md-8 label-control">{{$data->status ?? old('status')}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary"><i class="ft-arrow-left"></i> Back To List</a>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="{{$singleSection}}-table" class="table table-striped table-bordered text-inputs-searching">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Brand</th>
                                                    <th>Product Code</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Brand</th>
                                                    <th>Product Code</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                @if(isset($data) && count($data) > 0)
                                                    @foreach($data as $key => $row)
                                                        <tr>
                                                            <td>
                                                                @if( isset($row->product_image->media) && $row->product_image->media != '' )
                                                                    <img class="media-object width-100" style="max-width: 80px; max-height: 80px;" src="{{ $row->product_image->media }}" alt="">
                                                                @else
                                                                    <img class="media-object width-100" style="max-width: 80px; max-height: 80px;" src="{{url('public/images/no-image.png')}}" alt="">
                                                                @endif
                                                            </td>
                                                            <td>{{ $row->product_name }}</td>
                                                            <td>{{ isset($row->category->cat_name) ? $row->category->cat_name : '' }}</td>
                                                            <td>{{ isset($row->brand->brand_name) ? $row->brand->brand_name : '' }}</td>
                                                            <td>{{ $row->barcode }}</td>
                                                            {{--<td><a href="{{ url( $sectionSlug , [ 'view' , $row->id] ) }}"><i class="ft-eye"></i></a> | <a href="{{ url( $sectionSlug , [ 'edit' , $row->id ] ) }}"><i class="la la-pencil"></i></a></td>--}}
                                                            <td>
                                                            <a href="{{ url( $sectionSlug , [ 'edit' , $row->id ] ) }}"><i class="la la-pencil"></i></a> | 
                                                            {{--@if( ($row->is_active == '2') )
                                                                <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                                                                    <button class="btn btn-info round dropdown-toggle dropdown-menu-right box-shadow-2 px-2" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ft-settings icon-left"></i></button>
                                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                                        <a class="dropdown-item" href="{{ url( $sectionSlug ,[ $row->id , 1, 'application']) }}">Approve</a>
                                                                        <a class="dropdown-item" href="{{ url( $sectionSlug ,[ $row->id , 3, 'application']) }}">Disapprove</a>
                                                                    </div>
                                                                </div>
                                                            @endif--}}
                                                            @if( ($row->is_active == '0' || $row->is_active == '1') )
                                                                <a href="{{ url( $sectionSlug ,[ $row->id , $row->is_active, 'status']) }}" title="Change Status for active or inactive" >
                                                                    @if( $row->is_active == '0' )
                                                                        <i class="la la-check"></i>
                                                                    @else
                                                                        <i class="la la-times"></i>
                                                                    @endif
                                                                </a>
                                                            @endif
                                                        </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="3" class="text-center">No data found.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('jspage')
<script src="{{$ADMIN_THEME_PATH}}/app-assets/vendors/js/forms/tags/tagging.min.js"></script>
<script src="{{$ADMIN_THEME_PATH}}/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // case sensitive
        $(".case-sensitive").tagging({
            "case-sensitive": false,
            "no-duplicate": false,
        });
        @if(isset($view) && in_array($view, ['add','edit']))
            getSubSelectOption($('#cat_id').val(),"{{url('admin/child-category/sub-category')}}");
            $('#cat_id').change(function() {
                getSubSelectOption($(':selected').val(),"{{url('admin/child-category/sub-category')}}");
            });
            function getSubSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'cat_id':val}).done(function(response) {
                    $('#sub_cat_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#sub_cat_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    @if(isset($view) && in_array($view, ['edit']))
                        $('#sub_cat_id').val("{{$data->sub_cat_id ?? ""}}");
                    @endif
                    getChildSelectOption($('#sub_cat_id').val(),"{{url('admin/sub-child-category/child-category')}}");    
                });
            }
            $('#sub_cat_id').change(function() {
                getChildSelectOption($('#sub_cat_id').val(),"{{url('admin/sub-child-category/child-category')}}");
            });
            function getChildSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'sub_cat_id':val}).done(function(response) {
                    $('#child_cat_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#child_cat_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    @if(isset($view) && in_array($view, ['edit']))
                        $('#child_cat_id').val("{{$data->child_cat_id ?? ""}}");
                    @endif
                    getChildSubSelectOption($('#child_cat_id').val(),"{{url('admin/sub-child-category/child-sub-category')}}");
                });
            }
            $('#child_cat_id').change(function() {
                getChildSubSelectOption($('#child_cat_id').val(),"{{url('admin/sub-child-category/child-sub-category')}}");
            });
            function getChildSubSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'child_cat_id':val}).done(function(response) {
                    $('#child_sub_cat_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#child_sub_cat_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    @if(isset($view) && in_array($view, ['edit']))
                        $('#child_sub_cat_id').val("{{$data->child_sub_cat_id ?? ""}}");
                    @endif
                });
            }
        @endif
        $(".addmore").click(function(event) {
            var ele = $('.multi-imgs');
            var str = '<div class="col-md-3 single-img">'
                            +'<div class="form-group">'
                                +'<div class="fileinput fileinput-new" data-provides="fileinput">'
                                    +'<div class="fileinput-new thumbnail" style="width: 200px;">'
                                        +'<img src="{{url('public/images/no-image.png')}}" style="max-width: 200px;" alt= />'
                                    +'</div>'
                                    +'<div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>'
                                    +'<div>'
                                        +'<span class="btn default btn-file">'
                                        +'<span class="fileinput-new"> Select Image / Video </span>'
                                        +'<span class="fileinput-exists"> Change </span>'
                                        +'<input type="file" name="media_files[]" >'
                                        +'<input type="hidden" name="ids[]" value=""></span>'
                                        +'<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        +'</div>';
                ele.append(str);
        });
    });
    function TrashMedia(id) {
        cp_app.ajaxRequest("{{url('admin/products/trashmedia')}}",{_token:cp_token.getToken(),'id':id}).done(function(response) {
            $('.trashmedia-'+id).remove();
        });
    }
</script>
@stop

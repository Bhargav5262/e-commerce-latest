<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Eloquent, Request;

class Zone extends Eloquent {

	use SoftDeletes;

	protected $table = 'mst_zone';

	public $timestamps = true;

	protected $fillable = [
		'zone_code',
		'zone_name',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $dates = ['deleted_at'];

}
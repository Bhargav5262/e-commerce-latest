<?php
namespace App\Http\Controllers\Admin\Category;
use Request, Lang, File, Storage;
use App\Http\Controllers\Controller;
use App\Models\Category\SubCategory as SubCategoryModel;
use App\Models\Category\Category as CategoryModel;

class SubCategory extends Controller {

	protected $section;
	protected $singleSection;
    protected $viewPath;
    protected $sectionSlug;

	public function __construct(){
        $this->section = 'Sub Category';
		$this->singleSection = 'Sub Category';
        $this->viewPath = 'admin/category/sub-category';
        $this->sectionSlug = 'admin/sub-category';
	}

	public function index() {
		$data = SubCategoryModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->orderBy('sub_cat_id',"DESC")->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"list",
			'data'=>$data,
		);
		return view($this->viewPath, $_data);
	}

	public function Add() {
		$category = CategoryModel::where('is_active','1')->orderBy('cat_name','ASC')->get();
		$_data=array(
			'section'=>$this->section,
			'singleSection'=>$this->singleSection,
			'sectionSlug'=>$this->sectionSlug,
			'view'=>"add",
			'category' => $category,
		);
		return view($this->viewPath, $_data);
	}
	
	public function Edit($id="") {
        $data = SubCategoryModel::where('sub_cat_id',$id)->first();
        if(isset($data) && !empty($data)):
			$category = CategoryModel::where('is_active','1')->orderBy('cat_name','ASC')->get();
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	        	'sectionSlug'=>$this->sectionSlug,
	            'view'=>"edit",
	            'data'=>$data,
				'category' => $category,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function View($id="") {
        $data = SubCategoryModel::selectRaw('*,(CASE WHEN is_active = "0" THEN "Inactive" WHEN is_active = "1" THEN "Active" END) AS status')->where('sub_cat_id',$id)->first();
        if(isset($data) && !empty($data)):
			$category = CategoryModel::where('is_active','1')->orderBy('cat_name','ASC')->get();
	        $_data=array(
	        	'section'=>$this->section,
	        	'singleSection'=>$this->singleSection,
	        	'sectionSlug'=>$this->sectionSlug,
	            'view'=>"view",
	            'data'=>$data,
				'category' => $category,
	        );
	        return view($this->viewPath, $_data);
	    else:
			return redirect($this->sectionSlug)->with('error', "No ".$this->singleSection." found.");
		endif;
    }

    public function Action($action="",$id="") {
		$post_data = Request::all();
	    unset($post_data['_token']);
	    $userId = 0;
		if( auth()->guard('admin')->check() ):
			$userId = auth()->guard('admin')->user()->id;
		endif;
		if($action=="add"):
			$existCategory = SubCategoryModel::where('sub_cat_name',$post_data['sub_cat_name'])->get();
          	if(count($existCategory)):
            	return redirect($this->sectionSlug)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section.' Name' ])); 
          	endif;
			##  Images
          	if(isset($post_data['sub_cat_img_files']) && $post_data['sub_cat_img_files'] != ''):
		        if( Request::file() ):
		            $images = Request::file('sub_cat_img_files');
		            if($images):
		            	$imagesdata = array();
		            	foreach($images as $key => $row):
		            		$fileName = 'img_'.$userId.'_'.time().'_'.$row->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/sub-category/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($row));
           					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/sub-category/' . $fileName;
			                $imagesdata[] = _preg_replace($filePath);
		            	endforeach;	
			        	$post_data['sub_cat_img'] = $imagesdata;
		            endif;
		        endif;
		    endif;
		    #end
		    ##  Banner
          	if(isset($post_data['sub_cat_banner_files']) && $post_data['sub_cat_banner_files'] != ''):
		        if( Request::file() ):
		            $images = Request::file('sub_cat_banner_files');
		            if($images):
		            	$bannerdata = array();
		            	foreach($images as $key => $row):
		            		$fileName = 'banner_'.$userId.'_'.time().'_'.$row->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/sub-category/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($row));
           					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/sub-category/' . $fileName;
			                $bannerdata[] = _preg_replace($filePath);
		            	endforeach;	
			        	$post_data['sub_cat_banner'] = $bannerdata;
		            endif;
		        endif;
		    endif;
		    #end
		    $post_data['created_by'] = $userId;
		    $post_data['updated_by'] = $userId;
            SubCategoryModel::create($post_data);
        	return redirect($this->sectionSlug)->with( 'success', Lang::get('message.detailAdded', [ 'section' => $this->section ])); 
        elseif($action=="edit"):
        	$existCategory = SubCategoryModel::where('sub_cat_name',$post_data['sub_cat_name'])->where('sub_cat_id',"!=",$id)->get();
          	if(count($existCategory)): 
            	return redirect($this->sectionSlug.'/edit/'.$id)->with( 'warning', Lang::get('message.keyExist', [ 'section' => $this->section.' Name' ])); 
          	endif;
        	##  Images
          	if(isset($post_data['sub_cat_img_files']) && $post_data['sub_cat_img_files'] != ''):
		        if( Request::file() ):
		            $images = Request::file('sub_cat_img_files');
		            if($images):
		            	$imagesdata = array();
		            	foreach($images as $key => $row):
		            		$fileName = 'img_'.$userId.'_'.time().'_'.$row->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/sub-category/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($row));
           					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/sub-category/' . $fileName;
			                $imagesdata[] = _preg_replace($filePath);
		            	endforeach;	
			        	$post_data['sub_cat_img'] = $imagesdata;
		            endif;
		        endif;
		    endif;
		    #end
		    ##  Banner
          	if(isset($post_data['sub_cat_banner_files']) && $post_data['sub_cat_banner_files'] != ''):
		        if( Request::file() ):
		            $images = Request::file('sub_cat_banner_files');
		            if($images):
		            	$bannerdata = array();
		            	foreach($images as $key => $row):
		            		$fileName = 'banner_'.$userId.'_'.time().'_'.$row->getClientOriginalName();
		            		$fileName = str_replace(' ', '_', $fileName);
			                $filePath = '/'.env('S3_FOLDER').'/sub-category/' . $fileName;
			                Storage::disk('s3')->put($filePath, file_get_contents($row));
           					$filePath = env('S3_PATH').'/'.env('AWS_BUCKET').'/'.env('S3_FOLDER').'/sub-category/' . $fileName;
			                $bannerdata[] = _preg_replace($filePath);
		            	endforeach;	
			        	$post_data['sub_cat_banner'] = $bannerdata;
		            endif;
		        endif;
		    endif;
		    #end
		    $post_data['updated_by'] = $userId;
    		SubCategoryModel::find($id)->update($post_data);
            return redirect($this->sectionSlug.'/edit/'.$id)->with( 'success', Lang::get('message.detailUpdated', [ 'section' => $this->section ]));
		elseif($action=="delete"):
            $_data = SubCategoryModel::where('sub_cat_id',$id)->first();
        	if(isset($_data)):
                $_data->delete();
                $arr = array("success"=>"true","message"=>Lang::get('message.detailDeleted', [ 'section' => $this->section ]));
            else:
                $arr = array("success"=>"false","message"=>"Something Went Wrong.");
            endif;
            return response()->json($arr);
        endif; 
    }

    public function status( $id, $status ) {
		if($status == '1'):
			$data = ['is_active' => '0'];
			$message = Lang::get('message.statusInactive', ['section' => $this->section]);
		elseif($status == '0'):
			$data = ['is_active' => '1'];
			$message = Lang::get('message.statusActive', ['section' => $this->section]);
		endif;
		if(isset($data) && count($data)):
			SubCategoryModel::where('sub_cat_id',$id)->update($data);
		endif;
		return redirect($this->sectionSlug)->with('success', $message);
	}

}

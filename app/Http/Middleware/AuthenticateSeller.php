<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateSeller {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = 'seller') {

		if (Auth::guard('seller')->check()) {

			return $next($request);
		}

		return redirect('seller/')->with([
				'warning' => 'You must have to be logged in.',
			]);
	}   
}

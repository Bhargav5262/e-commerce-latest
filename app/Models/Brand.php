<?php

namespace App\Models;

use Eloquent, Request;

class Brand extends Eloquent {

	protected $table = 'mst_brand';

	public $timestamps = true;

	protected $fillable = [
		'brand_name',
		'brand_code',
		'brand_img',
		'brand_banner',
		'order_seq',
		'is_active',
		'created_by',
		'updated_by',
	];

	protected $primaryKey = "brand_id";
	const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

	protected $casts = ['brand_img' => 'array','brand_banner' => 'array'];

}
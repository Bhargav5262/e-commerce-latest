<?php
use PHPMailer\PHPMailer\PHPMailer, Carbon\Carbon;
use App\Models\EmailTemplateHeader as EmailTemplateHeaderModel;
use App\Models\EmailTemplate as EmailTemplateModel;
use App\Models\EmailTemplateFooter as EmailTemplateFooterModel;
use App\Models\Category\Category as CategoryModel;
use App\Models\Category\SubCategory as SubCategoryModel;
use App\Models\Brand as BrandModel;
use App\Models\Route as RouteModel;

function _price( $price = 0, $decimal = 2 ){
	$currency = '$';
	$price = str_replace( ',', '', $price );
	return $currency.number_format( round( $price, $decimal ), $decimal );
}

function _nice_date($bad_date = '', $format = FALSE){
	if(empty($bad_date)):
		$bad_date = date('Y-m-d');
	endif;
	if(!$format):
		$format = 'd, M, Y';
	endif;
	return Carbon::parse($bad_date)->format($format);
}

function _nice_time($bad_time = '', $timeformat = FALSE){
	if(empty($bad_time)):
		$bad_time = date('H:i');
	endif;
	if(!$timeformat):
		$timeformat = 'g:i A';
	endif;
	return Carbon::parse($bad_time)->format($timeformat);
}

function _preg_replace($url = ''){
	return preg_replace('/(\/+)/','/',$url);
	//return $url;
}

function _route($ids = ''){
	$data = [];
	if(isset($ids) && !empty($ids)):
    	$data = RouteModel::where('is_active','1')->whereIn('id', explode(',',$ids))->orderBy('route_name','ASC')->pluck('route_name')->toArray();
    	if(isset($data) && !empty($data)):
    		$data = implode(', ', $data);
    	endif;
	endif;
	return $data;
}

function _custom_tags($product_name = '',$cat_id = '',$sub_cat_id = '',$brand_id = '',$manufacturer = ''){
	if(isset($product_name) && !empty($product_name)):
		$product_name = $product_name;
	endif;
	$category = '';
	if(isset($cat_id) && !empty($cat_id)):
		$category = CategoryModel::find($cat_id);
		if(isset($category) && !empty($category)):
			$category = $category->cat_name;
		endif;
	endif;
	$subcategory = '';
	if(isset($sub_cat_id) && !empty($sub_cat_id)):
		$subcategory = SubCategoryModel::find($sub_cat_id);
		if(isset($subcategory) && !empty($subcategory)):
			$subcategory = $subcategory->sub_cat_name;
		endif;
	endif;
	$brand = '';
	if(isset($brand_id) && !empty($brand_id)):
		$brand = BrandModel::find($brand_id);
		if(isset($brand) && !empty($brand)):
			$brand = $brand->brand_name;
		endif;
	endif;
	if(isset($manufacturer) && !empty($manufacturer)):
		$manufacturer = $manufacturer;
	endif;
	return [str_replace(' ', '', $product_name),str_replace(' ', '', $category),str_replace(' ', '', $subcategory),str_replace(' ', '', $brand,$manufacturer)];
	//return $url;
}

function _email_template_content($id,$user_data) {
	$content_array = array();
	$emailTemplate = EmailTemplateModel::where('id', $id)->where('status', '1')->first();
	$string="";
	$subject=$emailTemplate->subject;
	if(isset($emailTemplate)):
		$keys = [
			'{LINK}',
			'{NAME}',
           	'{EMAIL}',
			'{PHONE}',
			'{SUBJECT}',
			'{PASSWORD}',
			'{MESSAGE}',
			'{STATUS}',
			'{DATE}',
			'{TIME}',
			'{USERNAME}',
			'{FROM_EMAIL}',
			'{TO_EMAIL}',
			'{FROM_NAME}',
			'{TO_NAME}',
		];
		$keys2 = [
			'{ACTION_URL}',
		];
		$only_string = $emailTemplate->body;
		$string = $emailTemplate->hasTemplateHeader->description;
		$string .= $emailTemplate->body;
		$string .= $emailTemplate->hasTemplateFooter->description;
		foreach ($keys as $v):
			$k = str_replace("{","",$v);
			$k = str_replace("}","",$k);
			if(isset($user_data[$k])):
				$string = str_replace( $v, $user_data[$k], $string);
				$only_string = str_replace( $v, $user_data[$k], $only_string);
				$subject = str_replace( $v, $user_data[$k], $subject);
			endif;
		endforeach;
		foreach ($keys2 as $v):
			$k = str_replace("{","",$v);
			$k = str_replace("}","",$k);
			if(isset($user_data[$k])):
				$string = str_replace( $v, $user_data[$k], $string);
				$only_string = str_replace( $v, $user_data[$k], $only_string);
				$subject = str_replace( $v, $user_data[$k], $subject);
			endif;
		endforeach;
	endif;
	//echo $string; exit;
	$content_array = array($subject,$string,$only_string);
	return $content_array;
}

function _mail_send_general( $replyData = array() ,$subject="" , $message="" , $mailids = array() , $attachments = array() ) {
	$fromData = $fromdata=array(
		'host'=>env('SMTP_HOST'),
		'port'=>env('SMTP_PORT'),
		'username'=>env('SMTP_USERNAME'),
		'password'=>env('SMTP_PASSWORD'),
		'from_name'=>env('SMTP_FROM_NAME'),
	);
	$replyToMail = $fromData['username'];
	$replyToName = $fromData['from_name'];
	if( isset($replyData['email']) && $replyData['email'] != '' ) $replyToMail = $replyData['email'];
	if( isset($replyData['name']) && $replyData['name'] != '' ) $replyToName = $replyData['name'];

	$mail = new PHPMailer;
	$IS_SMTP = 0;
	if($IS_SMTP):
		//$mail->SMTPDebug = 2; //Alternative to above constant
		$mail->isSMTP();
		$mail->CharSet = "utf-8";
		$mail->Host = $fromData['host'];
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$mail->Port = $fromData['port'];
	endif;
	$mail->Username = $fromData['username'];
	$mail->Password = $fromData['password'];
	$mail->setFrom( $fromData['username'] , $fromData['from_name'] );
	if( $replyToMail != '' ):
		$mail->AddReplyTo( $replyToMail , $replyToName );
	endif;
	//  Add Attachments >>
	if( isset( $attachments ) && count( $attachments ) ):
		foreach ( $attachments as $key => $value ):
			$mail->AddAttachment( $value );
		endforeach;
	endif;
	//  << Add Attachments
	$mail->Subject = $subject;
	$mail->MsgHTML($message);
	if(count($mailids)):
		foreach ($mailids as $key => $value):
			$mail->addAddress($key,$value);
		endforeach;
	endif;
	$mail->isHTML(true);
	$a = $mail->send();
	return $a;
}

function _curl_response( $url = "", $fields = "" ){
	$fields = json_encode ($fields);
	$headers = array (
		'Content-Type: application/json'
	);
	$ch = curl_init ();
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
	$result = curl_exec ( $ch );
	curl_close ( $ch );
	return json_decode($result, true);
}
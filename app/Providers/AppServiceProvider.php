<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('SITE_NAME', 'e-commerce-latest' );
        view()->share('ADMIN_THEME_PATH', url('public/backend') );
        view()->share('UPLOAD_PATH', url('public/uploads') );
    }
}

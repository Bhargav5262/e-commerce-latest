@extends('admin.app.index')

@section('title')
    {{$section ?? 'Admin'}}
@stop

@section('css')

@stop

@section('model')

@stop

@section('content')
<div class="content-wrapper">
    <div class="content-header row mb-1">
        <div class="content-header-left col-md-12 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Master Management</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">{{$section}} Management</a></li>
                        <li class="breadcrumb-item"><a href="{{url($sectionSlug)}}">{{$section}}</a></li>
                        <li class="breadcrumb-item active">{{ isset($view) ? ucfirst($view) : 'List'}}</li>
                    </ol>
                </div>
            </div>
            <h3 class="content-header-title mb-0">{{$singleSection}}</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="row match-height">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{{ isset($view) ? ucfirst($view) : 'List'}} {{$singleSection}}
                                @if ( isset($view) && in_array($view, ['add','edit','view']) )
                                    <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary pull-right"><i class="ft-arrow-left"></i> Back To List</a>
                                @else
                                    <a href="{{ url( $sectionSlug , 'add') }}" class="btn btn-primary pull-right"><i class="ft-plus"></i> Add {{ $singleSection }}</a>
                                @endif
                            </h4>
                        </div>    
                        @if ( isset($view) && in_array($view, ['add','edit']) )
                            {{--<link href="{{$ADMIN_THEME_PATH}}/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />--}}
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <form method="POST" name="subchildcategory_form" id="subchildcategory_form" class="form" action="{{url($sectionSlug.'/action/')}}/{{$view}}/{{isset($data->sub_child_cat_id) ? $data->sub_child_cat_id: '0'}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label for="cat_id">Category</label>
                                                <select class="form-control round" name="cat_id" id="cat_id">
                                                    <option value="">Select category</option>
                                                    @foreach($category as $cat)
                                                        <option value="{{ $cat->cat_id }}" @if(isset($data) && $cat->cat_id == $data->cat_id) selected @endif>{{ $cat->cat_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="sub_cat_id">Sub Category</label>
                                                <select class="form-control round" name="sub_cat_id" id="sub_cat_id">
                                                    <option value="">Select sub category</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="child_cat_id">Child Category</label>
                                                <select class="form-control round" name="child_cat_id" id="child_cat_id">
                                                    <option value="">Select child category</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="sub_child_cat_name">Sub Child Category Name</label>
                                                <input type="text" name="sub_child_cat_name" id="sub_child_cat_name" value="{{$data->sub_child_cat_name ?? old('sub_child_cat_name')}}" class="form-control round" placeholder="sub child category name">
                                                <input type="hidden" name="_sub_child_cat_name" id="_sub_child_cat_name" value="{{$data->sub_child_cat_name ?? ''}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="cat_dec">Description</label>
                                                <textarea name="cat_dec" id="cat_dec" class="form-control round" placeholder="Description">{{$data->cat_dec ?? old('cat_dec')}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="sub_child_cat_img_files">Sub Child Category Images</label>
                                                <input type="file" name="sub_child_cat_img_files[]" id="sub_child_cat_img_files" multiple="" class="form-control round @if(isset($view) && in_array($view, ['edit']) ) ignore @endif">
                                            </div>
                                            @if(isset($data->sub_child_cat_img) && !empty($data->sub_child_cat_img))
                                                <div class="row">
                                                    @foreach($data->sub_child_cat_img as $key => $row)
                                                        <figure class="col-lg-1 col-md-6 col-12">
                                                           <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                        </figure>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <label for="sub_child_cat_banner_files">Sub Child Category Banner</label>
                                                <input type="file" name="sub_child_cat_banner_files[]" id="sub_child_cat_banner_files" multiple="" class="form-control round @if(isset($view) && in_array($view, ['edit']) ) ignore @endif">
                                            </div>
                                            @if(isset($data->sub_child_cat_banner) && !empty($data->sub_child_cat_banner))
                                                <div class="row">
                                                    @foreach($data->sub_child_cat_banner as $key => $row)
                                                        <figure class="col-lg-1 col-md-6 col-12">
                                                           <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                        </figure>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <label for="order_seq">Order Seq </label>
                                                <input type="number" name="order_seq" id="order_seq" value="{{$data->order_seq ?? old('order_seq')}}" class="form-control round" placeholder="Order Seq   ">
                                            </div>
                                            <div class="form-group">
                                                <label for="is_active">Status</label>
                                                <select class="form-control round" name="is_active" id="is_active">
                                                    <option value="1" @if(isset($data) && $data->is_active == "1") selected @endif>Active</option>
                                                    <option value="0" @if(isset($data) && $data->is_active == "0") selected @endif>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                        @if ( isset($view) && in_array($view, ['add','edit']) )
                                            <div class="form-actions">
                                                <a href="{{url($sectionSlug)}}" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </a>
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        @endif
                                    </form>
                                </div>
                            </div>      
                            {{--<script src="{{$ADMIN_THEME_PATH}}/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>--}}
                        @elseif ( isset($view) && in_array($view, ['view']) )
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Category:</label>
                                                    <label class="col-md-8 label-control">{{$data->category->cat_name ?? ""}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Sub Category:</label>
                                                    <label class="col-md-8 label-control">{{$data->subcategory->sub_cat_name ?? ""}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Child Category:</label>
                                                    <label class="col-md-8 label-control">{{$data->childcategory->child_cat_name ?? ""}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Sub Child Category:</label>
                                                    <label class="col-md-8 label-control">{{$data->sub_child_cat_name ?? ""}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Description:</label>
                                                    <label class="col-md-8 label-control">{{$data->cat_dec ?? old('cat_dec')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Sub Child Category Images:</label>
                                                    @if(isset($data->sub_child_cat_img) && !empty($data->sub_child_cat_img))
                                                        <div class="row">
                                                            @foreach($data->sub_child_cat_img as $key => $row)
                                                                <figure class="col-lg-1 col-md-6 col-12">
                                                                   <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                                </figure>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Sub Child Category Banner:</label>
                                                    @if(isset($data->sub_child_cat_banner) && !empty($data->sub_child_cat_banner))
                                                        <div class="row">
                                                            @foreach($data->sub_child_cat_banner as $key => $row)
                                                                <figure class="col-lg-1 col-md-6 col-12">
                                                                   <img class="img-thumbnail img-fluid" src="{{ $row }}" itemprop="thumbnail"/>
                                                                </figure>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Order Seq:</label>
                                                    <label class="col-md-8 label-control">{{$data->order_seq ?? old('order_seq')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row mx-auto last">
                                                    <label class="col-md-4 label-control">Status:</label>
                                                    <label class="col-md-8 label-control">{{$data->status ?? ""}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="{{ url( $sectionSlug ) }}" class="btn btn-primary"><i class="ft-arrow-left"></i> Back To List</a>
                                    </div>
                                </div>
                            </div>    
                        @else
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered text-inputs-searching">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Sub Category</th>
                                                    <th>Child Category</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Sub Category</th>
                                                    <th>Child Category</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                @if(isset($data) && count($data) > 0)
                                                    @foreach($data as $key => $row)
                                                        <tr>
                                                            <td>{{ $row->sub_child_cat_name }}</td>
                                                            <td>{{ $row->category->cat_name }}</td>
                                                            <td>{{ $row->subcategory->sub_cat_name }}</td>
                                                            <td>{{ $row->childcategory->child_cat_name }}</td>
                                                            <td>{{ $row->status }}</td>
                                                            <td>
                                                                <a href="{{ url( $sectionSlug , [ 'view' , $row->sub_child_cat_id] ) }}" title="View"><i class="ft-eye"></i></a> | 
                                                                <a href="{{ url( $sectionSlug , [ 'edit' , $row->sub_child_cat_id ] ) }}" title="Edit"><i class="la la-pencil"></i></a> |
                                                                <a href="{{ url( $sectionSlug ,[ $row->sub_child_cat_id , $row->is_active, 'status']) }}" title="Change Status" >
                                                                    @if( $row->is_active == '0' )
                                                                        <i class="la la-check"></i>
                                                                    @else
                                                                        <i class="la la-times"></i>
                                                                    @endif
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="6" class="text-center">No data found.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop

@section('jspage')
<script type="text/javascript">
    $(document).ready(function(){
        @if(isset($view) && in_array($view, ['add','edit']))
            getSubSelectOption($('#cat_id').val(),"{{url('admin/child-category/sub-category')}}");
            $('#cat_id').change(function() {
                getSubSelectOption($(':selected').val(),"{{url('admin/child-category/sub-category')}}");
            });
            function getSubSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'cat_id':val}).done(function(response) {
                    $('#sub_cat_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#sub_cat_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    @if(isset($view) && in_array($view, ['edit']))
                        $('#sub_cat_id').val("{{$data->sub_cat_id ?? ""}}");
                    @endif
                    getChildSelectOption($('#sub_cat_id').val(),"{{url('admin/sub-child-category/child-category')}}");    
                });
            }
            $('#sub_cat_id').change(function() {
                getChildSelectOption($('#sub_cat_id').val(),"{{url('admin/sub-child-category/child-category')}}");
            });
            function getChildSelectOption(val,url) {
                cp_app.ajaxRequest(url,{_token:cp_token.getToken(),'sub_cat_id':val}).done(function(response) {
                    $('#child_cat_id option:gt(0)').remove();
                    $.each(response, function(key, value) {
                        $('#child_cat_id').append($("<option></option>").attr("value",value.id).text(value.text)); 
                    });
                    @if(isset($view) && in_array($view, ['edit']))
                        $('#child_cat_id').val("{{$data->child_cat_id ?? ""}}");
                    @endif
                });
            }
        @endif
    });
</script>
@stop

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
|		Get PHP Information
|--------------------------------------------------------------------------
*/

Route::get('/get-phpinfo', function () {
  phpinfo();
  die();
});

/*********************************************************************************
 *********************************************************************************
 *
 *										ADMIN
 *
 *********************************************************************************
 ********************************************************************************/

/*
|--------------------------------------------------------------------------
|		Authentication Routes
|--------------------------------------------------------------------------
*/

Route::get('/admin', function() {
	return redirect('admin/login');
});

Route::group([ 'prefix' => 'admin' ], function () {
	Route::get('/', 'Admin\Login@getLogin');				//	Show Login Form
	Route::get('/login', 'Admin\Login@getLogin');			//	Show Login Form
	Route::post('/login', 'Admin\Login@postLogin');			//	Check Login
	Route::get('/logout', 'Admin\Login@getLogout');			//	LogOut
	Route::get('/forgotpassword', 'Admin\Login@getForgotPassword');
	Route::post('/forgotpassword', 'Admin\Login@postForgotPassword');
	Route::get('/new-password/{id}', 'Admin\Login@newPassword');
	Route::post('/confirm-new-password', 'Admin\Login@postConfirmNewPassword');
	Route::get('/verifyotp/', 'Admin\Login@getVerifyOTP');
	Route::post('/verifyotp/', 'Admin\Login@postVerifyOTP');
	Route::post('/confirmotp', 'Admin\Login@postConfirmOTP');
});

/*
|--------------------------------------------------------------------------
|		Dashboard
|--------------------------------------------------------------------------
*/

Route::group([ 'prefix' => 'admin/dashboard', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Dashboard@index');
});

/*
|--------------------------------------------------------------------------
|		Category
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/category', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Category\Category@index');
	Route::post('/','Admin\Category\Category@index');
	Route::get('/add','Admin\Category\Category@Add');
	Route::get('/edit/{id}','Admin\Category\Category@Edit');
	Route::get('/view/{id}','Admin\Category\Category@View');
	Route::post('/action/{action}/{_id}', 'Admin\Category\Category@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Category\Category@Action');
	Route::get('/{id}/{status}/status','Admin\Category\Category@status');
});

Route::group(['prefix'=>'admin/pltCommon', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\pltCommon\Category@index');
	Route::post('/','Admin\pltCommon\Category@index');
	Route::get('/add','Admin\pltCommon\Category@Add');
	Route::get('/edit/{id}','Admin\pltCommon\Category@Edit');
	Route::get('/view/{id}','Admin\pltCommon\Category@View');
	Route::post('/action/{action}/{_id}', 'Admin\pltCommon\Category@Action');
	Route::get('/action/{action}/{_id}', 'Admin\pltCommon\Category@Action');
	Route::get('/{id}/{status}/status','Admin\pltCommon\Category@status');
});


/*
|--------------------------------------------------------------------------
|		Sub-category
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/sub-category', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Category\SubCategory@index');
	Route::post('/','Admin\Category\SubCategory@index');
	Route::get('/add','Admin\Category\SubCategory@Add');
	Route::get('/edit/{id}','Admin\Category\SubCategory@Edit');
	Route::get('/view/{id}','Admin\Category\SubCategory@View');
	Route::post('/action/{action}/{_id}', 'Admin\Category\SubCategory@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Category\SubCategory@Action');
	Route::get('/{id}/{status}/status','Admin\Category\SubCategory@status');
});

/*
|--------------------------------------------------------------------------
|		Child Category
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/child-category', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Category\ChildCategory@index');
	Route::post('/','Admin\Category\ChildCategory@index');
	Route::get('/add','Admin\Category\ChildCategory@Add');
	Route::get('/edit/{id}','Admin\Category\ChildCategory@Edit');
	Route::get('/view/{id}','Admin\Category\ChildCategory@View');
	Route::post('/action/{action}/{_id}', 'Admin\Category\ChildCategory@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Category\ChildCategory@Action');
	Route::get('/{id}/{status}/status','Admin\Category\ChildCategory@status');
	Route::post('sub-category', 'Admin\Category\ChildCategory@subCategory');
});

/*
|--------------------------------------------------------------------------
|		Sub Child Category
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/sub-child-category', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Category\SubChildCategory@index');
	Route::post('/','Admin\Category\SubChildCategory@index');
	Route::get('/add','Admin\Category\SubChildCategory@Add');
	Route::get('/edit/{id}','Admin\Category\SubChildCategory@Edit');
	Route::get('/view/{id}','Admin\Category\SubChildCategory@View');
	Route::post('/action/{action}/{_id}', 'Admin\Category\SubChildCategory@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Category\SubChildCategory@Action');
	Route::get('/{id}/{status}/status','Admin\Category\SubChildCategory@status');
	Route::post('child-category', 'Admin\Category\SubChildCategory@childCategory');
	Route::post('child-sub-category', 'Admin\Category\SubChildCategory@childSubCategory');
});

/*
|--------------------------------------------------------------------------
|		Zone
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/zone', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Zone@index');
	Route::post('/','Admin\Zone@index');
	Route::get('/add','Admin\Zone@Add');
	Route::get('/edit/{id}','Admin\Zone@Edit');
	Route::get('/view/{id}','Admin\Zone@View');
	Route::post('/action/{action}/{_id}', 'Admin\Zone@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Zone@Action');
	Route::get('/{id}/{status}/status','Admin\Zone@status');
});

/*
|--------------------------------------------------------------------------
|		Region
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/region', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Region@index');
	Route::post('/','Admin\Region@index');
	Route::get('/add','Admin\Region@Add');
	Route::get('/edit/{id}','Admin\Region@Edit');
	Route::get('/view/{id}','Admin\Region@View');
	Route::post('/action/{action}/{_id}', 'Admin\Region@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Region@Action');
	Route::get('/{id}/{status}/status','Admin\Region@status');
});

/*
|--------------------------------------------------------------------------
		Area
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/area', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Area@index');
	Route::post('/','Admin\Area@index');
	Route::get('/add','Admin\Area@Add');
	Route::get('/edit/{id}','Admin\Area@Edit');
	Route::get('/view/{id}','Admin\Area@View');
	Route::post('/action/{action}/{_id}', 'Admin\Area@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Area@Action');
	Route::get('/{id}/{status}/status','Admin\Area@status');
	Route::post('zone/region', 'Admin\Area@zoneregion');
});

/*
|--------------------------------------------------------------------------
		Route
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/route', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Route@index');
	Route::post('/','Admin\Route@index');
	Route::get('/add','Admin\Route@Add');
	Route::get('/edit/{id}','Admin\Route@Edit');
	Route::get('/view/{id}','Admin\Route@View');
	Route::post('/action/{action}/{_id}', 'Admin\Route@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Route@Action');
	Route::get('/{id}/{status}/status','Admin\Route@status');
	Route::post('zone/region', 'Admin\Route@zoneregion');
	Route::post('region/area', 'Admin\Route@regionarea');
});

/*
|--------------------------------------------------------------------------
|		Brand
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/brand', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Brand@index');
	Route::post('/','Admin\Brand@index');
	Route::get('/add','Admin\Brand@Add');
	Route::get('/edit/{id}','Admin\Brand@Edit');
	Route::get('/view/{id}','Admin\Brand@View');
	Route::post('/action/{action}/{_id}', 'Admin\Brand@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Brand@Action');
	Route::get('/{id}/{status}/status','Admin\Brand@status');
});

/*
|--------------------------------------------------------------------------
|		Products
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/products', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Products\Products@index');
	Route::post('/','Admin\Products\Products@index');
	Route::get('/add','Admin\Products\Products@Add');
	Route::get('/edit/{id}','Admin\Products\Products@Edit');
	Route::get('/view/{id}','Admin\Products\Products@View');
	Route::post('/action/{action}/{_id}', 'Admin\Products\Products@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Products\Products@Action');
	Route::get('/{id}/{status}/status','Admin\Products\Products@status');
	Route::get('/{id}/{status}/application','Admin\Products\Products@applicationstatus');
	Route::post('/trashmedia','Admin\Products\Products@trashmedia');
});

/*
|--------------------------------------------------------------------------
|		Team
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/team', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Team\Team@index');
	Route::post('/','Admin\Team\Team@index');
	Route::get('/add','Admin\Team\Team@Add');
	Route::get('/edit/{id}','Admin\Team\Team@Edit');
	Route::get('/view/{id}','Admin\Team\Team@View');
	Route::post('/action/{action}/{_id}', 'Admin\Team\Team@Action');
	Route::get('/action/{action}/{_id}', 'Admin\Team\Team@Action');
	Route::get('/{id}/{status}/status','Admin\Team\Team@status');
});

/*
|--------------------------------------------------------------------------
|		Users
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'admin/users', 'middleware' => ['admin'] ], function () {
	Route::get('/','Admin\Users\Users@index');
	Route::get('/{id}/{status}/application','Admin\Users\Users@applicationstatus');
	Route::get('/{id}/{status}/status','Admin\Users\Users@status');
});

/*
|--------------------------------------------------------------------------
|		Validations
|--------------------------------------------------------------------------
*/

Route::post('barcodeverify', 'Validations@barcodeverify');
Route::post('hsnnoverify', 'Validations@hsnnoverify');
Route::post('productnameverify', 'Validations@productnameverify');

Route::post('team-codeverify', 'Validations@teamcodeverify');
Route::post('team-mobileverify', 'Validations@teammobileverify');
Route::post('team-emailverify', 'Validations@teamemailverify');

Route::post('region', 'Validations@region');
Route::post('area', 'Validations@area');
Route::post('getroute', 'Validations@getroute');

Route::post('categorynameverify', 'Validations@categorynameverify');
Route::post('subcategorynameverify', 'Validations@subcategorynameverify');
Route::post('childcategorynameverify', 'Validations@childcategorynameverify');
Route::post('subchildcategorynameverify', 'Validations@subchildcategorynameverify');

Route::post('zonenameverify', 'Validations@zonenameverify');
Route::post('regionnameverify', 'Validations@regionnameverify');
Route::post('areanameverify', 'Validations@areanameverify');
Route::post('routenameverify', 'Validations@routenameverify');

Route::post('brandnamenameverify', 'Validations@brandnamenameverify');

/*
|--------------------------------------------------------------------------
|		Common
|--------------------------------------------------------------------------
*/

Route::post('sub-category', 'Common@subCategory');

/*********************************************************************************
 *********************************************************************************
 *
 *										SELLER
 *
 *********************************************************************************
 ********************************************************************************/

/*
|--------------------------------------------------------------------------
|		Authentication Routes
|--------------------------------------------------------------------------
*/

Route::get('/seller', function() {
	return redirect('seller/login');
});

Route::group([ 'prefix' => 'seller' ], function () {
	Route::get('/', 'Seller\Login@getLogin');				//	Show Login Form
	Route::get('/login', 'Seller\Login@getLogin');			//	Show Login Form
	Route::post('/login', 'Seller\Login@postLogin');			//	Check Login
	Route::get('/logout', 'Seller\Login@getLogout');			//	LogOut
	Route::get('/forgotpassword', 'Seller\Login@getForgotPassword');
	Route::post('/forgotpassword', 'Seller\Login@postForgotPassword');
	Route::get('/new-password/{id}', 'Seller\Login@newPassword');
	Route::post('/confirm-new-password', 'Seller\Login@postConfirmNewPassword');
	Route::get('/verifyotp/', 'Seller\Login@getVerifyOTP');
	Route::post('/verifyotp/', 'Seller\Login@postVerifyOTP');
	Route::post('/confirmotp', 'Seller\Login@postConfirmOTP');
});

/*
|--------------------------------------------------------------------------
|		Dashboard
|--------------------------------------------------------------------------
*/

Route::group([ 'prefix' => 'seller/dashboard', 'middleware' => ['seller'] ], function () {
	Route::get('/','Seller\Dashboard@index');
});

/*
|--------------------------------------------------------------------------
|		Products
|--------------------------------------------------------------------------
*/
Route::group(['prefix'=>'seller/products', 'middleware' => ['seller'] ], function () {
	Route::get('/','Seller\Products\Products@index');
	Route::post('/','Seller\Products\Products@index');
	Route::get('/draft','Seller\Products\Products@draft');
	Route::get('/search','Seller\Products\Products@productSearch');
	Route::post('/autocomplete','Seller\Products\Products@autocomplete');
	Route::get('/add/{id}','Seller\Products\Products@Add');
	Route::get('/addnew','Seller\Products\Products@AddNew');
	Route::get('/editdraft/{id}','Seller\Products\Products@EditDraft');
	Route::get('/edit/{id}','Seller\Products\Products@Edit');
	Route::get('/view/{id}','Seller\Products\Products@View');
	Route::post('/action/{action}/{_id}', 'Seller\Products\Products@Action');
	Route::get('/action/{action}/{_id}', 'Seller\Products\Products@Action');
	Route::get('/{id}/{status}/status','Seller\Products\Products@status');
	Route::post('/trashmedia','Seller\Products\Products@trashmedia');
});